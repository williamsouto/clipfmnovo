<?php

define('ROOT', dirname(__FILE__));
$loader = require_once __DIR__.'/app/lib/core/AutoLoad.php';

$framework = new AppFramework();
$request   = Request::createFromGlobals();

$response  = $framework->handle($request);
$response->send();
