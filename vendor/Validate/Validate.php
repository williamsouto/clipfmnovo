<?php

class Validate {

    const REQUIRED = 'required';
    const NULL     = 'null';
    const MIN      = 'min';
    const SIZE     = 'size';

    const INT      = 'int';
    const FLOAT    = 'float';
    const STRING   = 'string';
    const EARRAY   = 'array';

    /**
     * Verifica se os dados estão no padrão das regras informadas e retorna apenas
     * os valores exigidos
     *
     * @param array $data Conjunto de valores que serão validados
     * @param array $rules Regras que serão usadas para validar os dados data
     *
     * @return array
     * @throws ValidationException
     */
    public static function validation($data, $rules) {
        $errors = [];
        $keys   = array_keys($rules);
        $arrayFiltered = ArrayHelper::only($data, $keys);

        foreach ($rules as $key => $rule) {
            list($required, $cast, $others) = array_pad(explode('|', $rule, 3), 3, '');

            if ($required == Validate::REQUIRED && empty($arrayFiltered[$key])) {
                $errors[] = "Campo {$key} é obrigatório";
            }

            if (!empty($arrayFiltered[$key]) && $cast == Validate::INT && !is_numeric($arrayFiltered[$key] = (int)$arrayFiltered[$key])) {
                $errors[] = "Campo {$key} deve ser um inteiro";
            }

            if (!empty($arrayFiltered[$key]) && $cast == Validate::FLOAT && !is_numeric($arrayFiltered[$key] = floatval($arrayFiltered[$key]))) {
                $errors[] = "Campo {$key} deve ser um float";
            }

            if (!empty($arrayFiltered[$key]) && $cast == Validate::STRING && !is_string($arrayFiltered[$key])) {
                $errors[] = "Campo {$key} deve ser uma string";
            }

            if (!empty($arrayFiltered[$key]) && $cast == Validate::STRING && is_string($arrayFiltered[$key]) && !empty($others)) {
                list($strRule, $size) = explode(':', $others);

                if ($strRule == Validate::MIN && strlen($arrayFiltered[$key]) < $size) {
                    $errors[] = "Campo {$key} deve ter no mínimo {$size} caractere(s) \n";
                } else if ($strRule == Validate::SIZE && strlen($arrayFiltered[$key]) != $size) {
                    $errors[] = "Campo {$key} deve ter {$size} caractere(s) \n";
                }
            }

            if (!empty($arrayFiltered[$key]) && $cast == Validate::EARRAY && !is_array($arrayFiltered[$key])) {
                $errors[] = "Campo {$key} deve ser um array";
            }
        }

        if (!empty($errors)) {
            throw new ValidationException(sprintf("Erro de validação. \n%s", implode('. ', $errors)));
        }

        return $arrayFiltered;
    }
}
