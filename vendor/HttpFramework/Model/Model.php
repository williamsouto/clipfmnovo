<?php

class Model implements ModelInterface {

    /** @var Persistence  */
    protected $persistence;

    /**
     * Model constructor.
     * @throws Exception
     */
    public function __construct() {
        $this->persistence = Persistence::getInstance();
    }

    /**
     * Busca os dados do usuário logado
     *
     * @return object
     */
    protected function buscarUsuarioLogado() {
        return Session::getUserSessionData();
    }
}
