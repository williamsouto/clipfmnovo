<?php

class ResourceResolver extends Service {

    /**
     * Carrega os serviços da aplicação
     *
     * @return array
     */
    public function getResources() {
        $appFramework = new AppFramework();
        return $appFramework->registrarServicos();
    }
}
