<?php

interface HttpExceptionInterface {
    /**
     * Retorna o código do status
     *
     * @return int An HTTP response status code
     */
    public function getStatusCode();

    /**
     * Retorna o header da resposta
     *
     * @return array Response headers
     */
    public function getHeaders();
}
