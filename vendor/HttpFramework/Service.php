<?php

class Service {

    /** @var \ContainerFramework **/
    protected $container;

    /**
     * @var array $configApp
     */
    protected $appConfig;

    /**
     * Service constructor.
     * @param ContainerFramework|null $container
     */
    public function __construct(ContainerFramework $container = null) {
        $this->setContainer($container);
        $this->appConfig = Config::getInstance()->initializeConfiguration();
    }

    /**
     * Seta o container do framework
     *
     * @param \ContainerFramework|null $container
     */
    protected function setContainer(ContainerFramework $container = null) {
        $this->container = $container;
    }

    /**
     * Retorna a configuração solicitada
     *
     * @param  string $key
     * @return string|array
     *
     * @throws \Exception
     */
    protected function getConfig($key) {
        if (!array_key_exists($key, $this->appConfig)) {
            throw new Exception(sprintf('Configuração %s não foi parametrizada', $key));
        }

        return $this->appConfig[$key];
    }
}
