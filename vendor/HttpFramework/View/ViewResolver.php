<?php

/**
 * Classe responsável em gerenciar aspectos vinculados a view
 */
class ViewResolver extends Service implements ViewResolverInterface {

    /** @var Helper */
    private $helper;

    /** @var array Parâmetros para ser usados na view */
    private $parameters = [];

    /** @var array Arquivos JS que serão carregados na view */
    private $filesJS = [];

    /** @var  bool Indicador para verificar qual subheader incluir no header */
    private $headDefault;

    public function __construct(ContainerFramework $container) {
        parent::__construct($container);
    }

    /**
     * Método que renderiza a view passada como parâmetro
     *
     * @param string $view
     * @param string $title
     * @param bool $header
     * @param bool $headDefault
     * @param bool $footer
     *
     * @return string
     * @throws Exception
     */
    public function render($view, $title = '', $header = true, $headDefault = true, $footer = true) {
        $this->bootHelpers();
        $this->parameters['header'] = $header;
        $this->parameters['footer'] = $footer;

        ob_start();
        $this->headDefault = $headDefault;
        $this->setTitle($title);
        $this->loadHeader();
        $this->load($view);
        $this->loadFooter();
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    /**
     * Renderiza um template
     *
     * @param string $view Nome da view
     *
     * @return string
     * @throws Exception
     */
    public function renderTemplate($view) {
        $this->bootHelpers();

        ob_start();
        $this->loadTemplate($view);
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    /**
     * Renderiza um component
     *
     * @param string $view Nome da view
     *
     * @return string
     * @throws Exception
     */
    public function renderComponent($view) {
        $this->bootHelpers();

        ob_start();
        $this->load($view);
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    /**
     * Seta os parâmetros utilizados na view
     *
     * @param $parameters
     * @return $this
     */
    public function setParameters($parameters) {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * Carrega a view
     *
     * @param string $name
     *
     * @return bool
     * @throws Exception
     */
    public function load($name) {
        $this->loadJS($name);
        return require_once $this->getConfig('default').'/view/'.$name;
    }

    /**
     * Renderiza o header do site
     *
     * @internal param string $header
     * @throws Exception
     */
    private function loadHeader() {
        if (file_exists($this->getConfig('common').'/template/header.phtml')) {
            require $this->getConfig('common').'/template/header.phtml';
        }

        $this->loadJS('Template/header.phtml');
    }

    /**
     * Carrega o subheader do site
     *
     * @return string
     * @throws Exception
     */
    protected function loadSubHeader() {
        $header = $this->headDefault ? 'header-default.phtml' : 'header-administrador.phtml';

        ob_start();
        $this->loadTemplate($header);
        $subHead = ob_get_contents();
        ob_end_clean();

        return $subHead;
    }

    /**
     * Renderiza o footer do site
     *
     * @throws Exception
     */
    private function loadFooter() {
        if (file_exists($this->getConfig('common').'/template/footer.phtml')) {
            require $this->getConfig('common').'/template/footer.phtml';
        }
    }

    /**
     * Carrega o template passado como parâmetro
     *
     * @param  $template
     * @throws Exception
     */
    private function loadTemplate($template) {
        if (file_exists($this->getConfig('common').'/template/'.$template)) {
            require $this->getConfig('common').'/template/'.$template;
        }

        $this->loadJS('Template/'.$template);
    }

    /**
     * Seta o título da página
     *
     * @param $title
     */
    public function setTitle($title) {
        $this->parameters['title'] = $title;
    }

    /**
     * Renderiza uma view e retorna para a view que fez a chamada
     *
     * @param $view
     * @param array $parameters
     * @param bool $template
     *
     * @throws Exception
     */
    protected function renderView($view, $parameters = [], $template = false) {
        ob_start();
        $this->parameters = $parameters;

        if ($template) {
            $this->loadTemplate($view);
        } else {
            $this->load($view);
        }

        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    /**
     * Inicializa os helpers e configura as dependencias do serviço
     *
     * @throws Exception
     */
    private function bootHelpers() {
        $this->helper = $this->container->get('helper');
    }

    /**
     * Carrega o arquivo JS da view
     *
     * @param string $view
     * @throws Exception
     */
    private function loadJS($view) {
        list($folder, $view) = explode('/', $view);

        $view   = substr($view, 0 , (strrpos($view, ".")));
        $file   = $view == 'index' ? strtolower($folder) : $view;
        $fileJS = $folder.DS.$file.".js";

        require $this->getConfig('config').DS.'DependencyJS.php';
        $collectionJS = new JSCollection();
        $filesJS = $collectionJS->getCollectionJS($folder, $view);

        foreach ($filesJS as $file) {
            $this->filesJS[] = $file.'.js';
        }

        $path = $this->getConfig('public').DS.'js'.DS;

        if (file_exists($path.$fileJS)) {
            $this->filesJS[] = $fileJS;
        }
    }
}
