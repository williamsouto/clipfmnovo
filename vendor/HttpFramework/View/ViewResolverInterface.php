<?php


interface ViewResolverInterface {

    /**
     * Retorna a view
     *
     * @param string $name Nome da view
     *
     * @return mixed|false Uma view ou false em caso de n�o existir arquivo
     *
     */
    public function load($name);

    /**
     * Renderiza a view juntamente com os par�metros
     *
     * @param $view
     * @return mixed
     */
    public function render($view);
}
