<?php

class HttpFramework extends Service {

    public function __construct(ContainerFramework $container) {
        parent::__construct($container);
    }

    /**
     * {@inheritdoc}
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function handle(Request $request) {
        $request->headers->set('X-Php-Ob-Level', ob_get_level());

        try {
            return $this->handleRaw($request);
        } catch (\Exception $e) {
            return $this->handleException($e, $request);
        }
    }

    /**
     * Método responsável em converter uma requisição em resposta
     *
     * Exceptions are not caught.
     *
     * @param Request $request A Request instance
     *
     * @return Response A Response instance
     *
     * @throws Exception
     */
    private function handleRaw(Request $request) {
        /** @var $resolverController ControllerResolver **/
        $resolverController = $this->container->get('controller_resolver');

        if (false === $controller = $resolverController->getController($request)) {
            throw new NotFoundHttpException(sprintf('Não foi possivel achar a rota para o caminho "%s".', $request->getPathInfo()));
        }

        // controller arguments
        $arguments = $resolverController->getArguments($request, $controller);

        // call controller
        $response = call_user_func_array($controller, $arguments);

        if (!$response instanceof Response) {
            $msg = sprintf('O controller deve retornar um Response (%s dado).', $this->varToString($response));

            // O usuário pode ter esquecido de devolver algo
            if (null === $response) {
                $msg .= ' Você se esqueceu de adicionar uma declaração de Response em algum lugar em seu controller?';
            }
            throw new \LogicException($msg);
        }

        return $this->filterResponse($response);
    }

    /**
     * Filtra o objeto Response
     *
     * @param Response $response Uma instância de Response
     *
     * @return Response Uma instância de Response filtrado
     *
     * @throws \RuntimeException Se o objeto passado não é uma instância de Response
     */
    private function filterResponse(Response $response) {
        return $response;
    }

    /**
     * Handles an exception by trying to convert it to a Response.
     *
     * @param \Exception $e       An \Exception instance
     * @param Request    $request A Request instance
     *
     * @return Response A Response instance
     *
     * @throws \Exception
     */
    private function handleException(\Exception $e, $request) {
        $response = new Response('', 500, $request->headers->all());

        if ($e instanceof ExceptionInterface) {
            $response->setStatusCode($e->getHeader('X-Status-Code'));
            $response->setContent($e->getMessage());
        } else {
            if ($this->getConfig('environment') == 'dev') {
                $content = empty($e->xdebug_message) ? $e : '<table>'.$e->xdebug_message.'</table>';
                $response->setContent($content);
            }
        }

        try {
            return $this->filterResponse($response);
        } catch (\Exception $e) {
            return $response;
        }
    }

    private function varToString($var) {
        if (is_object($var)) {
            return sprintf('Objeto(%s)', get_class($var));
        }

        if (is_array($var)) {
            $a = array();
            foreach ($var as $k => $v) {
                $a[] = sprintf('%s => %s', $k, $this->varToString($v));
            }

            return sprintf('Array(%s)', implode(', ', $a));
        }

        if (is_resource($var)) {
            return sprintf('Recurso(%s)', get_resource_type($var));
        }

        if (null === $var) {
            return 'null';
        }

        if (false === $var) {
            return 'false';
        }

        if (true === $var) {
            return 'true';
        }

        return (string) $var;
    }
}
