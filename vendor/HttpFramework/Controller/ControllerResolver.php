<?php

class ControllerResolver extends Service implements ArgumentResolverInterface, ControllerResolverInterface {

    /** Construtor
     *
     * @param \ContainerFramework $container
     */
    public function __construct(ContainerFramework $container) {
        parent::__construct($container);
    }

    /**
     * {@inheritdoc}
     *
     * Este método verifica se o controller existe e retorna sua instância
     *
     * @return array $callable Um array contendo a instância do controller e o nome da action
     * @throws Exception
     */
    public function getController(Request $request) {
        $controller = $this->pullController($request);
        $callable   = $this->createController($controller);

        if (!is_callable($callable)) {
            throw new \InvalidArgumentException(sprintf('O controller para URL "%s" não é chamado', $request->server->get('HTTP_HOST').$request->server->get('REQUEST_URI')));
        }

        return $callable;
    }

    /**
     * Retorna uma chamada do controller.
     *
     * @param array $controller Um controller como string
     *
     * @return array Um array contendo a instância do controller e o nome da action
     *
     * @throws Exception
     */
    protected function createController($controller) {
        if (!$this->controllerExists($controller)) {
            throw new \InvalidArgumentException(sprintf('Classe "%s" não existe.', $controller['controller_name']));
        }

        return [$this->instantiateController($controller['controller_name'], $controller['request']), $controller['method']];
    }

    /**
     * Retorna uma instância do controller.
     *
     * @param string $class Um nome de classe
     *
     * @param Request $request
     * @return Controller
     */
    protected function instantiateController($class, $request) {
        $controller = new $class();

        if ($controller instanceof Controller) {
            $controller->setContainer($this->container);
            $controller->setRequest($request);
        }
        return $controller;
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function getArguments(Request $request, $controller) {
        $obj = new \ReflectionObject($controller[0]);
        $objMethod = $obj->getMethod($request->attributes->get('_method'));

        return $this->doGetArguments($request, $objMethod->getParameters());
    }

    /**
     * Extrai o nome do controller na requisição
     *
     * @param \Request $request
     *
     * @return array
     * @internal param $requestUri
     * @throws Exception
     */
    private function pullController(Request $request) {
        $requestUri = $request->server->get('REDIRECT_URL');

        $parameters = null;
        if (substr($requestUri, 1)) {
            list($controller, $method) = array_pad(explode('/', substr($requestUri, 1), 2), 2, '');
        }

        $controller = empty($controller) ? 'home' : $controller;
        $method     = empty($method) ? '/' : $method;

        $route      = $this->resolverRoute(ucfirst($controller.'Controller'), $method);

        if ($this->requiresAuthentication($controller, $method, $request)) {
            $route = $this->resolverRoute(ucfirst('login'.'Controller'), '/');
        }

        $request->attributes->set('_controller', $route['controller']);
        $request->attributes->set('_method', $route['method']);
        $request->attributes->set('_parameters', $route['parameters']);

        return ['controller_name' => $route['controller'], 'method' => $route['method'], 'request' => $request];
    }

    /**
     * Retorna os parâmetros que a action espera receber
     *
     * @param Request $request
     * @param \ReflectionParameter[] $parameters
     *
     * @return array Os argumentos usados quando é chamado o método(action)
     * @throws Exception
     */
    protected function doGetArguments(Request $request, array $parameters) {
        $attributes = $request->attributes->get('_parameters');
        $arguments  = [];

        if (!empty($attributes)) {
            $arguments  = array_pad($attributes, count($parameters), null);
        } else {
            $attributes = !empty($request->query->all()) ? $request->query->all() : $request->request->all();

            if (count($parameters) == 1) {
                $arguments[] = $attributes + $request->files->all();
            } else {
                $arguments = array_pad($attributes, count($parameters), null);
            }
        }

        return $this->container->get('security')->filter($arguments);
    }

    /**
     * Verifica se o controller passado como parâmetro existe
     *
     * @param $controller
     *
     * @return bool
     * @throws Exception
     */
    private function controllerExists($controller) {
        return file_exists($this->getConfig('default').'/controller/'.$controller['controller_name'].'.php');
    }

    /**
     * Verifica se o recurso solicitado necessica de autenticação
     *
     * @param string $controller O nome do controller
     * @param string $method O nome do método
     * @param $request
     *
     * @return bool
     * @throws Exception
     */
    private function requiresAuthentication($controller, $method, $request) {
        $security = $this->getSecurity();
        $required = $security->requiresAuthentication($controller, $method);
        $authenticate = !$security->validateAuthentication($request);

        if ($authenticate) {
            $security->clearUserData();
        }

        if ($authenticate && $required) {
            $security->saveRedirect($controller, $method);
        }

        $security->updateLastAccess();
        return $required ? $authenticate : false;
    }

    /**
     * Retorna o serviço de segurança que está no container
     *
     * @return \Security|Object
     * @throws Exception
     */
    private function getSecurity() {
        return $this->container->get('security');
    }

    /**
     * Processa a rota obtida da requisição
     *
     * @param $controller string Nome do controller
     * @param $method     string Nome do método
     *
     * @return array
     * @throws Exception
     */
    private function resolverRoute($controller, $method) {
        $this->loadRoutes();
        $routeCollections = new RouteCollection();
        $routes = $routeCollections->getRoutes($controller);

        $return = [
            'controller' => 'page',
            'method'     => 'notFound',
            'parameters' => []
        ];

        foreach ($routes as $route) {
            /** @var Route $route */
            $methodReal = $route->match($method);

            if (!empty($methodReal)) {
                $return['controller'] = $controller;
                $return['method']     = $methodReal['method'];
                $return['parameters'] = $methodReal['parameters'];
                break;
            }
        }

        return ['controller' => $return['controller'], 'method' => $return['method'], 'parameters' => $return['parameters']];
    }

    /**
     * Carrega o arquivo com as rotas
     * @throws Exception
     */
    private function loadRoutes() {
        require $this->getConfig('config') . '/Routing.php';
    }
}
