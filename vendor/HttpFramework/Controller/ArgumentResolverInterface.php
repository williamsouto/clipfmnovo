<?php


interface ArgumentResolverInterface {

    /**
     * Retorna os arqumentos para passar para o controller
     *
     * @param Request  $request
     * @param callable $controller
     *
     * @return array Array dos argumentos para passar para o controller
     *
     * @throws \RuntimeException
     */
    public function getArguments(Request $request, $controller);
}