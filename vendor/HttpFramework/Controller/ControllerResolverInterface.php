<?php

interface ControllerResolverInterface {

    /**
     * Retorna a instancia do controller associada com a requisi��o.
     *
     * As several resolvers can exist for a single application, a resolver must
     * return false when it is not able to determine the controller.
     *
     * The resolver must only throw an exception when it should be able to load
     * controller but cannot because of some errors made by the developer.
     *
     * @param Request $request Instance de Request
     *
     * @return callable|false A PHP callable representa o Controller,
     *                        ou false se n�o resolver o pedido da requisi��o
     *
     * @throws \LogicException Se o controller n�o for encontrado
     */
    public function getController(Request $request);

    /**
     * Retorna os argumentos para passar para o controller.
     *
     * @param Request  $request    Uma instancia de Request
     * @param callable $controller A PHP callable
     *
     * @return array Um array de argumentos
     *
     * @throws \RuntimeException
     *
     * @deprecated
     */
    public function getArguments(Request $request, $controller);
}
