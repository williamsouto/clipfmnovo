<?php

class Controller {

    /**
     * @var ContainerFramework
     */
    private $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * Seta o container
     *
     * @param ContainerFramework|null $container Uma instância ContainerFramework ou null
     */
    public function setContainer(ContainerFramework $container = null) {
        $this->container = $container;
    }

    /**
     * Seta a requisição do usuário
     *
     * @param Request $request
     */
    public function setRequest($request) {
        $this->request = $request;
    }

    /**
     * Redireciona o usuário para a url passada como parâmetro
     *
     * @param $url
     * @param int $status
     * @param array $headers
     *
     * @return Response
     */
    protected function redirect($url, $status = 302, $headers = []) {
        $response = new Response('', $status, $headers);
        $url = 'http://'.URL_BASE.'/'.$url;

        $response->setContent(
            sprintf('<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="refresh" content="1;url=%1$s" />

        <title>Redirecting to %1$s</title>
    </head>
    <body>
        Redirecting to <a href="%1$s">%1$s</a>.
    </body>
</html>', htmlspecialchars($url, ENT_QUOTES, 'UTF-8')));

        if (empty($url)) {
            throw new \InvalidArgumentException('Não é possível redirecionar para uma url vazia.');
        }

        $response->headers->set('Location', $url);

        if (301 == $status && !array_key_exists('cache-control', $headers)) {
            $response->headers->remove('cache-control');
        }

        return $response;
    }

    /**
     * Renderiza uma view.
     *
     * @param string   $view O nome da view
     * @param string   $title O título da página
     * @param array    $parameters Um array de parâmetros passados para a view
     * @param bool     $header Indica se é pra exibir o header da aplicação
     * @param bool     $headerDefault
     * @param bool     $footer
     * @param Response $response Uma instancia do Response
     *
     * @return Response A Response instance
     * @throws Exception
     */
    protected function render($view, $title = '', array $parameters = [], $header = true, $headerDefault = true, $footer = true, Response $response = null) {
        if (null === $response) {
            $response = new Response();
        }

        return $response->setContent($this->container->get('view_resolver')->setParameters($parameters)->render($view, $title, $header, $headerDefault, $footer));
    }

    /**
     * Renderiza um template
     *
     * @param string $view
     * @param array $parameters
     * @param \Response|null $response
     *
     * @return Response A Response instance
     * @throws Exception
     */
    protected function renderTemplate($view, array $parameters = [], Response $response = null) {
        if (null === $response) {
            $response = new Response();
        }

        return $response->setContent($this->container->get('view_resolver')->setParameters($parameters)->renderTemplate($view));
    }

    /**
     * Renderiza um componente
     *
     * @param string $view
     * @param array $parameters
     * @param \Response|null $response
     *
     * @return Response A Response instance
     * @throws Exception
     */
    protected function renderComponent($view, array $parameters = [], Response $response = null) {
        if (null === $response) {
            $response = new Response();
        }

        return $response->setContent($this->container->get('view_resolver')->setParameters($parameters)->renderComponent($view));
    }

    /**
     * Retorna um json_encode
     *
     * @param mixed $data    Um data Response
     * @param int   $status  O código do status para ser usado na resposta
     * @param array $headers Array de headers extra para ser adicionados
     *
     * @return \Response
     */
    protected function json($data, $status = 200, $headers = []) {
        $response = new Response(json_encode($data), $status, $headers);

        if (null === $data) {
            $data = new \ArrayObject();
        }

        return $response->setJson($data);
    }

    /**
     * Registra a autenticação do usuário
     *
     * @param $idUser
     * @param $user
     *
     * @return string
     *
     * @throws Exception
     * @internal param int $idUsuario
     */
    protected function registerAuthenticatedUser($idUser, $user) {
        return $this->container->get('security')->registerAuthenticated($idUser, $user, $this->request);
    }

    /**
     * Retorna a requisição do usuário
     *
     * @return Request
     */
    protected function getRequest() {
        return $this->request;
    }

    /**
     * @param $file
     * @param $folder
     *
     * @return string
     * @throws Exception
     */
    protected function fileResolver($file, $folder) {
        return $this->container->get('file_resolver')->handle($file, $folder);
    }

    /**
     * Retorna o caminho das imagens
     *
     * @param $folder
     *
     * @return mixed
     * @throws \Exception
     */
    protected function getRootImages($folder) {
        return $this->container->get('file_resolver')->getRoot($folder);
    }

    /**
     * Lê o conteúdo do arquivo
     *
     * @param string $file
     * @param string $folder
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function readResourceContent($file, $folder) {
        return $this->container->get('file_resolver')->read($file, $folder);
    }

    /**
     * Obtém IP do usuário
     *
     * @return string
     */
    protected function getUserIp() {
        $parameters = $this->request->server->all();

        if (!empty($parameters['HTTP_CLIENT_IP'])) {
            $ip = $parameters['HTTP_CLIENT_IP'];

        } else if (!empty($parameters['HTTP_X_FORWARDED_FOR'])) {
            $ip = $parameters['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $parameters['REMOTE_ADDR'];
        }

        return $ip;
    }
}
