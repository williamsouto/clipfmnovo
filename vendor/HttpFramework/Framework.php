<?php

abstract class Framework {

    /** @var  ContainerFramework */
    protected $container;

    protected $booted = false;

    public function handle(Request $request) {
        if (false === $this->booted) {
            $this->boot();
        }

        /** @var $httpFramework \HttpFramework*/
        return $this->getHttpFramework()->handle($request);
    }

    /**
     * Inicializa o conteúdo do Framework
     */
    private function boot() {
        if (!empty($this->container)) {
            return;
        }

        $this->container = new ContainerFramework();
        $this->container->inicializarContainer();
        $this->container->registerAppResources($this->registerServices());
    }

    /**
     * Retorna o HttpFramework do container
     *
     * @return HttpFramework| object
     * @throws Exception
     */
    private function getHttpFramework() {
        return $this->container->get('http_framework');
    }
}
