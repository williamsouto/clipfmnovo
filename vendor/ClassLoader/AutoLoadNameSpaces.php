<?php

$vendorDir = dirname(dirname(__FILE__));

return [
    "DependencyInjection" => [$vendorDir."/DependencyInjection"],

    "Exception"           => [$vendorDir."/Exception"],

    "File"                => [$vendorDir."/File"],

    "Helper"              => [$vendorDir."/Helper"],

    "HttpFoundation"      => [$vendorDir."/HttpFoundation"],

    "HttpFramework"       => [
        $vendorDir."/HttpFramework",
        $vendorDir."/HttpFramework/Controller",
        $vendorDir."/HttpFramework/Exception",
        $vendorDir."/HttpFramework/View",
        $vendorDir."/HttpFramework/Model"
    ],

    "HttpRequest"         => [$vendorDir."/HttpRequest"],

    "Persistence"         => [$vendorDir."/Persistence"],

    "Route"               => [$vendorDir."/Route"],

    "Security"            => [$vendorDir."/HttpFramework", $vendorDir."/Security", $vendorDir."/Security/Session"],

    "Util"                => [$vendorDir."/Util"],

    "Validate"            => [$vendorDir."/Validate"],
];
