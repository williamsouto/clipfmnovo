<?php

class AutoLoadReal {

    private static $loader;

    public static function loadClassLoader($class) {
        if (__DIR__.'ClassLoader.php' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    public static function getLoader() {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(['AutoLoadReal', 'loadClassLoader'], true, true);

        require_once __DIR__.'/../ClassLoader/ClassLoader.php';

        self::$loader = $loader = new ClassLoader();
        spl_autoload_unregister(['AutoLoadReal', 'loadClassLoader']);

        $map = require __DIR__ . '/AutoLoadNameSpaces.php';
        foreach ($map as $namespace => $path) {
            $loader->set($namespace, $path);
        }

        $classMap = require __DIR__ . '/AutoLoadClassMap.php';
        if ($classMap) {
            $loader->addClassMap($classMap);
        }

        $loader->register(true);
        return $loader;
    }
}
