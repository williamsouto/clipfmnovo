<?php

class ArrayHelper {

    /**
     * Constrói um array associativo com apenas as chaves passadas como parâmetros
     *
     * @param array $array array com todos os valores a ser filtrado
     * @param array $arrayOnly apenas os valores que serão retornados
     *
     * @return array
     */
    public static function only($array, $arrayOnly) {
        $only = [];

        foreach ($arrayOnly as $key) {
            if (key_exists($key, $array)) {
                $only[$key] = $array[$key];
            }
        }

        return $only;
    }

    /**
     * Realiza as mudanças de chaves para o array informado
     *
     * @param array $array Estrutura que sofrerá as mudanças de chave
     * @param array $keys  Array associativo que irá conter como chave, a chave atual do array que sofrerá mudança, e o valor é a nova chave
     *
     * @return mixed
     */
    public static function keyChange($array, $keys) {
        array_map(function ($key) use (&$array, $keys) {
            $value = $keys[$key];
            if (array_key_exists($key, $array)) {
                $array[$value] = $array[$key];
                unset($array[$key]);
            }
        }, array_keys($keys));

        return $array;
    }

    /**
     * Agrupa uma coleção de dados pela chave informada por parâmetro
     *
     * @param string $key   Chave para agrupar a coleção
     * @param array  $array Coleção que será agrupada
     *
     * @return array
     */
    public static function groupByKey($key, $array) {
        $colection = [];
        foreach ($array as $item) {
            $colection[$item[$key]][] = $item;
        }

        return $colection;
    }

    /**
     * Formata a data que está contido na coleção, para o formato desejado
     *
     * @param array  $collection Coleção de dados
     * @param string $key Chave onde esta mapeada a data na coleção
     * @param string $format Formato da data
     * @param bool   $unique
     */
    public static function collectionDateFormat(&$collection, $key, $format, $unique = false) {
        if ($unique) {
            $collection = [$collection];
        }

        $collection = array_map(function($data) use ($key, $format) {
            $data[$key] = Utils::dateFormat($data[$key], $format);
            return $data;
        }, $collection);

        $collection = $unique ? array_shift($collection) : $collection;
    }
}
