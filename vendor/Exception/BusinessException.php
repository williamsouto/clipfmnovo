<?php

class BusinessException extends Exception implements ExceptionInterface {

    protected $statusCode;
    protected $header;

    public function __construct($message = "", Throwable $previous = null) {
        parent::__construct($message, Response::STATUS_INTERNAL_SERVER_ERROR, $previous);
        $this->header['X-Status-Code'] = Response::STATUS_INTERNAL_SERVER_ERROR;
    }

    /**
     * Seta o status code para ser usado no response
     *
     * @param $status
     */
    public function setStatusCode($status) {
        $this->statusCode = $status;
    }

    /**
     * Retorna o status code
     *
     * @return int
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * Seta o valor no header
     *
     * @param $key
     * @param $value
     */
    public function setHeader($key, $value) {
        $this->header[$key] = $value;
    }

    /**
     * Retorna o valor do header de acordo com a chave passada
     *
     * @param $key
     * @return mixed
     */
    public function getHeader($key) {
        return $this->header[$key];
    }

    /**
     * Verifica se a chave passada, existe no header do Exception
     *
     * @param $key
     * @return bool
     */
    public function hasHeader($key) {
        return array_key_exists($key, $this->header);
    }
}
