<?php

interface ExceptionInterface {

    /**
     * Seta o status code para ser usado no response
     *
     * @param $status
     */
    public function setStatusCode($status);

    /**
     * Retorna o status code
     *
     * @return int
     */
    public function getStatusCode();

    /**
     * Seta o valor no header
     *
     * @param $key
     * @param $value
     */
    public function setHeader($key, $value);

    /**
     * Retorna o valor do header de acordo com a chave passada
     *
     * @param $key
     * @return mixed
     */
    public function getHeader($key);

    /**
     * Verifica se a chave passada, existe no header do Exception
     *
     * @param $key
     * @return bool
     */
    public function hasHeader($key);
}
