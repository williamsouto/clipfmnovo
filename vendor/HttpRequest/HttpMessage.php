<?php

class HttpMessage {

    /** @var  string */
    private $apiUrl;

    /** @var  string */
    private $method;

    /** @var  array parâmetros do cabeçalho da requisição */
    private $header = [];

    /** @var  string Recurso solicitado */
    private $path;

    /** @var  resource */
    private $resource;

    /** @var bool Indicador se a api recebe parâmetros em json */
    private $json = false;

    /** @var  array*/
    public $requisitionInfo;

    /** @var null|string Certificado para usar em requisições https */
    public $ssh = null;

    /**
     * Constrói a querystring
     *
     * @param string $path  Recurso solicitado
     * @param array $params Parâmetros para passar na requisição
     *
     * @return string
     */
    public function prepareQueryString($path, array &$params = []) {
        if (!$params) {
            return $path;
        }

        $path .= false === strpos($path, '?') ? '?' : '&';
        $path .= http_build_query($params, '', '&');

        return $path;
    }

    /**
     * Normaliza a url da api com o recurso solicitado
     *
     * @param string $path Recurso solicitado
     * @return string
     */
    private function prepareUrl($path) {
        return $this->apiUrl.'/'.ltrim($path, '/');
    }

    /**
     * Seta o metodo solicitado
     *
     * @param string $method Tipo de requisição {Verbo}
     * @return $this
     */
    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }

    /**
     * Seta os parâmetros do cabeçalho da requisição
     *
     * @param $header
     * @return $this
     */
    public function setHeader($header) {
        foreach ($header as $key => $value) {
            $this->header[] = $key.': '.$value;
        }
        return $this;
    }

    /**
     * Seta o host da api
     *
     * @param string $url Host da api
     * @param string $ssh Certificado ssh
     *
     * @return $this
     */
    public function setUrl($url, $ssh) {
        $this->apiUrl = $url;
        $this->ssh    = !empty($ssh) ? $ssh : null;
        return $this;
    }

    /**
     * Seta o recurso a ser requisitado
     *
     * @param $path
     * @return $this
     */
    public function setPath($path) {
        $this->path = $this->prepareUrl($path);
        return $this;
    }

    /**
     * Seta os parametros para a requisição
     *
     * @param $parameters
     */
    public function setParameters($parameters) {
        if ($this->json) {
            $parameters = json_encode($parameters);
            $this->setHeader(['Content-Type' => 'application/json']);
        }

        if (!empty($parameters) && $this->method == HttpRequest::POST) {
            curl_setopt($this->resource,CURLOPT_POST,true);
            curl_setopt($this->resource,CURLOPT_POSTFIELDS, $parameters);
        }
    }

    /**
     * Seta o indicador que informa que a requisição é em Json
     *
     * @param bool true|false
     * @return $this
     */
    public function setContentType($indicador) {
        $this->json = $indicador;
        return $this;
    }

    /**
     * Retorna o indicador que valida se a requisição usou json
     *
     * @return bool
     */
    public function json() {
        return $this->json;
    }

    /**
     * Inicializa o recurso Curl do php
     */
    private function initiateResource() {
        $this->resource = curl_init($this->path);
    }

    /**
     * Finaliza o recurso cURL do php
     */
    private function finalizeResource() {
        curl_close($this->resource);
        $this->header = [];
    }

    /**
     * Configura a requisição com os parâmetros padrões
     */
    private function setUpRequisition() {
        curl_setopt($this->resource, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($this->resource, CURLINFO_HEADER_OUT , 1);
        curl_setopt($this->resource, CURLOPT_RETURNTRANSFER, 1);

        if ($this->ssh) {
            curl_setopt($this->resource, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($this->resource, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($this->resource, CURLOPT_CAINFO, $this->ssh);
        }

        if (HttpRequest::POST == $this->method) {
            curl_setopt($this->resource, CURLOPT_POST, 1);
        }
    }

    /**
     * Retorna a resposta da requisição
     *
     * @return mixed
     */
    private function exec() {
        $response = curl_exec($this->resource);
        $this->requisitionInfo = curl_getinfo($this->resource);

        return $response;
    }

    /**
     * Envia a requisição para a api configurada
     *
     * @param $params
     * @return mixed
     */
    public function send(array $params) {
        $this->initiateResource();
        $this->setParameters($params);
        $this->setUpRequisition();

        $response = $this->exec();
        $this->finalizeResource();

        return $response;
    }
}
