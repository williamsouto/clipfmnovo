<?php

class HttpRequest {

    const GET     = 'GET';
    const POST    = 'POST';
    const PUT     = 'PUT';
    const DELETE  = 'DELETE';

    /** @var  HttpRequest */
    private static $instance;

    /** @var HttpMessage  */
    private $http;

    /** @var array  */
    private $header = [];

    /**
     * Obtém uma instância de HttpRequest
     *
     * @return HttpRequest
     */
    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new HttpRequest();
        }

        return self::$instance;
    }

    public function __construct() {
        $this->http = new HttpMessage();
    }

    /**
     * Seta as configurações do cabeçalho da requisição
     *
     * @param array $header
     * @return $this
     */
    public function header(array $header) {
        foreach ($header as $key => $value) {
            $this->header[$key] = $value;
        }

        return $this;
    }

    /**
     * Retorna todos os parâmetros do header da requisição
     *
     * @return array
     */
    private function getHeaders() {
        return $this->header;
    }

    /**
     * Solicita a requisição do tipo GET
     *
     * @param string $url Host da api
     * @param string $path Recurso a ser solicitado
     * @param array $parameters Array de parâmetros a ser passado na requisição
     * @param string $ssh Certificado ssh
     * @param bool $json Indicador de content/type: json
     *
     * @return array
     * @throws Exception
     */
    public function get($url, $path, array $parameters = [], $ssh = null, $json = false) {
        return $this->request($url, HttpRequest::GET, $path, $parameters, $ssh, $json);
    }

    /**
     * Solicita a requisição do tipo POST
     *
     * @param string $url Host da api
     * @param string $path Recurso a ser solicitado
     * @param array $parameters Array de parâmetros a ser passado na requisição
     * @param null $ssh Certificado SSH
     * @param bool $json Indicador de content/type: json
     *
     * @return array
     * @throws Exception
     */
    public function post($url, $path, array $parameters = [], $ssh = null, $json = false) {
        return $this->request($url, HttpRequest::POST, $path, $parameters, $ssh, $json);
    }

    /**
     * Configura e solicita a requisição
     *
     * @param string $url Host da api
     * @param string $method Tipo de método da requisição (Verbo)
     * @param string $path Recurso a ser chamado
     * @param array $parameters Dados a ser enviados
     * @param null $ssh Certificado ssh para conexões https
     * @param bool $json Indicador para informar se é content-type = Json
     *
     * @return array Parametros da resposta da requisição
     * @throws Exception
     */
    public function request($url, $method, $path, array $parameters, $ssh = null, $json = false) {
        if (HttpRequest::GET === $method) {
            $path = $this->http->prepareQueryString($path, $parameters);
        }

        $response = $this->http
            ->setMethod($method)
            ->setContentType($json)
            ->setUrl($url, $ssh)
            ->setPath($path)
            ->setHeader($this->getHeaders())
            ->send($parameters)
        ;

        $statusCodes = [
            Response::STATUS_BAD_REQUEST,
            Response::STATUS_UNAUTHORIZED,
            Response::STATUS_FORBIDDEN,
            Response::STATUS_NOT_FOUND,
            Response::STATUS_TOO_MANY_REQUESTS,
            Response::STATUS_INTERNAL_SERVER_ERROR,
            Response::STATUS_SERVICE_UNAVAILABLE,
        ];

        if (in_array($this->http->requisitionInfo['http_code'], $statusCodes)) {
            throw new Exception('Serviço indisponível no momento.');
        }

        return json_decode($response, true);
    }
}
