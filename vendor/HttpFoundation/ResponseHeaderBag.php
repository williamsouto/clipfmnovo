<?php

class ResponseHeaderBag extends HeaderBag {

    /**
     * @var array
     */
    protected $headerNames = [];

    /**
     * Constructor.
     *
     * @param array $headers An array of HTTP headers
     */
    public function __construct(array $headers = []) {
        parent::__construct($headers);

        if (!isset($this->headers['cache-control'])) {
            $this->set('Cache-Control', '');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key) {
        parent::remove($key);

        $uniqueKey = str_replace('_', '-', strtolower($key));
        unset($this->headerNames[$uniqueKey]);
    }

    /**
     * Retorna o header, com capitalização original.
     *
     * @return array Um array com o header
     */
    public function allPreserveCase() {
        return $this->headers;
    }
}
