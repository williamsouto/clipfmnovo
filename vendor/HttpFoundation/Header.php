<?php

class Header extends Parameters implements \IteratorAggregate, \Countable {

    protected $headers = [];

    /**
     * Returns a header value by name.
     *
     * @param string $key     The header name
     * @param mixed  $default The default value
     * @param bool   $first   Whether to return the first value or all header values
     *
     * @return string|array The first header value if $first is true, an array of values otherwise
     */
    public function get($key, $default = null, $first = true) {
        $key = str_replace('_', '-', strtolower($key));

        if (!array_key_exists($key, $this->headers)) {
            if (null === $default) {
                return $first ? null : [];
            }

            return $first ? $default : [$default];
        }

        if ($first) {
            return count($this->headers[$key]) ? $this->headers[$key][0] : $default;
        }

        return $this->headers[$key];
    }

    /**
     * Seta o parametro no header.
     *
     * @param string       $key     Chave
     * @param string|array $values  O valor da parametro
     * @param bool         $replace Flag que indica se pode atualizar o valor caso exista
     */
    public function set($key, $values, $replace = true) {
        $key = str_replace('_', '-', strtolower($key));

        $values = array_values((array) $values);

        if (true === $replace || !isset($this->headers[$key])) {
            $this->headers[$key] = $values;
        } else {
            $this->headers[$key] = array_merge($this->headers[$key], $values);
        }
    }

    /**
     * Retorna todos os headers
     *
     * @return array Um array com os headers
     */
    public function all() {
        return $this->headers;
    }

    /**
     * Returns an iterator for parametros.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator() {
        return new \ArrayIterator($this->headers);
    }

    /**
     * Returns the number of parametros.
     *
     * @return int The number of parametros
     */
    public function count() {
        return count($this->headers);
    }
}
