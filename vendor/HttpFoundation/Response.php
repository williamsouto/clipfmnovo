<?php

/**
 * Response representa uma resposta HTTP.
 *
 */
class Response {

    const STATUS_OK                    = 200;
    const STATUS_BAD_REQUEST           = 400;
    const STATUS_UNAUTHORIZED          = 401;
    const STATUS_FORBIDDEN             = 403;
    const STATUS_NOT_FOUND             = 404;
    const STATUS_UNPROCESSABLE_ENTITY  = 422;
    const STATUS_TOO_MANY_REQUESTS     = 429;
    const STATUS_INTERNAL_SERVER_ERROR = 500;
    const STATUS_SERVICE_UNAVAILABLE   = 503;

    /**
     * Status codes translation table.
     *
     * The list of codes is complete according to the
     * {@link http://www.iana.org/assignments/http-status-codes/ Hypertext Transfer Protocol (HTTP) Status Code Registry}
     * (last updated 2016-03-01).
     *
     * Unless otherwise noted, the status code is defined in RFC2616.
     *
     * @var array
     */
    public static $statusTexts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        421 => 'Misdirected Request',                                         // RFC7540
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        451 => 'Unavailable For Legal Reasons',                               // RFC7725
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',                                     // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    ];

    /** @var array */
    protected $data;

    /**
     * @var ResponseHeaderBag
     */
    public $headers;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusText;

    /**
     * Constructor.
     *
     * @param mixed $content O conteudo da resposta
     * @param int   $status  O código do status da resposta
     * @param array $headers Um array de headers
     *
     * @throws \InvalidArgumentException When the HTTP status code não é válido
     */
    public function __construct($content = '', $status = 200, $headers = []) {
        $this->headers = new ResponseHeaderBag($headers);
        $this->setContent($content);
        $this->setStatusCode($status);

        /* RFC2616 - 14.18 says all Responses need to have a Date */
        if (!$this->headers->has('Date')) {
            $this->setDate(\DateTime::createFromFormat('U', time()));
        }
    }

    /**
     * Seta o conteúdo do response.
     *
     * Valida o tipo se é uma string, número, null, e objeto que implementa o method __toString().
     *
     * @param mixed $content Content that can be cast to string
     *
     * @return $this
     *
     * @throws \UnexpectedValueException
     */
    public function setContent($content) {
        $this->content = (string)$content;
        return $this;
    }

    /**
     * Envia HTTP header e o conteúdo.
     *
     * @return $this
     */
    public function send() {
        $this->sendHeaders();
        $this->sendContent();

        return $this;
    }

    /**
     * Seta o status code da resposta.
     *
     * @param int   $code HTTP status code
     * @param mixed $text HTTP status text
     *
     * Se o texto do status é null isto irá preencher automaticamente o status code
     *
     * @return $this
     *
     * @throws \InvalidArgumentException Quando o status code do http não existe
     */
    public function setStatusCode($code, $text = null) {
        $this->statusCode = $code = (int) $code;
        if ($this->isInvalid()) {
            throw new \InvalidArgumentException(sprintf('The HTTP status code "%s" não é válido.', $code));
        }

        if (null === $text) {
            $this->statusText = isset(self::$statusTexts[$code]) ? self::$statusTexts[$code] : 'unknown status';

            return $this;
        }

        if (false === $text) {
            $this->statusText = '';
            return $this;
        }

        $this->statusText = $text;
        return $this;
    }

    /**
     * Envia HTTP headers.
     *
     * @return $this
     */
    public function sendHeaders() {
        // Verifica se o header já foi enviado
        if (headers_sent()) {
            return $this;
        }

        /* RFC2616 - 14.18 diz Toda resposta necessita ter uma data */
        if (!$this->headers->has('Date')) {
            $this->setDate(\DateTime::createFromFormat('U', time()));
        }

        // headers
        foreach ($this->headers->allPreserveCase() as $name => $values) {
            foreach ($values as $value) {
                header($name.': '.$value, false, $this->statusCode);
            }
        }

        // status
        header(sprintf('HTTP/1.0 %s %s', $this->statusCode, $this->statusText), true, $this->statusCode);
        return $this;
    }

    /**
     * Envia o conteúdo para o corrente web response.
     *
     * @return $this
     */
    public function sendContent() {
        echo $this->content;
        return $this;
    }

    /**
     * Retorna o conteúdo da resposta
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * É uma resposta válida?
     *
     * @return bool
     *
     */
    public function isInvalid() {
        return $this->statusCode < 100 || $this->statusCode >= 600;
    }

    /**
     * É um cliente error?
     *
     * @return bool
     */
    public function isClientError() {
        return $this->statusCode >= 400 && $this->statusCode < 500;
    }

    /**
     * Foi um erro do servidor?
     *
     * @return bool
     */
    public function isServerError() {
        return $this->statusCode >= 500 && $this->statusCode < 600;
    }

    /**
     * É um response de redirecionamento de um form?
     *
     * @param string $location
     *
     * @return bool
     */
    public function isRedirect($location = null) {
        return in_array($this->statusCode, [201, 301, 302, 303, 307, 308]) && (null === $location ?: $location == $this->headers->get('Location'));
    }

    /**
     * Seta o data header.
     *
     * @param \DateTime $date A \DateTime instance
     *
     * @return $this
     */
    public function setDate(\DateTime $date) {
        $date->setTimezone(new \DateTimeZone('UTC'));
        $this->headers->set('Date', $date->format('D, d M Y H:i:s').' GMT');

        return $this;
    }

    /**
     * Seta uma string contendo um documento json para ser enviado.
     *
     * @param string $json
     *
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function setJson($json) {
        $this->data = json_encode($json);
        return $this->update();
    }

    /**
     * Atualiza o conteúdo e acorda para um data Json.
     *
     * @return $this
     */
    private function update() {
        $this->headers->set('Content-Type', 'application/json');
        return $this->setContent($this->data);
    }
}
