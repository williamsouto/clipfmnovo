<?php

class Request {
    /**
     * Custom parameters.
     *
     * @var \Parameters()
     */
    public $attributes;

    /**
     * Request body parameters ($_POST).
     *
     * @var \Parameters()
     */
    public $request;

    /**
     * Request body parameters ($_FILES).
     *
     * @var \Parameters()
     */
    public $files;

    /**
     * Query string parameters ($_GET).
     *
     * @var \Parameters()
     */
    public $query;

    /**
     * Server and execution environment parameters ($_SERVER).
     *
     * @var Server
     */
    public $server;

    /**
     * Cookies ($_COOKIE).
     *
     * @var \Parameters()
     */
    public $cookies;

    /**
     * Headers (taken from the $_SERVER).
     *
     * @var Header
     */
    public $headers;

    /**
     * @var string|resource
     */
    protected $content;

    /** @var  string Informação do caminho **/
    protected $pathInfo = null;

    /** @var  string Informação do caminho **/
    protected $requestUri = null;

    /** * @var string */
    protected $baseUrl;

    protected static $requestFactory;

    /**
     * Constructor.
     *
     * @param array           $query      The GET parameters
     * @param array           $request    The POST parameters
     * @param array           $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array           $cookies    The COOKIE parameters
     * @param array           $server     The SERVER parameters
     * @param string|resource $content    The raw body data
     */
    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $server = [], $content = null) {
        $this->initialize($query, $request, $attributes, $cookies, $server, $content);
    }

    /**
     * Sets the parameters for this request.
     *
     * This method also re-initializes all properties.
     *
     * @param array $query      The GET parameters
     * @param array $request    The POST parameters
     * @param array $files      The FILES parameters
     * @param array $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array $cookies    The COOKIE parameters
     * @param array $server     The SERVER parameters
     * @param string|resource $content The raw body data
     */
    public function initialize(array $query = [], array $request = [], array $files = [], array $attributes = [], array $cookies = [], array $server = [], $content = null) {
        $this->request    = new Parameters($request);
        $this->query      = new Parameters($query);
        $this->files      = new Parameters($files);
        $this->attributes = new Parameters($attributes);
        $this->cookies    = new Parameters($cookies);
        $this->server     = new Server($server);
        $this->headers    = new Header($this->server->getHeaders());

        $this->content = $content;
    }


    public static function createFromGlobals() {
        // With the php's bug #66606, the php's built-in web server
        // stores the Content-Type and Content-Length header values in
        // HTTP_CONTENT_TYPE and HTTP_CONTENT_LENGTH fields.
        $server = $_SERVER;
        if ('cli-server' === PHP_SAPI) {
            if (array_key_exists('HTTP_CONTENT_LENGTH', $_SERVER)) {
                $server['CONTENT_LENGTH'] = $_SERVER['HTTP_CONTENT_LENGTH'];
            }
            if (array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) {
                $server['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
            }
        }

        $request = self::createRequestFromFactory($_GET, $_POST, $_FILES, [], $_COOKIE, $server);

        if (0 === strpos($request->headers->get('CONTENT_TYPE'), 'application/x-www-form-urlencoded')
            && in_array(strtoupper($request->server->get('REQUEST_METHOD', 'GET')), ['PUT', 'DELETE', 'PATCH'])) {
            parse_str($request->getContent(), $data);
            $request->request = new Parameters($data);
        }

        return $request;
    }

    private static function createRequestFromFactory(
        array $query      = [],
        array $request    = [],
        array $attributes = [],
        array $cookies    = [],
        array $server     = [],
        $content = null) {

        if (self::$requestFactory) {
            $request = call_user_func(self::$requestFactory, $query, $request, $attributes, $cookies, $server, $content);

            if (!$request instanceof self) {
                throw new \LogicException('A Fabrica de requisição deve retornar uma instância de HttpFoundation\Request.');
            }

            return $request;
        }

        return new static($query, $request, $attributes, $cookies, $server, $content);
    }

    /**
     * Retorna o conteúdo do corpo da requisição.
     *
     * @param bool $asResource If true, um recurso será retornado
     *
     * @return string|resource O conteúdo do corpo da requisição ou um recurso para ler o corpo do stream
     *
     * @throws \LogicException
     */
    public function getContent($asResource = false) {
        $currentContentIsResource = is_resource($this->content);
        if (PHP_VERSION_ID < 50600 && false === $this->content) {
            throw new \LogicException('getContent() Só pode ser chamado uma vez ao usar o tipo de retorno de recurso e PHP abaixo 5.6.');
        }

        if (true === $asResource) {
            if ($currentContentIsResource) {
                rewind($this->content);
                return $this->content;
            }

            // Content passed in parameter (test)
            if (is_string($this->content)) {
                $resource = fopen('php://temp', 'r+');
                fwrite($resource, $this->content);
                rewind($resource);

                return $resource;
            }

            $this->content = false;
            return fopen('php://input', 'rb');
        }

        if ($currentContentIsResource) {
            rewind($this->content);
            return stream_get_contents($this->content);
        }

        if (null === $this->content || false === $this->content) {
            $this->content = file_get_contents('php://input');
        }

        return $this->content;
    }

    /**
     * Retorna o caminho solicitado em relação ao script executado.
     *
     * A informação do caminho iniciando sempre com o /.
     *
     * @return string O caminho bruto
     */
    public function getPathInfo() {
        if (null === $this->pathInfo) {
            $this->pathInfo = $this->preparePathInfo();
        }

        return $this->pathInfo;
    }

    /**
     * Returna a requisição URI (caminho e query string).
     *
     * @return string URI
     */
    public function getRequestUri() {
        if (null === $this->requestUri) {
            $this->requestUri = $this->server->get('REQUEST_URI');
        }

        return $this->requestUri;
    }

    /**
     * Retorna o URL raiz do qual esta requisição é executada.
     *
     * O URL base nunca termina com um /.
     *
     * @return string A URL
     */
    public function getBaseUrl() {
        if (null === $this->baseUrl) {
            $this->baseUrl = $this->prepareBaseUrl();
        }

        return $this->baseUrl;
    }

    /**
     * Prepara a informação do caminho.
     *
     * @return string path info
     */
    protected function preparePathInfo() {
        $baseUrl = $this->getBaseUrl();

        if (null === ($requestUri = $this->getRequestUri())) {
            return '/';
        }

        // Remove the query string from REQUEST_URI
        if (false !== $pos = strpos($requestUri, '?')) {
            $requestUri = substr($requestUri, 0, $pos);
        }
        if ($requestUri !== '' && $requestUri[0] !== '/') {
            $requestUri = '/'.$requestUri;
        }

        $pathInfo = substr($requestUri, strlen($baseUrl));
        if (null !== $baseUrl && (false === $pathInfo || '' === $pathInfo)) {
            // If substr() returns false then PATH_INFO is set to an empty string
            return '/';
        } elseif (null === $baseUrl) {
            return $requestUri;
        }

        return (string) $pathInfo;
    }

    /**
     * Prepara a base URL.
     *
     * @return string
     */
    protected function prepareBaseUrl() {
        $filename = basename($this->server->get('SCRIPT_FILENAME'));

        if (basename($this->server->get('SCRIPT_NAME')) === $filename) {
            $baseUrl = $this->server->get('SCRIPT_NAME');
        } elseif (basename($this->server->get('PHP_SELF')) === $filename) {
            $baseUrl = $this->server->get('PHP_SELF');
        } elseif (basename($this->server->get('ORIG_SCRIPT_NAME')) === $filename) {
            $baseUrl = $this->server->get('ORIG_SCRIPT_NAME'); // 1and1 shared hosting compatibility
        } else {
            // Backtrack up the script_filename to find the portion matching
            // php_self
            $path = $this->server->get('PHP_SELF', '');
            $file = $this->server->get('SCRIPT_FILENAME', '');
            $segs = explode('/', trim($file, '/'));
            $segs = array_reverse($segs);
            $index = 0;
            $last = count($segs);
            $baseUrl = '';
            do {
                $seg = $segs[$index];
                $baseUrl = '/'.$seg.$baseUrl;
                ++$index;
            } while ($last > $index && (false !== $pos = strpos($path, $baseUrl)) && 0 != $pos);
        }

        // Does the baseUrl have anything in common with the request_uri?
        $requestUri = $this->getRequestUri();
        if ($requestUri !== '' && $requestUri[0] !== '/') {
            $requestUri = '/'.$requestUri;
        }

        if ($baseUrl && false !== $prefix = $this->getUrlencodedPrefix($requestUri, $baseUrl)) {
            // full $baseUrl matches
            return $prefix;
        }

        if ($baseUrl && false !== $prefix = $this->getUrlencodedPrefix($requestUri, rtrim(dirname($baseUrl), '/'.DIRECTORY_SEPARATOR).'/')) {
            // directory portion of $baseUrl matches
            return rtrim($prefix, '/'.DIRECTORY_SEPARATOR);
        }

        $truncatedRequestUri = $requestUri;
        if (false !== $pos = strpos($requestUri, '?')) {
            $truncatedRequestUri = substr($requestUri, 0, $pos);
        }

        $basename = basename($baseUrl);
        if (empty($basename) || !strpos(rawurldecode($truncatedRequestUri), $basename)) {
            // no match whatsoever; set it blank
            return '';
        }

        // If using mod_rewrite or ISAPI_Rewrite strip the script filename
        // out of baseUrl. $pos !== 0 makes sure it is not matching a value
        // from PATH_INFO or QUERY_STRING
        if (strlen($requestUri) >= strlen($baseUrl) && (false !== $pos = strpos($requestUri, $baseUrl)) && $pos !== 0) {
            $baseUrl = substr($requestUri, 0, $pos + strlen($baseUrl));
        }

        return rtrim($baseUrl, '/'.DIRECTORY_SEPARATOR);
    }

    /*
     * Returns the prefix as encoded in the string when the string starts with
     * the given prefix, false otherwise.
     *
     * @param string $string The urlencoded string
     * @param string $prefix The prefix not encoded
     *
     * @return string|false The prefix as it is encoded in $string, or false
     */
    private function getUrlencodedPrefix($string, $prefix) {
        if (0 !== strpos(rawurldecode($string), $prefix)) {
            return false;
        }

        $len = strlen($prefix);

        if (preg_match(sprintf('#^(%%[[:xdigit:]]{2}|.){%d}#', $len), $string, $match)) {
            return $match[0];
        }

        return false;
    }
}
