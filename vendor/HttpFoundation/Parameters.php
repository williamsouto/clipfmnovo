<?php

class Parameters implements \IteratorAggregate, \Countable {

    /**
     * Parameters armazenados.
     *
     * @var array
     */
    protected $parameters;

    /**
     * Constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters = []) {
        $this->parameters = $parameters;
    }

    /**
     * Retorna o nome do parametro.
     *
     * @param string $key     The key
     * @param mixed  $default O valor default se o chave não existir
     *
     * @return mixed
     */
    public function get($key, $default = null) {
        return array_key_exists($key, $this->parameters) ? $this->parameters[$key] : $default;
    }

    /**
     * Seta o valor associado a chave passada como parâmetro
     *
     * @param $key
     * @param $value
     */
    public function set($key, $value) {
        if (!empty($key)) {
            $this->parameters[$key] = $value;
        }
    }

    /**
     * Returns the parameters.
     *
     * @return array An array of parameters
     */
    public function all() {
        return $this->parameters;
    }

    /**
     * Retorna um iterator para os parameters.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator() {
        return new \ArrayIterator($this->parameters);
    }

    /**
     * Retorna o número de parameters
     *
     * @return int O número de parameters
     */
    public function count() {
        return count($this->parameters);
    }
}
