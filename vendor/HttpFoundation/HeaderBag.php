<?php

class HeaderBag implements \IteratorAggregate, \Countable {

    protected $headers = [];

    /**
     * Constructor.
     *
     * @param array $headers An array of HTTP headers
     */
    public function __construct(array $headers = []) {
        foreach ($headers as $key => $values) {
            $this->set($key, $values);
        }
    }

    /**
     * Retorna os headers.
     *
     * @return array Um array de headers
     */
    public function all() {
        return $this->headers;
    }

    /**
     * Retorna as chaves dos parâmetros
     *
     * @return array Um array de chaves dos parâmetros
     */
    public function keys() {
        return array_keys($this->headers);
    }

    /**
     * Adds new headers the current HTTP headers set.
     *
     * @param array $headers An array of HTTP headers
     */
    public function add(array $headers) {
        foreach ($headers as $key => $values) {
            $this->set($key, $values);
        }
    }

    /**
     * Retorna um valor do header por nome.
     *
     * @param string $key     The header name
     * @param mixed  $default The default value
     * @param bool   $first   Whether to return the first value or all header values
     *
     * @return string|array The first header value if $first is true, an array of values otherwise
     */
    public function get($key, $default = null, $first = true) {
        $key = str_replace('_', '-', strtolower($key));

        if (!array_key_exists($key, $this->headers)) {
            if (null === $default) {
                return $first ? null : array();
            }

            return $first ? $default : array($default);
        }

        if ($first) {
            return count($this->headers[$key]) ? $this->headers[$key][0] : $default;
        }

        return $this->headers[$key];
    }

    /**
     * Sets a header by name.
     *
     * @param string       $key     The key
     * @param string|array $values  The value or an array of values
     * @param bool         $replace Whether to replace the actual value or not (true by default)
     */
    public function set($key, $values, $replace = true) {
        $key = str_replace('_', '-', strtolower($key));

        $values = array_values((array) $values);

        if (true === $replace || !isset($this->headers[$key])) {
            $this->headers[$key] = $values;
        } else {
            $this->headers[$key] = array_merge($this->headers[$key], $values);
        }
    }

    /**
     * Retorna true se o HTTP header esta definido.
     *
     * @param string $key The HTTP header
     *
     * @return bool true se o parâmetro existe, senão false
     */
    public function has($key) {
        return array_key_exists(str_replace('_', '-', strtolower($key)), $this->headers);
    }

    /**
     * Remove um parâmetro do header.
     *
     * @param string $key Chave do header
     */
    public function remove($key) {
        $key = str_replace('_', '-', strtolower($key));

        unset($this->headers[$key]);
    }

    /**
     * Retorna true se Http Header contém o valor passado.
     *
     * @param string $key   The HTTP header name
     * @param string $value The HTTP value
     *
     * @return bool true se o valor esta contido no header, senão false
     */
    public function contains($key, $value) {
        return in_array($value, $this->get($key, null, false));
    }

    public function getIterator() {
        return new \ArrayIterator($this->headers);
    }

    /**
     * Retorna o número de headers
     *
     * @return int O número de headers
     */
    public function count() {
        return count($this->headers);
    }
}
