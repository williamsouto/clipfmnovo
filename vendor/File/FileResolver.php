<?php

class FileResolver extends Service {

    public function __construct(ContainerFramework $container) {
        parent::__construct($container);
    }

    /**
     * Carrega o arquivo e retorna o conteúdo
     *
     * @param $config string Pasta onde se encontra o arquivo
     * @param $file   string Nome do arquivo a ser carregado
     * @param string $extension
     *
     * @return mixed
     * @throws Exception
     */
    public function fileContent($config, $file, $extension = FileExtensions::JSON) {
        $configPath  = $this->getConfig($config);
        $fileContent = [];

        if (file_exists($path = $configPath.'/'.$file.$extension)) {
            $fileContent = file_get_contents($path);
        }

        return $this->preparingContent($fileContent, $extension);
    }

    /**
     * Prepara o conteúdo do arquivo para ser utilizado dependendo da extensão
     *
     * @param $content
     * @param $extension
     *
     * @return mixed
     */
    private function preparingContent($content, $extension) {
        if (!empty($content)) {
            switch ($extension) {
                case FileExtensions::JSON:
                    $content = json_decode($content, true);
                    break;
                default:
                    break;
            }
        }

        return $content;
    }

    /**
     * Valida o arquivo se é de extensão compatível e salva em um diretório parametrizado
     *
     * @param $file
     * @param $folder
     *
     * @return string
     * @throws Exception
     */
    public function handle($file, $folder) {
        if (!isset($file['error']) || is_array($file['error'])) {
            throw new FileException('Falha ao fazer upload do arquivo.');
        }

        if ($file['size'] > 1000000) {
            throw new FileException('Arquivo muito grande.');
        }

        if (!in_array(pathinfo($file['name'], PATHINFO_EXTENSION), FileExtensions::getMimes())) {
            throw new FileException('Extensão não é permitida');
        }

        $name = basename($file['name']);
        $root = $this->getConfig('public');

        $destination = $root. DS ."img". DS . "upload" . DS . $folder;

        if (!file_exists($destination. DS .$file['name'])) {
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }

            if (!is_writable($destination)) {
                $oldUmask = umask(0);
                chmod($destination, 0777);
                umask($oldUmask);
            }

            $name = uniqid().$name;
            $destination = $destination . DS . $name;

            if (!move_uploaded_file($file['tmp_name'], $destination)) {
                throw new FileException('Não foi possível salvar o arquivo');
            }
        }

        return $name;
    }

    /**
     * Lê o conteúdo do arquivo
     *
     * @param $file
     * @param $folder
     *
     * @throws \Exception
     */
    public function read($file, $folder) {
        $dir = $this->getConfig('public') . DS . 'img' . DS . 'upload' . DS . $folder. DS . $file;

        if (!is_file($dir)) {
            throw new FileException('Arquivo não existe');
        }

        header("Last-Modified: "  . gmdate("D, d M Y H:i:s", filemtime($dir)) . " GMT", true, 200);
        header("Content-Length: " . filesize($dir));
        header("Content-Type: "   . $this->getMimeType($dir));
        header("Render: PHP-Framework");

        ob_start();
        echo file_get_contents($dir);
        ob_end_flush();
    }

    /**
     * Retorna o caminho das imagens
     *
     * @param $folder
     *
     * @return string
     * @throws \Exception
     */
    public function getRoot($folder) {
        return $root = $this->getConfig('public').DS ."img". DS . "upload" . DS . $folder;
    }

    /**
     * Retorna o Mime de acordo com a extensão do arquivo
     *
     * @param      $file
     * @param bool $fromFileName
     *
     * @return string
     */
    public function getMimeType($file, $fromFileName = true) {
        $type = $file;

        if ($fromFileName) {
            preg_match("|\.([a-z0-9]{2,4})$|i", $file, $fileSuffix);

            $type = $fileSuffix[1];
        }

        switch (strtolower($type)) {
            case "js" :
                return "application/x-javascript";

            case "json" :
                return "application/json";

            case "jpg" :
            case "jpeg" :
            case "jpe" :

                return "image/jpg";

            case "png" :
            case "gif" :
            case "bmp" :
            case "tiff" :
                return "image/" . strtolower($type);

            case "css" :
                return "text/css";

            case "xml" :
                return "application/xml";

            case "doc" :
            case "docx" :
                return "application/msword";

            case "xls" :
            case "xlt" :
            case "xlm" :
            case "xld" :
            case "xla" :
            case "xlc" :
            case "xlw" :
            case "xll" :
                return "application/vnd.ms-excel";

            case "ppt" :
            case "pps" :
                return "application/vnd.ms-powerpoint";

            case "rtf" :
                return "application/rtf";

            case "pdf" :
                return "application/pdf";
            default :
                $fileSuffix = null;

                if ($fromFileName && function_exists("mime_content_type")) {
                    $fileSuffix = @mime_content_type($file);
                }

                return "unknown/" . (empty($fileSuffix) ? trim($type, ".") : $fileSuffix);
        }
    }
}
