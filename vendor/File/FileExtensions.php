<?php

class FileExtensions {

    const JSON = '.json';
    const PHP  = '.php';
    const XML  = '.xml';
    const TXT  = '.txt';

    /**
     * Retorna os mimes permitidos pela aplicação
     *
     * @return array
     */
    public static function getMimes() {
        return ['jpeg', 'jpg', 'png', 'gif'];
    }
}
