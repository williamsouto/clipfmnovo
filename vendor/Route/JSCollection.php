<?php

/**
 * Classe responsável por gerenciar os arquivos javascripts que são dependentes de cada view
 *
 * Class JSCollection
 */
class JSCollection {

    CONST ALL = 'ALL';
    public static $collectionsJS = [];

    /**
     * Adiciona a coleção javascript para ser carregada
     *
     * @param array  $collection Coleção de js que deverão ser carregados
     * @param string $path       Nome da pasta
     * @param string $views      Nome da view
     */
    public static function add(array $collection, $path, $views = JSCollection::ALL) {
        if (is_array($views) && count($views) > 0) {
            foreach ($views as $view) {
                static::$collectionsJS[$path][$view] = $collection;
            }
        }

        static::$collectionsJS[$path][$views] = $collection;
    }

    /**
     * Retorna a coleção de javascripts
     *
     * @param string $path Nome da pasta
     * @param string $view Nome da view
     *
     * @return array
     */
    public function getCollectionJS($path, $view) {
        if (!array_key_exists($path, static::$collectionsJS)) {
            return [];
        }

        $collections = !empty(static::$collectionsJS[$path][JSCollection::ALL]) ? static::$collectionsJS[$path][JSCollection::ALL] : [];

        if (array_key_exists($view, static::$collectionsJS[$path])) {
            $collections = array_merge(static::$collectionsJS[$path][$view], $collections);
        }

        return $collections;
    }
}
