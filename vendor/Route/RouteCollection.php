<?php

class RouteCollection {

    private static $routes;

    /**
     * Adiciona uma rota para o controller passado como parametro
     *
     * @param $class
     * @param Route $route
     */
    public static function add($class, Route $route) {
        self::$routes[$class][] = $route;
    }

    /**
     * Retorna todas as rotas adicionadas
     *
     * @return mixed
     */
    public function getAllRoutes() {
        return self::$routes;
    }

    /**
     * Retorna as rotas do controller
     *
     * @param $controller
     * @return null [Route]
     */
    public function getRoutes($controller) {
        return !empty(self::$routes[$controller]) ? self::$routes[$controller] : null;
    }
}
