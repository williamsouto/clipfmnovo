<?php

class Route {

    private $method;
    private $route;

    /**
     * Route constructor.
     *
     * @param $route  string Rota que será mapeada para o método em caso de match
     * @param $method string Nome do método que será chamado
     */
    public function __construct($route, $method) {
        $this->setRoute($route);
        $this->setMethod($method);
    }

    /**
     * Seta o método que será utilizado
     *
     * @param $method
     */
    private function setMethod($method) {
        $this->method = $method;
    }

    /**
     * Rota que será utilizado para fazer combinação
     *
     * @param $route
     */
    private function setRoute($route) {
        $this->route = $route;
    }

    /**
     * Retorna a rota
     *
     * @return mixed
     */
    private function getRoute() {
        return $this->route;
    }

    /**
     * Retorna o método mapeado para essa rota
     *
     * @return mixed
     */
    private function getMethod() {
        return $this->method;
    }

    /**
     * Processa a rota e identifica a correspodencia para um método
     *
     * @param  $method
     * @return array
     */
    public function match($method) {
        $request = $method;
        $route   = $this->getRoute();

        if ($method == '/' && $route == $method) {
            return ['method' => $this->getMethod(), 'parameters' => []];
        }

        $route = $route != '/' ? explode('/', $route) : [];

        $routeRequest = $match = $map = [];

        foreach ($route as $resource) {
            if (substr($resource, 0, 1) != '{') {
                $match[] = $resource;
            }
        }

        $lengthMatch = count($match);
        $method      = explode('/', $method);

        for ($i = 0; $i < $lengthMatch; $i++) {
            if (array_key_exists($i, $method)) {
                $routeRequest[] = $method[$i];
            }
        }

        $routeRequest = implode($routeRequest, '/');
        $match        = implode($match, '/');

        if ($routeRequest == $match && count($route) == count($method)) {
            $parameters = [];
            if (strlen($request) > strlen($routeRequest)) {
                $parameters = substr($request, strlen($routeRequest)+1, strlen($request));
                $parameters = explode('/', $parameters);
            }

            $map = [
                'method'     => $this->getMethod(),
                'parameters' => $parameters
            ];
        }

        return $map;
    }
}
