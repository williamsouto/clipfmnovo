<?php

interface IServiceExternal {

    /**
     * Seta o container do framework, tendo acesso a outros serviços
     *
     * @param $container \ContainerFramework
     *
     * @return mixed
     */
    public function setServicesFramework($container);
}
