<?php

class ContainerFramework {

    /**
     * @var array Recursos utilizados pelo framework
     */
    private $resource = [];

    public function inicializarContainer() {
        $this->resource['http_framework']      = new HttpFramework($this);
        $this->resource['view_resolver']       = new ViewResolver($this);
        $this->resource['controller_resolver'] = new ControllerResolver($this);
        $this->resource['session']             = new Session();
        $this->resource['security']            = new Security($this);
        $this->resource['file_resolver']       = new FileResolver($this);
    }

    /**
     * Retorna o recurso passado como parâmetro
     *
     * @param $resource
     *
     * @return Object
     * @throws \Exception
     */
    public function get($resource) {
        if (!$this->checkConfiguration($resource)) {
            throw new Exception(sprintf('Serviço %s não suportado.', $resource));
        }

        return $this->getResource($resource);
    }

    /**
     * Verifica se o recurso que esta sendo solicitado é suportado pelo framework
     *
     * @param $resource
     *
     * @return bool
     */
    private function checkConfiguration($resource) {
        return array_key_exists($resource, $this->resource);
    }

    /**
     * Retorna o recurso de acordo com a chave
     *
     * @param $key string
     *
     * @return mixed
     */
    private function getResource($key) {
        return (!empty($this->resource[$key])) ? $this->resource[$key] : null;
    }

    /**
     * Registrar os recursos da aplicação no container
     *
     * @param $resourcesApp array
     */
    public function registerAppResources($resourcesApp) {
        foreach ($resourcesApp as $key => $object) {
            $this->resource[$key] = $object;
        }
    }
}
