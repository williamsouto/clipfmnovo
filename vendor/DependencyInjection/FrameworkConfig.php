<?php

/**
 * Class FrameworkConfig
 *
 * Classe modelo para implementar o config da app
 */
class FrameworkConfig implements IFrameworkConfig {

    protected static $instance;

    /**
     * Cria uma instância de FrameworkConfig
     *
     * @return \FrameworkConfig
     */
    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Inicializa as configurações da aplicação
     *
     * @return array
     */
    public function initializeConfiguration() {
        return [];
    }
}
