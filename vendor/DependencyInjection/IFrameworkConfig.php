<?php


interface IFrameworkConfig {

    /**
     * Método responsável em carregar as configurações da aplicação que utiliza o framework
     *
     * @return mixed
     */
    public function initializeConfiguration();
}
