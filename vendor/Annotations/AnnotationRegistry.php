<?php

class AnnotationRegistry {

    /**
     * A map of autoloader callables.
     *
     * @var array
     */
    static private $loaders = [];

    /**
     * Registers an autoloading callable for annotations, much like spl_autoload_register().
     *
     * NOTE: These class loaders HAVE to be silent when a class was not found!
     * IMPORTANT: Loaders have to return true if they loaded a class that could contain the searched annotation class.
     *
     * @param callable $callable
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    static public function registerLoader($callable) {
        if (!is_callable($callable)) {
            throw new \InvalidArgumentException("A callable is expected in AnnotationRegistry::registerLoader().");
        }
        self::$loaders[] = $callable;
    }
}
