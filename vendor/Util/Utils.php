<?php

class Utils {

    const SEGUNDO = 's';
    const MINUTO  = 'm';
    const HORA    = 'h';
    const DIA     = 'd';
    const MES     = 'M';
    const ANO     = 'a';

    const INTEGER = 'integer';
    const STRING  = 'string';

    /**
     * Subtrai data1 com data2 e retorna o resultado em Seg|Min|Hora|Dia
     *
     * @param $initialDate
     * @param $finalDate
     * @param string $cast
     *
     * @return int|null
     */
    public static function diffDate($initialDate, $finalDate, $cast = 's') {
        $initialTime = strtotime($initialDate);
        $endTime     = strtotime($finalDate);

        $diff   = $endTime - $initialTime;
        $result = null;

        switch ($cast) {
            case Utils::SEGUNDO:
                $result = (int) floor($diff / ((60 * 60) / 60)) * 60;
                break;
            case Utils::MINUTO:
                $result = (int) floor($diff / ((60 * 60) / 60));
                break;
            case Utils::HORA:
                $result = (int) floor($diff / (60 * 60));
                break;
            case Utils::DIA:
                $result = (int) floor($diff / (60 * 60 * 24));
                break;
            case Utils::MES:
                $result = (int) floor($diff / (30 * 60 * 60 * 24));
                break;
            default:
                $result = (int) floor($diff / (365 * 60 * 60 * 24));
        }

        return $result;
    }

    /**
     * Cria um date e formata de acordo com padrão passado
     *
     * @param string $date   data que deseja formatar
     * @param string $format padrão que irá ser formatada a data
     *
     * @return false|string
     */
    public static function dateFormat($date, $format) {
        $dateCreate = date_create($date);
        return date_format($dateCreate, $format);
    }

    /**
     * Retorna a data atual
     *
     * @return false|string
     */
    public static function date() {
        return date('Y-m-d H:i:s');
    }

    /**
     * Retorna a hora atual
     */
    public static function hour() {
        return date('H:i:s');
    }

    /**
     * Retorna o dia atual da semana
     *
     * @return false|string
     */
    public static function weekDay() {
        $day = [
            0 => 'Domingo',
            1 => 'Segunda',
            2 => 'Terça',
            3 => 'Quarta',
            4 => 'Quinta',
            5 => 'Sexta',
            6 => 'Sabado',
        ];

        $unixTimestamp = strtotime(Utils::date());
        $dayOfWeek = date("w", $unixTimestamp);
        return $day[$dayOfWeek];
    }

    /**
     * Normaliza o valor para o tipo passado como parâmetro
     *
     * @param mixed  $value Valor a ser convertido
     * @param string $type  Tipo para converter
     *
     * @return int|string
     */
    public static function standardize($value, $type = Utils::INTEGER) {
        switch ($type) {
            case Utils::INTEGER:
                return (int) $value;
            default:
                return (string) $value;
        }
    }

    /**
     * Converte a estrutura de dados em json
     *
     * @param $data
     * @return string
     */
    public static function json($data) {
        return json_encode($data);
    }

    /**
     * Decodifica o json em uma estrutura de dados
     *
     * @param string $json  Data em formato json
     * @param bool   $assoc Indicador que informa se a decodificação deve ser em array associativo
     *
     * @return array
     */
    public static function deJson($json, $assoc = true) {
        return json_decode($json, $assoc);
    }

    /**
     * Converter entidades HTML especiais de volta para caracteres
     *
     * @param $html
     * @return string
     */
    public static function decodeHtml($html) {
        return htmlspecialchars_decode($html);
    }

    /**
     * Encripta dado passado como parâmetro
     *
     * @param string $data Dado a ser encriptado
     * @param string $hash Tipo de hash utilizado para encriptar
     *
     * @return string
     */
    public static function encrypt($data, $hash = 'md5') {
        $encript = null;

        switch ($hash) {
            case 'sha1':
                $encript = sha1($data);
                break;
            default:
                $encript = md5($data);
        }

        return $encript;
    }
}
