<?php

class UtilsDB {

    const SELECT = 's';
    const UPDATE = 'u';

    /**
     * Constrói clausula utilizada nas querys
     *
     * @param array  $parameters Dados que serão manipulados
     * @param string $tipo       Qual o tipo da query
     *
     * @return array|string
     */
    public static function buildParametersQuery(array $parameters, $tipo = self::SELECT) {
        $build = [];

        if ($tipo == self::UPDATE) {

            foreach ($parameters as $key) {
                $build[] = $key." = :".$key;
            }

            $build = implode(', ', $build);
        }

        return $build;
    }

    /**
     * Constrói a cláusula in para query
     *
     * @param string $column Nome da coluna
     * @param mixed  $data   Dados que irão ser utilizados na cláusula in
     *
     * @return string
     */
    public static function whereIn($column, $data) {
        $data = is_array($data) ? implode(', ', $data) : $data;
        return " {$column} IN ({$data}) ";
    }
}
