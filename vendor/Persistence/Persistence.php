<?php

class Persistence extends Service {

    private $host;
    private $dbsa;
    private $user;
    private $pass;
    private $port;
    private $bindings;

    CONST PRIMARY_KEY = 'PRI';
    CONST TYPE_SELECT = 's';
    CONST TYPE_UPDATE = 'u';
    CONST TYPE_INSERT = 'i';

    /** @var  Persistence */
    private static $instance;

    /** @var PDO */
    private $connect;

    /** @var string caminho das querys */
    private $queryDir;

    /** @var  int Quantidade de transações ativas */
    private $transactionCount;

    /**
     * Retorna a instancia da classe Persistence (Padrão Singleton)
     *
     * @return Persistence
     * @throws Exception
     */
    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new Persistence();
        }

        return self::$instance;
    }

    /**
     * Persistence constructor.
     *
     * @throws Exception
     */
    public function __construct() {
        parent::__construct();
        $this->loadParameters();
        $this->queryDir = $this->getConfig('default').'/query/';
        $this->transactionCount = 0;
    }

    /**
     * Carrega os parametros de conexão com o banco
     * @throws Exception
     */
    private function loadParameters() {
        $config = $this->getConfig('data_base');

        $this->host = $config['host'];
        $this->user = $config['user'];
        $this->pass = $config['pass'];
        $this->dbsa = $config['dbsa'];
        $this->port = $config['port'];
    }

    /**
     * Conecta com o banco de dados com o pattern singleton. Retorna um objeto PDO
     *
     * @return PDO
     */
    private function connect() {
        try {
            if (empty($this->connect)) {
                $dsn = 'mysql:host=' . $this->host . ';port='.$this->port.';dbname=' . $this->dbsa;
                $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'];
                $this->connect = new PDO($dsn, $this->user, $this->pass, $options);
            }
        } catch (PDOException $e) {
            throw $e;
        }

        $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->connect;
    }

    /**
     * Retorna uma instância de PDO
     *
     * @return PDO
     */
    private function getConnection() {
        return $this->connect();
    }

    /**
     * Inicializa uma transação no banco
     */
    public function beginTransaction() {
        if ($this->transactionCount == 0) {
            $connection = $this->getConnection();
            $connection->beginTransaction();
        }

        $this->transactionCount++;
    }

    /**
     * Finaliza a transação aberta no banco de dados
     */
    public function commit() {
        $this->transactionCount--;

        if ($this->transactionCount == 0) {
            $this->getConnection()->commit();
        }
    }

    /**
     * Finaliza uma transação aberta
     */
    public function rollback() {
        $this->transactionCount--;

        if ($this->transactionCount == 0) {
            $this->getConnection()->rollBack();
        }
    }

    /**
     * Carrega a query e executa
     *
     * @param string $file       Nome do arquivo que esta a query
     * @param array  $parameters Parâmetros utilizados na query
     * @param bool   $unique     Indica se o resultado deve ser unico
     * @param string $type       Tipo da query
     *
     * @return array
     * @throws Exception
     */
    public function query($file, $parameters = [], $unique = false, $type = Persistence::TYPE_INSERT) {
        $query  = $this->loadQuery($file, $parameters);
        $result = $this->executeQuery($query, $parameters, $type);

        return $unique ? array_shift($result) : $result;
    }

    /**
     * Carrega o arquivo que possui a consulta
     *
     * @param $file
     * @param $bindings
     *
     * @return string
     * @throws Exception
     */
    private function loadQuery($file, &$bindings) {
        $query = '';
        if (!file_exists($this->queryDir.$file.'.php')) {
            throw new Exception('Query não encontrada');
        }

        $this->bindings = $bindings;
        require $this->queryDir.$file.'.php';
        return $query;
    }

    /**
     * Constrói a query que será executada
     *
     * @param string       $table   Nome da tabela
     * @param array|string $columns Colunas da tabela que serão retornadas na consulta
     * @param array        $where   Condição da consulta
     * @param bool         $unique  Indicador que irá definir se a consulta deve retornar apenas um registro
     * @param bool|string  $orderBy Campo que irá ser levado em consideração para ordenar os registros
     * @param string|int   $limit   Limite de dados a ser buscado
     *
     * @return string
     */
    private function buildQuery($table, $columns, $where, $unique, $orderBy = "", $limit = '') {
        $clausuleWhere = "(1 = 1)";
        foreach ($where as $key => $value) {
            $clausuleWhere .= " AND {$key} = :{$key}";
        }

        if (!empty($orderBy)) {
            $orderBy = "ORDER BY " . $orderBy;
        }

        $limit = $unique ? 'LIMIT 1' : empty($limit) ? '' : 'LIMIT ' . $limit;

        return sprintf('SELECT ' . '%s FROM %s WHERE %s %s %s', $columns, $table, $clausuleWhere, $orderBy, $limit);
    }

    /**
     * Executa a query que foi cconstruída
     *
     * @param string $query  Query que será executada
     * @param array  $params Parâmetros utilizados na query
     * @param string $type   Tipo de query
     *
     * @return array|mixed
     */
    private function executeQuery($query, $params, $type = Persistence::TYPE_INSERT) {
        $result     = [];
        $connection = $this->getConnection();
        $stm        = $connection->prepare($query);

        if ($stm && $stm->execute($params) && $type == Persistence::TYPE_INSERT) {
            $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        }

        return $result;
    }

    /**
     * Busca os registros da tabela passada como parâmetro
     *
     * @param string $table
     * @param array  $where
     * @param string $columns
     * @param bool   $unique
     * @param string $orderBy
     * @param string $limit
     *
     * @return array
     */
    public function selectTable($table, $where = [], $columns = '*', $unique = false, $orderBy = '', $limit = '') {
        $query  = $this->buildQuery($table, $columns, $where, $unique, $orderBy, $limit);
        $result = $this->executeQuery($query, $where);
        return $unique ? array_pop($result) : $result;
    }

    /**
     * Insere os dados passados como parametros na tabela informada
     *
     * @param $table
     * @param $data
     *
     * @return string
     */
    public function insertTable($table, $data) {
        $columns = $this->selectTable('INFORMATION_SCHEMA.COLUMNS', [
            'TABLE_SCHEMA' => $this->dbsa,
            'TABLE_NAME'   => $table
        ]);

        $columns = array_filter($columns, function ($column) {
            return empty($column['EXTRA']);
        });

        $columns   = array_column($columns, 'COLUMN_NAME');
        $values    = [];
        $dataTable = [];

        foreach ($columns as $column) {
            $values[] = ':'.$column;
            $dataTable[$column] = isset($data[$column]) && $data[$column] !== '' ? $data[$column] : null;
        }

        $columnsName = implode(', ', $columns);
        $values      = implode(', ', $values);

        return $this->insert($table, $columnsName, $values, $dataTable);
    }

    /**
     * Atualiza os dados na tabela informada
     *
     * @param $table string Nome da tabela
     * @param $data  array  Valores a ser atualizados
     * @param $where array  Condição para atualizar
     */
    public function updateTable($table, $data, $where) {
        $columns = $this->selectTable('INFORMATION_SCHEMA.COLUMNS', [
            'TABLE_SCHEMA' => $this->dbsa,
            'TABLE_NAME'   => $table
        ]);

        $columns = array_column($columns, 'COLUMN_NAME');
        $dataColumns   = [];
        $clausuleWhere = '';

        foreach ($columns as $column) {
            if (array_key_exists($column, $data)) {
                $dataColumns[] = $column . ' = :'. $column;
            }
        }

        foreach ($where as $key => $value) {
            $clausuleWhere .= " AND {$key} = :{$key}";
        }

        $dataColumns = implode(', ', $dataColumns);
        $this->update($table, $dataColumns, $clausuleWhere, array_merge($data, $where));
    }

    /**
     * @param      $table
     * @param      $data
     * @param bool $increment
     *
     * @return int|null
     * @throws PersistenceException
     */
    public function insertOrupdate($table, $data, $increment = true) {
        $columns = $this->selectTable('INFORMATION_SCHEMA.COLUMNS', [
            'TABLE_SCHEMA' => $this->dbsa,
            'TABLE_NAME'   => $table
        ]);

        $insert = false;
        $where  = [];
        $id     = null;

        foreach ($columns as $column) {
            if ($column['COLUMN_KEY'] == Persistence::PRIMARY_KEY && !array_key_exists($column['COLUMN_NAME'], $data) && $increment) {
                $insert = true;
                break;
            } else if ($column['COLUMN_KEY'] == Persistence::PRIMARY_KEY && !array_key_exists($column['COLUMN_NAME'], $data)) {
                throw new PersistenceException('Chave primária não foi fornecida.');
            } else if ($column['COLUMN_KEY'] == Persistence::PRIMARY_KEY && array_key_exists($column['COLUMN_NAME'], $data) && $increment) {
                $where[$column['COLUMN_NAME']] = $data[$column['COLUMN_NAME']];
            }
        }

        if ($insert || !$increment) {
            $id = $this->insertTable($table, $data);
        } else {
            $this->updateTable($table, $data, $where);
        }

        return $id;
    }

    /**
     * Deleta o registro da tabela informada, de acordo com as condições
     *
     * @param $table string Nome da tabela
     * @param $where array  Condições para deletar registro
     */
    public function deleteTable($table, $where = []) {
        $clausuleWhere = '';
        foreach ($where as $key => $value) {
            $clausuleWhere .= " AND {$key} = :{$key}";
        }

        $query = sprintf("DELETE FROM %s WHERE (1 = 1) %s", $table, $clausuleWhere);
        $connection = $this->getConnection();
        $stm        = $connection->prepare($query);

        $stm->execute($where);
    }

    /**
     * Constrói o script de insert e insere na tabela os parametros informados
     *
     * @param string $table Nome da tabela
     * @param string $columns Nome das colunas que serão inseridas
     * @param string $values Valores das colunas que serão inseridas
     * @param array $data Parãmetros que serão salvos
     *
     * @return string
     */
    private function insert($table, $columns, $values, $data) {
        $query = sprintf("INSERT INTO %s (%s) VALUES (%s)", $table, $columns, $values);

        $connection = $this->getConnection();
        $stm        = $connection->prepare($query);

        $stm->execute($data);
        return $connection->lastInsertId();
    }

    /**
     * Constrói um script de update
     *
     * @param $table   string Nome da tabela
     * @param $columns string Nome das colunas que serão atualizddas
     * @param $where   string Condição para realizar o update
     * @param $data    array Dados da atualização
     */
    private function update($table, $columns, $where, $data) {
        $query = sprintf("UPDATE %s SET %s WHERE (1 = 1) %s", $table, $columns, $where);

        $connection = $this->getConnection();
        $stm        = $connection->prepare($query);

        $stm->execute($data);
    }
}
