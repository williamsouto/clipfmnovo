<?php

/**
 * Class Security
 *
 * Classe responsável em manipular e controlar a segurança da aplicação
 */

class Security extends Service {

    public function __construct($container) {
        parent::__construct($container);
    }

    /**
     * Verifica se o recurso solicitado necessita de autenticação de segurança
     *
     * @param  string $controller O nome do controller
     * @param  string $method O nome do método
     *
     * @return bool
     * @throws Exception
     */
    public function requiresAuthentication($controller, $method) {
        $routes = $this->container->get('file_resolver')->fileContent('config', 'security');

        if (key_exists($controller, $routes)) {
            $rule = $routes[$controller];

            if ($rule == '*' || (!is_array($rule) && $rule == $method) || (is_array($rule) && $rule[$method] == $method)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Registra a autenticação do usuário e salva na sessão
     *
     * @param int     $idUser Código identificador do usuário
     * @param         $user
     * @param Request $request
     *
     * @return mixed|string
     * @throws Exception
     */
    public function registerAuthenticated($idUser, $user, $request) {
        return $this->getSessionService()->register($idUser, $user, $request);
    }

    /**
     * Valida se o usuário está autenticado
     *
     * @param Request $request
     *
     * @return bool
     * @throws Exception
     */
    public function validateAuthentication($request) {
        return $this->getSessionService()->sessionValidate($request);
    }

    /**
     * Retorna as rotas configuradas que exigem login
     *
     * @return array
     * @throws Exception
     */
    private function getRoutes() {
        return $this->getConfig('authentication');
    }

    /**
     * Retorna o serviço de sessão
     *
     * @return Object|\Session
     * @throws Exception
     */
    private function getSessionService() {
        return $this->container->get('session');
    }

    /**
     * Filtra e remove todos os tipos de inject utilizado em ataques XSS(Cross-site scripting), SQL e CFRS
     *
     * @param $arguments
     * @return array
     */
    public function filter($arguments) {
        foreach ($arguments as $key => $value) {
            if (is_array($value)) {
                $arguments[$key]= $this->filter($value);
            } else {
                $arguments[$key] = $this->filterInject($value);
            }
        }

        return $arguments;
    }

    /**
     * Filtra o valor passado, removendo todos os scripts de js, php e as tags html
     *
     * @param $value
     * @return mixed|string
     */
    private function filterInject($value) {
        $value = preg_replace("/((<script(.*?))(.*?)(\/script>))/sim", "", $value);

        $allowTags = '<img><p><ol><ul><li><u><em><strong><s><sub><sup><blockquote><a><table><tboby><thead><tr><td><th><hr><iframe><span><h1><h2>';

        $value = strip_tags($value, $allowTags);
        $value = htmlspecialchars($value);

        $value = trim($value);
        return $value;
    }

    /**
     * Retorna os dados do usuário logado
     *
     * @return Object
     *
     * @throws Exception
     */
    public function getUserSessionData() {
        return $this->getSessionService()->getUserSessionData();
    }

    /**
     * Atualiza na sessão a data/hora do último acesso do usuário logado
     * @throws Exception
     */
    public function updateLastAccess() {
        $this->getSessionService()->updateLastAccess();
    }

    /**
     * Limpa os dados do usuário na sessão
     * @throws Exception
     */
    public function clearUserData() {
        $this->getSessionService()->clearUserData();
    }

    /**
     * Salva a rota para fazer o redirecionamento
     *
     * @param $controller
     * @param $method
     *
     * @throws \Exception
     */
    public function saveRedirect($controller, $method) {
        $this->getSessionService()->saveRedirect($controller, $method);
    }

    /**
     * Retorna a rota de redirecionamento
     *
     * @return mixed
     * @throws \Exception
     */
    public function getRedirect() {
        return $this->getSessionService()->getRedirect();
    }
}
