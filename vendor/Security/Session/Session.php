<?php

/**
 * Class Session
 *
 * Classe que representa a sessão do usuário
 */
class Session {

    /** @var  \ArrayObject */
    public static $userData;

    /** @var string Validação para identificar se usuário esta logado */
    private $validatonKey;

    public function __construct() {
        $this->validatonKey = 'user_authentication_1';
    }

    /**
     * Registra a autenticação do usuário na sessão
     *
     * @param int     $idUser
     * @param         $user
     * @param Request $request
     *
     * @return mixed|string
     */
    public function register($idUser, $user, $request) {
        $this->sessionStart();
        $this->setValueSession('user', $this->Encrypt('user_'.$idUser));
        $this->setValueSession('user_authentication', $this->Encrypt($this->validatonKey));
        $this->setValueSession('user_date', date('Y-m-d H:i:s'));
        $this->setValueSession('user_data', $user);

        $value = $this->generateToken($idUser, $request);
        setcookie('user_token', $value, time()+43200, '/');

        self::$userData = $user;

        $url = $this->getValueSession('route_redirect');
        return !empty($url) ? $url : '';
    }

    /**
     * Inicializa a sessão do usuário
     */
    public function sessionStart() {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    /**
     * Destrói a sessão do usuário
     */
    public function sessionDestroy() {
        session_destroy();
    }

    /**
     * Verifica se existe uma sessão para o usuário e esta autenticado
     *
     * @param Request $request
     *
     * @return bool
     */
    public function sessionValidate($request) {
        $this->sessionStart();

        if(!empty($user = $this->getValueSession('user')) && !empty($token = $request->cookies->get('user_token'))) {
            $user   = explode('_', $this->decrypt($user));
            $idUser = array_pop($user);
        } else {
            return false;
        }

        $expired       = (Utils::diffDate($this->getValueSession('user_date'), date('Y-m-d H:i:s'), Utils::MINUTO)) > 30;
        $authenticated = $this->decrypt($this->getValueSession('user_authentication')) == $this->validatonKey;
        $trueUser      = ($this->decrypt($this->getValueSession('user_authentication')) == $this->validatonKey) && ($this->generateToken($idUser, $request) == $token);
        return $authenticated && $trueUser && !$expired;
    }

    /**
     * Atualiza o ultimo acesso do usuário na sessão
     */
    public function updateLastAccess() {
        $this->sessionStart();
        $this->setValueSession('user_date', date('Y-m-d H:i:s'));
    }

    /**
     * Salva a rota de redirecionamento do usuário
     *
     * @param $controller
     * @param $method
     */
    public function saveRedirect($controller, $method) {
        $url = empty($method) || $method == '/' ? $controller : $controller . '/' . $method;
        $this->setValueSession('route_redirect', $url);
    }

    /**
     * Retorna a rota de redirecionamento
     *
     */
    public function getRedirect() {
        return $this->getValueSession('route_redirect');
    }

    /**
     * Limpa os dados do usuário na sessão
     */
    public function clearUserData() {
        $this->sessionStart();
        $this->destroyValueSession('user_data');
        $this->destroyValueSession('user_authentication');
        $this->destroyValueSession('user');
        $this->destroyValueSession('user_date');
    }

    /**
     * Encriptografa o valor passado como parâmetro
     *
     * @param string $key
     *
     * @return string
     */
    private function Encrypt($key) {
        return base64_encode($key);
    }

    /**
     * Descriptografa o parâmetro informado
     *
     * @param string $key
     *
     * @return string
     */
    private function decrypt($key) {
        return base64_decode($key);
    }

    /**
     * Gera um token apartir do código do usuário
     *
     * @param int $idUser
     * @param Request $request
     *
     * @return string
     */
    private function generateToken($idUser, $request) {
        return $this->Encrypt(join('#', array($idUser, $request->server->get('REMOTE_ADDR'), $request->server->get('HTTP_USER_AGENT'))));
    }

    /**
     * Seta o valor passado como parâmetro na sessão
     *
     * @param $key
     * @param $value
     */
    private function setValueSession($key, $value) {
        $_SESSION[$key] = $value;
    }

    /**
     * Apaga o dado da sessão de acordo com a chave passada
     *
     * @param $key
     */
    private function destroyValueSession($key) {
        unset($_SESSION[$key]);
    }

    /**
     * Retorna a sessão do usuário
     *
     * @return object
     */
    private function getSession() {
        return $_SESSION;
    }

    /**
     * Verifica se existe o campo na seção e retorna seu valor
     *
     * @param $key
     *
     * @return mixed
     */
    private function getValueSession($key) {
        return !empty($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    /**
     * Retorna os dados do usuário logado
     *
     * @return object
     */
    public static function getUserSessionData() {
        if (!isset($_SESSION)) {
            session_start();
        }

        return !empty($_SESSION['user_data'])? $_SESSION['user_data'] : null;
    }
}
