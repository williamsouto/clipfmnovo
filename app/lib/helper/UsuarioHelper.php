<?php

class UsuarioHelper extends Helper {

    /** @var  \Session Serviço de sessão */
    private $session;

    /**
     * Verifica se o usuário esta logado
     *
     * @return bool
     */
    public function usuarioLogado() {
        $usuarioLogado = Session::getUserSessionData();
        return !empty($usuarioLogado);
    }

    /**
     * Seta o serviço de sessão para ser utilizado
     *
     * @param $service
     */
    public function setSessionService($service) {
        $this->session = $service;
    }
}
