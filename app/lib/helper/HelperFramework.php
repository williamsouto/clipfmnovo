<?php

class HelperFramework implements IServiceExternal {

    /** @var array Pacote de helpers da aplicação */
    private $helpers = [];

    /** @var  \ContainerFramework */
    private $services;

    public function __construct() {
        $this->helpers = [
            'usuario' => new UsuarioHelper()
        ];
    }

    public function setServicesFramework($services) {
        $this->services = $services;
    }

    /**
     * Retorna todos os helpers da aplicação registrados
     *
     * @return array
     */
    public function getAllHelpers() {
        return $this->helpers;
    }

    /**
     * Retorna o helper solicitado
     *
     * @param $helper
     * @return mixed
     */
    public function get($helper) {
        if (!array_key_exists($helper, $this->helpers)) {
            throw new InvalidArgumentException(sprintf('Helper %s não foi encontrado', $helper));
        }

        return $this->helpers[$helper];
    }
}
