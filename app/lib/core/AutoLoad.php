<?php

require ROOT . '/vendor/Annotations/AnnotationRegistry.php';
require ROOT . '/vendor/ClassLoader/AutoLoadReal.php';

$loader = AutoLoadReal::getLoader();
$loader->registerClassMap(dirname(__FILE__).'/AutoLoadPersonalApp.php');

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
