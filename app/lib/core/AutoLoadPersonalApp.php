<?php

$appDir = dirname(dirname(dirname(__FILE__)));

return [
    "config"     => [$appDir."/config"],
    "controller" => [$appDir."/default/controller"],
    "model"      => [$appDir."/default/model"],
    "constant"   => [$appDir."/default/constant"],
    "coinbase"   => [$appDir."/lib/api/coinbase"],
    "helper"     => [$appDir."/lib/helper"]
];
