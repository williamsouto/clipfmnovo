CREATE DATABASE clipfm;

CREATE TABLE `categoria` (
  `id_categoria` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Código da categoria',
  `nome` VARCHAR(50) NOT NULL COMMENT 'Nome da categoria',
  `nivel` TINYINT(2) DEFAULT 1 NOT NULL COMMENT 'Nível da categoria',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data da cricação do registro',
  PRIMARY KEY (`id_categoria`)
) ENGINE = INNODB;

CREATE TABLE `programacao` (
  `id_programacao` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Código da programacao',
  `nome_programa` VARCHAR(50) NOT NULL COMMENT 'Nome do programa',
  `horario` TIME NOT NULL COMMENT 'Horário da programação',
  `dia` VARCHAR(10) NOT NULL COMMENT 'Dia da semana',
  `descricao` VARCHAR(500) NOT NULL COMMENT 'Breve descrição sobre o programa',
  `imagem` VARCHAR(30) NOT NULL COMMENT 'Imagem da programação',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data da criação do registro',
  PRIMARY KEY (`id_programacao`)
) ENGINE = INNODB;

CREATE TABLE `noticia` (
  `id_noticia` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Código da notícia',
  `titulo` VARCHAR(300) NOT NULL COMMENT 'Título da notícia',
  `texto` TEXT NOT NULL COMMENT 'Texto da notícia',
  `id_categoria` INT(11) UNSIGNED NOT NULL COMMENT 'Código da categoria',
  `autor` VARCHAR(30) NOT NULL COMMENT 'Quem escreveu o artigo',
  `imagem` VARCHAR(50) NOT NULL COMMENT 'Imagem do header da notícia',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data da cricação do registro',
  PRIMARY KEY (`id_noticia`),
  CONSTRAINT `fk_noticia_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria`(`id_categoria`)
) ENGINE = INNODB;

CREATE TABLE `noticia_like` (
  `id_noticia` INT(13) UNSIGNED NOT NULL COMMENT 'Código da Notícia',
  `ip_usuario` VARCHAR(45) NOT NULL COMMENT 'Ip do Usuário',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data do like',
  PRIMARY KEY (`id_noticia`, `ip_usuario`),
  UNIQUE (`id_noticia`, `ip_usuario`),
  CONSTRAINT `fk_noticia_like_noticia` FOREIGN KEY (`id_noticia`) REFERENCES `noticia`(`id_noticia`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `comentario` (
  `id_comentario` INT(13) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Código do Registro',
  `comentario` TEXT NOT NULL COMMENT 'Texto do Comentário',
  `ind_exibir` TINYINT(2) NOT NULL DEFAULT 1 COMMENT 'Indicador se deve exibir mensagem (0 = Não, 1 = Sim)',
  `id_noticia` INT(11) UNSIGNED NOT NULL COMMENT 'Código da Notícia',
  `autor` VARCHAR(30) NOT NULL COMMENT 'Autor do comentário',
  `email` VARCHAR(50) NOT NULL COMMENT 'Email do autor do comentário',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data Criação do Registro',
  PRIMARY KEY (`id_comentario`),
  CONSTRAINT `fk_comentario_id_noticia_noticia` FOREIGN KEY (`id_noticia`) REFERENCES `noticia`(`id_noticia`)
);

CREATE TABLE `comentario_like_dislike` (
  `id_noticia` INT(13) UNSIGNED NOT NULL COMMENT 'Código da Notícia',
  `id_comentario` INT(13) UNSIGNED NOT NULL COMMENT 'Código do Comentário',
  `ip_usuario` VARCHAR(45) NOT NULL COMMENT 'Ip do Usuário',
  `ind_like` TINYINT(2) NOT NULL DEFAULT 0 COMMENT 'Indicador de like (0 = Dislike, 1 = Like)',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data do like',
  PRIMARY KEY (`id_noticia`, `id_comentario`, `ip_usuario`),
  UNIQUE (`id_noticia`, `id_comentario`, `ip_usuario`),
  CONSTRAINT `fk_comentario_like_dislike_noticia` FOREIGN KEY (`id_noticia`) REFERENCES `noticia`(`id_noticia`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_comentario_like_dislike_comentario` FOREIGN KEY (`id_comentario`) REFERENCES `comentario`(`id_comentario`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `pedido` (
  `id_pedido` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Código do pedido',
  `email` VARCHAR(30) NOT NULL COMMENT 'Email do contato',
  `nome` VARCHAR(50) NOT NULL COMMENT 'Nome do contato',
  `pedido` VARCHAR(60) NOT NULL COMMENT 'Pedido do usuário',
  `ind_dashboard` TINYINT(2) DEFAULT 0 COMMENT 'Indicador esta exibindo o pedido no dashboard (0 = Não, 1 = Sim)',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data do pedido',
  PRIMARY KEY (`id_pedido`)
);

CREATE TABLE `contato` (
  `id_contato` INT(13) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Código do Registro',
  `nome` VARCHAR(50) NOT NULL COMMENT 'Nome da Pessoa',
  `email` VARCHAR(100) NOT NULL COMMENT 'Email de Contato',
  `assunto` VARCHAR(100) NOT NULL COMMENT 'Motivo do Contato',
  `mensagem` VARCHAR(600) NOT NULL COMMENT 'Mensagem',
  `ind_visto` TINYINT(2) DEFAULT 0 NOT NULL COMMENT 'Indicador Se Foi Vista a Mensagem (0 = Não, 1 = Sim)',
  `ind_dashboard` TINYINT(2) DEFAULT 0 COMMENT 'Indicador esta exibindo o contato no dashboard (0 = Não, 1 = Sim)',
  `data_visualizacao` DATETIME DEFAULT NULL COMMENT 'Data da Visualização do Registro',
  `data_criacao` DATETIME NOT NULL COMMENT 'Data da Criação do Registro',
  PRIMARY KEY (`id_contato`)
);
