<?php

RouteCollection::add(PageController::class, new Route('modal', 'renderModal'));
RouteCollection::add(PageController::class, new Route('dialog', 'renderDialog'));

RouteCollection::add(HomeController::class, new Route('/', 'index'));

RouteCollection::add(SiteController::class, new Route('/', 'site'));
RouteCollection::add(SiteController::class, new Route('radio', 'radio'));
RouteCollection::add(SiteController::class, new Route('programacao', 'programacao'));
RouteCollection::add(SiteController::class, new Route('noticias', 'noticias'));
RouteCollection::add(SiteController::class, new Route('promocoes', 'promocoes'));
RouteCollection::add(SiteController::class, new Route('equipe', 'equipe'));

RouteCollection::add(AdministradorController::class, new Route('/', 'index'));

RouteCollection::add(AdministradorController::class, new Route('carousel', 'carousel'));
RouteCollection::add(AdministradorController::class, new Route('carousel/buscar', 'buscarCarousels'));
RouteCollection::add(AdministradorController::class, new Route('carousel/buscar/{id_carousel}', 'buscarCarousels'));
RouteCollection::add(AdministradorController::class, new Route('carousel-tipo/buscar', 'buscarTiposCarousel'));
RouteCollection::add(AdministradorController::class, new Route('carousel-tipo/buscar/{id_carousel_tipo}', 'buscarTiposCarousel'));
RouteCollection::add(AdministradorController::class, new Route('carousel/salvar', 'salvarCarousel'));

RouteCollection::add(AdministradorController::class, new Route('noticia', 'noticia'));
RouteCollection::add(AdministradorController::class, new Route('noticia/buscar', 'buscarNoticias'));
RouteCollection::add(AdministradorController::class, new Route('noticia/buscar/termo', 'buscarNoticiasTermo'));
RouteCollection::add(AdministradorController::class, new Route('noticia/buscar/{id}', 'buscarNoticias'));
RouteCollection::add(AdministradorController::class, new Route('noticia/imagem', 'salvarImagemNoticia'));
RouteCollection::add(AdministradorController::class, new Route('noticia/imagem/buscar/{img}', 'buscarImagem'));
RouteCollection::add(AdministradorController::class, new Route('noticia/salvar', 'salvarNoticia'));

RouteCollection::add(AdministradorController::class, new Route('categoria', 'categoria'));
RouteCollection::add(AdministradorController::class, new Route('categoria/buscar', 'buscarCategoria'));
RouteCollection::add(AdministradorController::class, new Route('categoria/buscar/{id}', 'buscarCategoria'));
RouteCollection::add(AdministradorController::class, new Route('categoria/salvar', 'salvarCategoria'));

RouteCollection::add(AdministradorController::class, new Route('programacao', 'programacao'));
RouteCollection::add(AdministradorController::class, new Route('programacao/buscar', 'buscarProgramacao'));
RouteCollection::add(AdministradorController::class, new Route('programacao/buscar/{id}', 'buscarProgramacao'));
RouteCollection::add(AdministradorController::class, new Route('programacao/salvar', 'salvarProgramacao'));

RouteCollection::add(AdministradorController::class, new Route('usuario', 'usuario'));
RouteCollection::add(AdministradorController::class, new Route('usuario/buscar', 'buscarUsuarios'));
RouteCollection::add(AdministradorController::class, new Route('usuario/buscar/{id}', 'buscarUsuarios'));
RouteCollection::add(AdministradorController::class, new Route('usuario/salvar', 'salvarUsuario'));

RouteCollection::add(AdministradorController::class, new Route('parceiro', 'parceiro'));
RouteCollection::add(AdministradorController::class, new Route('parceiro/buscar', 'buscarParceiros'));
RouteCollection::add(AdministradorController::class, new Route('parceiro/buscar/{id}', 'buscarParceiros'));
RouteCollection::add(AdministradorController::class, new Route('parceiro/salvar', 'salvarParceiro'));

RouteCollection::add(AdministradorController::class, new Route('comentario', 'comentario'));
RouteCollection::add(AdministradorController::class, new Route('comentario/buscar', 'buscarComentarios'));
RouteCollection::add(AdministradorController::class, new Route('comentario/buscar/{id}', 'buscarComentarios'));
RouteCollection::add(AdministradorController::class, new Route('comentario/aprovar', 'aprovarComentario'));

RouteCollection::add(AdministradorController::class, new Route('contato', 'contato'));
RouteCollection::add(AdministradorController::class, new Route('contato/buscar', 'buscarContatos'));
RouteCollection::add(AdministradorController::class, new Route('contato/buscar/{id}', 'buscarContatos'));
RouteCollection::add(AdministradorController::class, new Route('contato/dashboard/{limite}', 'buscarContatosDashboard'));
RouteCollection::add(AdministradorController::class, new Route('contato/visualizar', 'visualizarContato'));
RouteCollection::add(AdministradorController::class, new Route('contato/visualizar-dashboard', 'visualizarContatoDashboard'));
RouteCollection::add(AdministradorController::class, new Route('contato/deletar', 'deletarContato'));

RouteCollection::add(AdministradorController::class, new Route('pedido/buscar/{limite}', 'buscarPedidos'));
RouteCollection::add(AdministradorController::class, new Route('pedido/deletar', 'deletarPedido'));

RouteCollection::add(AdministradorController::class, new Route('programa-no-ar', 'buscarProgramaNoAr'));

RouteCollection::add(AdministradorController::class, new Route('estatistica', 'buscarEstatistica'));
