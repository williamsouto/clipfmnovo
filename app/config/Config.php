<?php

/**
 * Class Config
 *
 * Classe responsável em configurar o ambiente para aplicação
 * Cargas iniciais, diretórios e parametrizações
 */
class Config extends FrameworkConfig {

    public function __construct() {
        $this->initializeVariables();
    }

    /**
     * Inicializa as configurações da aplicação que utiliza o framework
     */
    public function initializeConfiguration() {
        return $this->loadConfigs();
    }

    /**
     * Cria as variáveis globais para uso da aplicação
     */
    private function initializeVariables() {
        define('URL_BASE', 'www.clipfm.test');
        define('DS', DIRECTORY_SEPARATOR);
    }

    /**
     * Carrega todas as configurações da aplicação
     */
    private function loadConfigs() {
        $configs = [];

        if (file_exists($path = ROOT.'/app/config/config.json')) {
            $fileContent = file_get_contents($path);
            $configs     = json_decode($fileContent, true);

            $configs = [
                'root'        => ROOT,
                'default'     => ROOT.$configs['default'],
                'common'      => ROOT.$configs['common'],
                'public'      => ROOT.$configs['public'],
                'config'      => ROOT.$configs['config'],
                'environment' => $configs['environment'],
                'data_base'   => $configs['data_base']
            ];
        }

        return $configs;
    }
}
