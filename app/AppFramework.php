<?php

class AppFramework extends Framework {

    public function __construct() {
        $this->loadGlobals();
        $this->setarConfigsDisplayErrors();
    }

    /**
     * Registra os serviços da aplicação
     *
     * @return array
     */
    public function registerServices() {
        return [
           'helper' =>  new HelperFramework()
        ];
    }

    /**
     * Carrega as variáveis globais
     */
    private function loadGlobals() {
        define('HOST', 'www.radioabc.test');
    }

    /**
     * Configura as exibições de erros
     */
    private function setarConfigsDisplayErrors() {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }
}
