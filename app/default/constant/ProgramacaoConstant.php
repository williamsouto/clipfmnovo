<?php

class ProgramacaoConstant {

    const DIA_SEGUNDA = 'Segunda';
    const DIA_TERCA   = 'Terca';
    const DIA_QUARTA  = 'Quarta';
    const DIA_QUINTA  = 'Quinta';
    const DIA_SEXTA   = 'Sexta';
    const DIA_SABADO  = 'Sabado';
    const DIA_DOMINGO = 'Domingo';

    /**
     * Retorna o nome do dia da semana
     *
     * @param $dia
     * @return mixed|null
     */
    public static function getNomeDia($dia) {
        $dias = [
            ProgramacaoConstant::DIA_SEGUNDA => 'Segunda',
            ProgramacaoConstant::DIA_TERCA   => 'Terça',
            ProgramacaoConstant::DIA_QUARTA  => 'Quarta',
            ProgramacaoConstant::DIA_QUINTA  => 'Quinta',
            ProgramacaoConstant::DIA_SEXTA   => 'Sexta',
            ProgramacaoConstant::DIA_SABADO  => 'Sábado',
            ProgramacaoConstant::DIA_DOMINGO => 'Domingo'
        ];

        return array_key_exists($dia, $dias) ? $dias[$dia] : null;
    }
}
