<?php

$query = "
    SELECT 
      n.id_noticia,
      n.titulo,
      n.texto,
      n.id_categoria,
      n.autor,
      n.imagem,
      DATE_FORMAT(n.data_criacao, '%d/%m/%Y') as data_criacao,
      c.nome AS categoria
    FROM noticia n
        INNER JOIN categoria c ON n.id_categoria = c.id_categoria
";

if (!empty($bindings['id_noticia'])) {
    $query .= ' WHERE n.id_noticia = :id_noticia';
}

$query .= " ORDER BY data_criacao DESC";
