<?php

$query = "
    SELECT
      n.id_noticia,
      n.titulo,
      n.imagem
    FROM noticia n 
";

if (!empty($bindings['id_noticia'])) {
    $query .= ' WHERE n.id_noticia <> :id_noticia';
}

$query .= ' ORDER BY n.id_noticia DESC';

if (!empty($bindings['limite'])) {
    $query .= ' LIMIT '.$bindings['limite'];
    unset($bindings['limite']);
}
