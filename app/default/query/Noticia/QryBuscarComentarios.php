<?php

$query = "
    SELECT
        c.id_comentario,
        c.comentario,
        c.ind_exibir,
        IF(c.ind_exibir = 0, 'Não', 'Sim') AS descricao_exibir,
        c.id_noticia,
        c.autor,
        c.email,
        DATE_FORMAT(c.data_criacao, '%d/%m/%Y') as data_criacao,
        n.titulo,
        cat.nome AS nome_categoria
    FROM comentario c 
      INNER JOIN noticia n ON n.id_noticia = c.id_noticia 
      INNER JOIN categoria cat on n.id_categoria = cat.id_categoria 
";

$query .= !empty($bindings['id_comentario']) ? 'WHERE id_comentario = '.$bindings['id_comentario'] : '';
