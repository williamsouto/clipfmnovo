<?php

$query = "
    SELECT
      n.id_noticia,
      n.titulo,
      n.imagem,
      DATE_FORMAT(n.data_criacao, '%d/%m/%Y') as data_criacao
    FROM noticia n
    WHERE n.id_noticia <> :id_noticia AND n.id_categoria = :id_categoria
    ORDER BY n.id_noticia DESC
";

if (!empty($bindings['limite'])) {
    $query .= ' LIMIT '.$bindings['limite'];
    unset($bindings['limite']);
}
