<?php

$query = "
    SELECT
      n.id_noticia,
      n.titulo,
      n.texto,
      n.id_categoria,
      n.autor,
      n.imagem,
      DATE_FORMAT(n.data_criacao, '%d/%m/%Y %H:%i') as data_criacao
    FROM noticia n 
    ORDER BY n.data_criacao DESC 
";

if (!empty($bindings['limite'])) {
    $query .= " LIMIT " . $bindings['limite'];
    unset($bindings['limite']);
}

if (!empty($bindings['offset'])) {
    $query .= " OFFSET " . $bindings['offset'];
    unset($bindings['offset']);
}
