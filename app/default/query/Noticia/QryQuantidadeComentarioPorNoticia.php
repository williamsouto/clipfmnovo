<?php

$query = "
    SELECT 
        n.id_noticia,
        COUNT(n.id_noticia) AS quantidade
    FROM noticia n 
        INNER JOIN comentario c ON c.id_noticia = n.id_noticia
    WHERE n.id_noticia = :id_noticia
    GROUP BY n.id_noticia
";
