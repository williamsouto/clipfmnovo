<?php

$query = "
    SELECT
      p.id_pedido,
      p.email,
      p.nome,
      p.pedido,
      p.ind_dashboard,
      DATE_FORMAT(p.data_criacao, '%d/%m/%Y') as data_criacao
    FROM pedido p 
    WHERE (1 = 1) 
";

if (!empty($bindings['id_pedido'])) {
    $query .= "AND p.id_pedido = ".$bindings['id_pedido'];
}

if (isset($bindings['ind_dashboard'])) {
    $query .= " AND p.ind_dashboard = ".$bindings['ind_dashboard'];
}

$query .= " ORDER BY p.ind_dashboard, p.data_criacao";

if (!empty($bindings['limite'])) {
    $query .= " LIMIT ".$bindings['limite'];
}
