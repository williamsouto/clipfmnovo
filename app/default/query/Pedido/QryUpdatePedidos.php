<?php

$query = "
    UPDATE pedido 
    SET %s
    WHERE (1 = 1) %s 
";

$where = "";

if (!empty($bindings['id_pedido'])) {
    $where = "AND " . UtilsDB::whereIn('id_pedido', $bindings['id_pedido']);
    unset($bindings['id_pedido']);
}

$query = sprintf($query, UtilsDB::buildParametersQuery(array_keys($bindings), UtilsDB::UPDATE), $where);
