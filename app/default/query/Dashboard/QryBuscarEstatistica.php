<?php

$query = "
    SELECT
        IF(SUM(resultado.total_noticia) IS NULL, 0, SUM(resultado.total_noticia)) AS total_noticia,
        IF(SUM(resultado.total_comentario) IS NULL, 0, SUM(resultado.total_comentario)) AS total_comentario,
        IF(SUM(resultado.total_pedido) IS NULL, 0, SUM(resultado.total_pedido)) AS total_pedido
    FROM (
         SELECT
             COUNT(n.id_noticia) AS total_noticia,
             0 AS total_comentario,
             0 AS total_pedido
         FROM noticia n
         GROUP BY n.id_noticia

         UNION

         SELECT
             0 AS total_noticia,
             COUNT(c.id_comentario) AS total_comentario,
             0 AS total_pedido
         FROM comentario c
         WHERE c.ind_exibir = 1
         GROUP BY c.id_comentario

         UNION

         SELECT
             0 AS total_noticia,
             0 AS total_comentario,
             COUNT(p.id_pedido) AS total_pedido
         FROM pedido p
         GROUP BY p.id_pedido
    ) resultado
";
