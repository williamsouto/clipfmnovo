<?php

$query = "
    UPDATE contato 
    SET %s
    WHERE (1 = 1) %s 
";

$where = "";

if (!empty($bindings['id_contato'])) {
    $where = "AND " . UtilsDB::whereIn('id_contato', $bindings['id_contato']);
    unset($bindings['id_contato']);
}

$query = sprintf($query, UtilsDB::buildParametersQuery(array_keys($bindings), UtilsDB::UPDATE), $where);
