<?php

$query = "
    SELECT
      c.id_contato,
      c.nome,
      c.email,
      c.assunto,
      c.mensagem,
      c.ind_visto,
      IF(ind_visto = 0, 'Não', 'Sim') AS descricao_visto,
      DATE_FORMAT(c.data_visualizacao, '%d/%m/%Y') as data_visualizacao,
      DATE_FORMAT(c.data_criacao, '%d/%m/%Y') as data_criacao
    FROM contato c 
    WHERE (1 = 1) 
";

if (!empty($bindings['id_contato'])) {
    $query .= "AND c.id_contato = ".$bindings['id_contato'];
}

if (isset($bindings['ind_dashboard'])) {
    $query .= " AND c.ind_dashboard = ".$bindings['ind_dashboard'];
}

if (isset($bindings['ind_visto'])) {
    $query .= " AND c.ind_visto = ".$bindings['ind_visto'];
}

$query .= " ORDER BY c.ind_visto, c.data_criacao";

if (!empty($bindings['limite'])) {
    $query .= " LIMIT ".$bindings['limite'];
}
