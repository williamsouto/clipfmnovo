<?php

class AdministradorController extends Controller {

    /**
     * @return Response
     * @throws Exception
     */
    public function index() {
        return $this->render('Administrador/administrador.phtml', 'Administrador', [], false, false, false);
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function carousel() {
        return $this->render('Administrador/carousel.phtml', 'Administrador > Carousel', [], false, false, false);
    }

    /**
     * @return \Response
     * @throws \Exception
     */
    public function noticia() {
        return $this->render('Administrador/noticia.phtml', 'Notícias', [], false, false, false);
    }

    /**
     * Renderiza a tela de categorias
     *
     * @return Response
     * @throws Exception
     */
    public function categoria() {
        return $this->render('Administrador/categoria.phtml', 'Categorias', [], false, false, false);
    }

    /**
     * Renderiza a tela de programação
     *
     * @return Response
     * @throws Exception
     */
    public function programacao() {
        return $this->render('Administrador/programacao.phtml', 'Administrador > Programação', [], false, false, false);
    }

    /**
     * Renderiza a tela de usuário
     *
     * @return \Response
     * @throws \Exception
     */
    public function usuario() {
        return $this->render('Administrador/usuario.phtml', 'Administrador > Usuário', [], false, false, false);
    }

    /**
     * Renderiza a tela de parceiro
     *
     * @return \Response
     * @throws \Exception
     */
    public function parceiro() {
        return $this->render('Administrador/parceiro.phtml', 'Administrador > Parceiro', [], false, false, false);
    }

    /**
     * Renderiza a tela de comentário
     *
     * @return \Response
     * @throws \Exception
     */
    public function comentario() {
        return $this->render('Administrador/comentario.phtml', 'Administrador > Comentário', [], false, false, false);
    }

    /**
     * Renderiza a tela de contato
     *
     * @return \Response
     * @throws \Exception
     */
    public function contato() {
        return $this->render('Administrador/contato.phtml', 'Administrador > Contato', [], false, false, false);
    }

    /**
     * Busca todos os carousels cadastrados
     *
     * @param null $idCarousel
     *
     * @return Response
     * @throws Exception
     */
    public function buscarCarousels($idCarousel = null) {
        $carousels = (new CarouselBO())->buscarCarousels($idCarousel);
        return $this->json(['carousels' => $carousels]);
    }

    /**
     * Busca todos os carousels cadastrados
     *
     * @param $idCarouselTipo
     *
     * @return Response
     * @throws Exception
     */
    public function buscarTiposCarousel($idCarouselTipo) {
        $tiposCarousels = (new CarouselBO())->buscarTiposCarousel($idCarouselTipo);
        return $this->json(['tipos_carousel' => $tiposCarousels]);
    }

    /**
     * Salva o carousel
     *
     * @param $carousel
     *
     * @return Response
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function salvarCarousel($carousel) {
        $carousel = Validate::validation($carousel, [
            'id_carousel'      => 'null|int',
            'id_carousel_tipo' => 'required|int',
            'titulo'           => 'required|string|min:1',
            'ordem'            => 'required|int',
            'descricao'        => 'null|string|min:1',
            'imagem'           => 'required|array',
            'id_noticia'       => 'required|int'
        ]);

        $msg = 'Carousel salvo com sucesso!';

        try {
            $carousel['imagem'] = $this->fileResolver($carousel['imagem'], 'carousel');
            (new CarouselBO())->salvarCarousel($carousel);

        } catch (Exception $e) {
            $msg = $e->getMessage();
        }

        return $this->json(['message' => $msg]);
    }

    /**
     * Busca todos as notícias cadastradas
     *
     * @param null|int $idNoticia
     *
     * @return Response
     * @throws Exception
     */
    public function buscarNoticias($idNoticia = null) {
        $noticias = (new NoticiaBO())->buscarNoticias($idNoticia);
        return $this->json(['noticias' => $noticias]);
    }

    /**
     * Busca todas as notícias pelo termo informado
     *
     * @param $termo
     *
     * @return Response
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function buscarNoticiasTermo($termo) {
        $termo = Validate::validation($termo, [
           'termo' => 'required|string|min:3'
        ]);

        $noticias = (new NoticiaBO())->buscarNoticiasTermo($termo);
        return $this->json(['noticias' => $noticias]);
    }

    /**
     * Salva a imagem para ser inserida no post de notícias
     *
     * @param array $imagem Imagem a ser feita upload
     *
     * @return string
     * @throws \Exception
     */
    public function salvarImagemNoticia($imagem) {
        $arquivo  = '';
        $msgErro  = '';
        $uploaded = false;

        try {
            $arquivo  = $this->fileResolver($imagem['upload'], 'noticia');
            $uploaded = true;
        } catch (Exception $e) {
            $msgErro = $e->getMessage();
        }

        $data = [
            'url'      => 'noticia/imagem/buscar/' . $arquivo,
            'uploaded' => $uploaded
        ];

        if (!empty($msgErro)) {
            $data['error'] = $msgErro;
        }

        return $this->json($data);
    }

    /**
     * Busca o arquivo e lê seu conteúdo
     *
     * @param $imagem
     *
     * @return \Response
     * @throws \Exception
     */
    public function buscarImagem($imagem) {
        $this->readResourceContent($imagem, 'noticia');
        return $this->json('');
    }

    /**
     * Salva os dados da notícia
     *
     * @param $noticia
     *
     * @return Response
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function salvarNoticia($noticia) {
        $noticia = Validate::validation($noticia, [
            'id_noticia'   => 'null|int',
            'titulo'       => 'required|string|min:1',
            'texto'        => 'required|string|min:1',
            'id_categoria' => 'required|int',
            'autor'        => 'required|string|min:1',
            'imagem'       => 'required|array'
        ]);

        $noticia['imagem'] = $this->fileResolver($noticia['imagem'], 'noticia');
        (new NoticiaBO())->salvarNoticia($noticia);

        return $this->json(['message' => 'Notícia salva com sucesso!']);
    }

    /**
     * Busca todas as categorias
     *
     * @param int $idCategoria
     *
     * @return Response
     * @throws Exception
     */
    public function buscarCategoria($idCategoria = null) {
        $categorias = (new CategoriaBO)->buscarCategorias($idCategoria);
        return $this->json(['categorias' => $categorias]);
    }

    /**
     * Salva a categoria
     *
     * @param $categoria
     *
     * @return Response
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function salvarCategoria($categoria) {
        $categoria = Validate::validation($categoria, [
            'id_categoria' => 'null|int',
            'nome'         => 'required|string|min:1',
            'nivel'        => 'required|int',
        ]);

        (new CategoriaBO())->salvarCategoria($categoria);
        return $this->json(['message' => 'Categoria salva com sucesso!']);
    }

    /**
     * Busca as programações cadastradas
     *
     * @param null $idProgramacao
     *
     * @return \Response
     * @throws Exception
     */
    public function buscarProgramacao($idProgramacao = null) {
        $programacoes = (new ProgramacaoBO())->buscarProgramacao($idProgramacao);
        return $this->json(['programacoes' => $programacoes]);
    }

    /**
     * Salva a programação
     *
     * @param $programacao
     *
     * @return \Response
     *
     * @throws \ValidationException
     * @throws Exception
     */
    public function salvarProgramacao($programacao) {
        $programacao = Validate::validation($programacao, [
            'id_programacao' => 'null|int',
            'nome_programa'  => 'required|string|min:1',
            'horario'        => 'required|string|min:5',
            'dia'            => 'required|string|min:5',
            'descricao'      => 'required|string|min:1',
            'imagem'         => 'required|array'
        ]);

        $nomeArquivo = $this->fileResolver($programacao['imagem'], 'programacao');
        (new ProgramacaoBO())->salvarProgramacao($programacao, $nomeArquivo);

        return $this->json(['message' => 'Programação salvo com sucesso.']);
    }

    /**
     * Busca os usuários cadastrados
     *
     * @param int $idUsuario
     *
     * @return Response
     * @throws Exception
     */
    public function buscarUsuarios($idUsuario = null) {
        $usuarios = (new UsuarioBO())->buscarUsuarios($idUsuario);
        return $this->json(['usuarios' => $usuarios]);
    }

    /**
     * Salva o usuário
     *
     * @param $usuario
     *
     * @return Response
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function salvarUsuario($usuario) {
        $usuario = Validate::validation($usuario, [
            'id_usuario'      => 'null|int',
            'nome'            => 'required|string|min:1',
            'usuario'         => 'required|string|min:1',
            'senha'           => 'required|string|min:5',
            'confirmar_senha' => 'required|string|min:5'
        ]);

        (new UsuarioBO())->salvarUsuario($usuario);
        return $this->json(['message' => 'Usuário salvo com sucesso.']);
    }

    /**
     * Busca os dados do parceiro
     *
     * @param int $idParceiro Código do parceiro
     *
     * @return Response
     * @throws Exception
     */
    public function buscarParceiros($idParceiro = null) {
        $parceiros = (new ParceiroBO())->buscarParceiros($idParceiro);
        return $this->json(['parceiros' => $parceiros]);
    }

    /**
     * Salva os dados do parceiro
     *
     * @param $parceiro
     *
     * @return Response
     *
     * @throws ValidationException
     * @throws Exception
     */
    public function salvarParceiro($parceiro) {
        $parceiro = Validate::validation($parceiro, [
            'id_parceiro' => 'null|int',
            'nome'        => 'required|string|min:1',
            'descricao'   => 'required|string|min:1',
            'imagem'      => 'required|array'
        ]);

        $parceiro['imagem'] = $this->fileResolver($parceiro['imagem'], 'parceiro');
        (new ParceiroBO())->salvarUsuario($parceiro);

        return $this->json(['message' => 'Parceiro salvo com sucesso.']);
    }

    /**
     * Busca os comentários cadastrados
     *
     * @param int $idComentario Id do comentário
     *
     * @return Response
     * @throws Exception
     */
    public function buscarComentarios($idComentario = null) {
        $comentarios = (new NoticiaBO())->buscarComentarios($idComentario);
        return $this->json(['comentarios' => $comentarios]);
    }

    /**
     * Aprova o comentário cadastrado
     *
     * @param array $comentario Dados do comentário
     *
     * @return Response
     * @throws Exception
     */
    public function aprovarComentario($comentario) {
        $comentario = Validate::validation($comentario, [
            'id_comentario' => 'required|int',
            'id_noticia'    => 'required|int'
        ]);

        (new NoticiaBO())->aprovarComentario($comentario);
        return $this->json(['message' => 'Comentário aprovado com sucesso.']);
    }

    /**
     * Busca os contatos realizados pelo usuário
     *
     * @param int $idContato Id do contato
     *
     * @return Response
     * @throws Exception
     */
    public function buscarContatos($idContato) {
        $contatos = (new ContatoBO())->buscarContatos($idContato);
        return $this->json(['contatos' => $contatos]);
    }

    /**
     * Busca os contatos realizados pelo usuário
     *
     * @param int $limite Limite de registros para retornar
     *
     * @return Response
     * @throws Exception
     */
    public function buscarContatosDashboard($limite) {
        $contatoBO = new ContatoBO();

        $contatos = $contatoBO->buscarContatos(null, 0, $limite);

        if (!empty($contatos)) {
            $idsContatos = array_column($contatos, 'id_contato');
            $contatoBO->atualizarContato($idsContatos, ['ind_dashboard' => 1]);
        }

        return $this->json(['contatos' => $contatos]);
    }

    /**
     * Flegar contato como visto
     *
     * @param array $contato Dados do contato
     *
     * @return Response
     * @throws Exception
     */
    public function visualizarContato($contato) {
        $contato = Validate::validation($contato, [
            'id_contato' => 'required|int'
        ]);

        (new ContatoBO())->visualizarContato($contato);
        return $this->json(["message" => "Contato {$contato['id_contato']} atualizado para status visualizado."]);
    }

    /**
     * Flegar contato como visto
     *
     * @param array $contato Dados do contato
     *
     * @return Response
     * @throws Exception
     */
    public function visualizarContatoDashboard($contato) {
        $contato = Validate::validation($contato, [
            'id_contato' => 'required|int'
        ]);

        $contatoBO = new ContatoBO();
        $contato['ind_dashboard'] = 0;
        $idContato = $contato['id_contato'];

        $contatoBO->visualizarContato($contato);
        $contato = $contatoBO->buscarProximoContatoDashboard();

        return $this->json(["message" => "Contato {$idContato} atualizado para status visualizado.", "contato" => $contato]);
    }

    /**
     * Deletar o contato
     *
     * @param array $contato Dados do contato
     *
     * @return Response
     * @throws Exception
     */
    public function deletarContato($contato) {
        $contato = Validate::validation($contato, [
            'id_contato' => $contato['id_contato']
        ]);

        $contatoBO = new ContatoBO();
        $idContato = $contato['id_contato'];

        $contatoBO->deletarContato($contato['id_contato']);
        $contato = $contatoBO->buscarProximoContatoDashboard();

        return $this->json(["message" => "Contato {$idContato} deletado com sucesso.", 'contato' => $contato]);
    }

    /**
     * Busca os contatos realizados pelo usuário
     *
     * @param int $limite Limite de registros para retornar
     *
     * @return Response
     * @throws Exception
     */
    public function buscarPedidos($limite) {
        $pedidoBO = new PedidoBO();
        $pedidos = $pedidoBO->buscarPedidos(null, $limite);

        if (!empty($pedidos)) {
            $idsPedidos = array_column($pedidos, 'id_pedido');
            $pedidoBO->atualizarPedido($idsPedidos, ['ind_dashboard' => 1]);
        }

        return $this->json(['pedidos' => $pedidos]);
    }

    /**
     * Deletar o pedido
     *
     * @param array $pedido Dados do pedido
     *
     * @return Response
     * @throws Exception
     */
    public function deletarPedido($pedido) {
        $pedido = Validate::validation($pedido, [
            'id_pedido' => $pedido['id_pedido']
        ]);

        $pedidoBO = new PedidoBO();
        $idPedido = $pedido['id_pedido'];

        $pedidoBO->deletarPedido($pedido['id_pedido']);
        $pedido = $pedidoBO->buscarProximoPedidoDashboard();

        return $this->json(["message" => "Pedido {$idPedido} deletado com sucesso.", 'pedido' => $pedido]);
    }

    /**
     * Busca o programa que esta no ar no momento
     *
     * @throws \Exception
     */
    public function buscarProgramaNoAr() {
        $programa = (new ProgramacaoBO())->buscarProgramacaoNoAr();
        return $this->json(['programa' => $programa]);
    }

    /**
     * Busca os dados de estatistica do dashboard
     *
     * @return Response
     * @throws Exception
     */
    public function buscarEstatistica() {
        $estatistica = (new NoticiaBO())->buscarEstatistica();
        return $this->json(['estatistica' => $estatistica]);
    }
}
