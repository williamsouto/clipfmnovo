<?php

class SiteController extends Controller {

    /**
     * @return Response
     * @throws Exception
     */
    public function site() {
        return $this->render('Home/site.phtml', 'Clip FM');
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function radio() {
        return $this->render('Site/radio.phtml', 'Clip FM');
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function programacao() {
        return $this->render('Site/programacao.phtml', 'Clip FM - Programação');
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function noticias() {
        return $this->render('Site/noticias.phtml', 'Clip FM - Notícias');
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function promocoes() {
        return $this->render('Site/promocoes.phtml', 'Clip FM - Promoções');
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function equipe() {
        return $this->render('Site/equipe.phtml', 'Clip FM - Equipe');
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function contato() {
        return $this->render('Site/contato.phtml', 'Clip FM - Contato');
    }
}
