<?php

class PageController extends Controller {

    public function notFound() {
        return new Response('Página não encontrada');
    }

    /**
     * Renderiza uma modal
     *
     * @param string $titulo
     *
     * @return \Response
     * @throws Exception
     */
    public function renderModal($titulo) {
        return $this->renderTemplate('modal.phtml', $titulo);
    }

    /**
     * Renderiza um dialog
     *
     * @return Response
     * @throws Exception
     */
    public function renderDialog() {
        return $this->renderTemplate('dialog.phtml');
    }
}
