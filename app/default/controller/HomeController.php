<?php

class HomeController extends Controller {

    /**
     * @return Response
     * @throws Exception
     */
    public function index() {
        return $this->render('Home/home.phtml', 'Home', [], false, false, false);
    }
}
