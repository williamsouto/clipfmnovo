<?php

class NoticiaBO extends Model {

    /**
     * Busca todas as notícias cadastradas
     *
     * @param null $idNoticia
     * @return array
     * @throws Exception
     */
    public function buscarNoticias($idNoticia = null) {
        $unique = !empty($idNoticia);
        $filtro = $unique ? ['id_noticia' => $idNoticia] : [];

        $categorias = $this->persistence->query('Noticia/QryBuscarNoticias', $filtro);

        $categorias = array_map(function ($categoria) {
            $categoria['texto'] = Utils::decodeHtml($categoria['texto']);
            return $categoria;
        }, $categorias);

        return $unique ? array_shift($categorias) : $categorias;
    }

    /**
     * Busca as notícias de forma paginada
     *
     * @param int $limite Quantidade de registros que irão ser buscados
     * @param int $offset Posição inicial do registro na tabela
     *
     * @return array
     * @throws \Exception
     */
    public function buscarNoticiasPaginado($limite, $offset) {
        $noticias = $this->persistence->query('Noticia/QryBuscarNoticiasPaginado', ['limite' => $limite, 'offset' => $offset]);

        $noticias = array_map(function ($noticia) {
            $noticia['texto'] = Utils::decodeHtml($noticia['texto']);

            $quantidadeComentario = $this->buscarQuantidadeComentarioPorNoticia($noticia['id_noticia']);
            $quantidadeLike = $this->buscarQuantidadeLikePorNoticia($noticia['id_noticia']);

            $noticia['quantidade_comentario'] = empty($quantidadeComentario) ? 0 : $quantidadeComentario['quantidade'];
            $noticia['quantidade_like'] = empty($quantidadeLike) ? 0 : $quantidadeLike['quantidade'];

            return $noticia;
        }, $noticias);

        return $noticias;
    }

    /**
     * Salva a notícia
     *
     * @param $noticia
     *
     * @return string
     */
    public function salvarNoticia($noticia) {
        if (!empty($noticia['id_noticia'])) {
            $this->atualizarNoticia($noticia['id_noticia'], $noticia);
            return $noticia['id_noticia'];
        }

        return $this->persistence->insertTable('noticia', array_merge($noticia, ['data_criacao' => Utils::date()]));
    }

    /**
     * Atualiza a noticia
     *
     * @param $idNoticia
     * @param $data
     */
    public function atualizarNoticia($idNoticia, $data) {
        $this->persistence->updateTable('noticia', $data, ['id_noticia' => $idNoticia]);
    }

    /**
     * Busca todas as notícias que estão cadastradas e atende os termos
     *
     * @param $termo
     * @return array
     * @throws Exception
     */
    public function buscarNoticiasTermo($termo) {
        return $this->persistence->query('Noticia/QryBuscarNoticiasPorTermo', ['termo' => $termo['termo']]);
    }

    /**
     * Busca as ultimas notícias cadastradas
     *
     * @param int $idNoticia
     * @param int $limite
     *
     * @return array
     * @throws Exception
     */
    public function buscarUltimasNoticias($idNoticia = null, $limite = 10) {
        $filtro = !empty($idNoticia) ? ['id_noticia' => $idNoticia] : [];
        return $this->persistence->query('Noticia/QryBuscarUltimasNoticias', array_merge($filtro, ['limite' => $limite]));
    }

    /**
     * Busca as ultimas notícias cadastradas
     *
     * @param int $idNoticia
     * @param int $categoria
     * @param int $limite
     *
     * @return array
     * @throws Exception
     */
    public function buscarNoticiasRelacionadas($idNoticia, $categoria, $limite = 10) {
        $noticias = $this->persistence->query('Noticia/QryBuscarNoticiasRelacionadas', ['id_noticia' => $idNoticia, 'id_categoria' => $categoria, 'limite' => $limite]);

        foreach ($noticias as &$noticia) {
            $quantidadeComentario = $this->buscarQuantidadeComentarioPorNoticia($noticia['id_noticia']);
            $quantidadeLike = $this->buscarQuantidadeLikePorNoticia($idNoticia);

            $noticia['quantidade_comentario'] = empty($quantidadeComentario) ? 0 : $quantidadeComentario['quantidade'];
            $noticia['quantidade_like'] = empty($quantidadeLike) ? 0 : $quantidadeLike['quantidade'];
        }

        return $noticias;
    }

    /**
     * Salva o like do usuário
     *
     * @param int $idNoticia
     * @param string $ip Ip do usuário
     */
    public function salvarLike($idNoticia, $ip) {
        if (!$this->verificarPossuiLike($idNoticia, $ip)) {
            $this->persistence->insertTable('noticia_like', ['id_noticia' => $idNoticia, 'ip_usuario' => $ip, 'data_criacao' => Utils::date()]);
        }
    }

    /**
     * Verifica se o post possui like relacionado aquele IP
     *
     * @param $idNoticia
     * @param $ip
     *
     * @return bool
     */
    public function verificarPossuiLike($idNoticia, $ip) {
        $exist = $this->persistence->selectTable('noticia_like', ['id_noticia' => $idNoticia, 'ip_usuario' => $ip], 'id_noticia', true);
        return count($exist) > 0;
    }

    /**
     * Busca os comentários vinculados a notícia
     *
     * @param $idNoticia
     * @return array
     */
    public function buscarComentariosNoticia($idNoticia) {
        $comentarios = $this->persistence->selectTable('comentario', ['id_noticia' => $idNoticia, 'ind_exibir' => 1]);

        foreach ($comentarios as &$comentario) {
            $likesDislikes = $this->persistence->selectTable('comentario_like_dislike', ['id_noticia' => $idNoticia, 'id_comentario' => $comentario['id_comentario']]);

            $comentario['quantidade_like'] = count(array_filter($likesDislikes, function ($likeDislike) {
                return $likeDislike['ind_like'];
            }));

            $comentario['quantidade_dislike'] = count(array_filter($likesDislikes, function ($likeDislike) {
                return !$likeDislike['ind_like'];
            }));

            ArrayHelper::collectionDateFormat($comentario, 'data_criacao', 'd/m/Y', true);
        }

        return $comentarios;
    }

    /**
     * Busca os comentário cadastrados
     *
     * @param int $idComentario Id do comentário
     *
     * @return array
     * @throws Exception
     */
    public function buscarComentarios($idComentario = null) {
        $unique = !empty($idComentario);
        $filtro = $unique ? ['id_comentario' => $idComentario] : [];

        return $this->persistence->query('Noticia/QryBuscarComentarios', $filtro, $unique);
    }

    /**
     * Aprova o comentário
     *
     * @param array $comentario Dados do comentário
     *
     * @throws BusinessException
     */
    public function aprovarComentario($comentario) {
        if (!$this->verificarExisteNoticia($comentario['id_noticia'])) {
            throw new BusinessException('Notícia não existe');
        }

        $this->persistence->updateTable('comentario', ['ind_exibir' => 1], ['id_comentario' => $comentario['id_comentario']]);
    }

    /**
     * Salva o comentário relacionado a notícia
     *
     * @param $comentario
     * @throws BusinessException
     */
    public function salvarComentario($comentario) {
        if (!$this->verificarExisteNoticia($comentario['id_noticia'])) {
            throw new BusinessException('Notícia não existe');
        }

        $this->persistence->insertTable('comentario', [
            'comentario'   => $comentario['comentario'],
            'ind_exibir'   => 0,
            'id_noticia'   => $comentario['id_noticia'],
            'autor'        => $comentario['autor'],
            'email'        => $comentario['email'],
            'data_criacao' => Utils::date()
        ]);
    }

    /**
     * Busca a quantidade de comentário pelo id da notícia
     *
     * @param int $idNoticia Código da notícia
     *
     * @return array
     * @throws Exception
     */
    private function buscarQuantidadeComentarioPorNoticia($idNoticia) {
        return $this->persistence->query('Noticia/QryQuantidadeComentarioPorNoticia', ['id_noticia' => $idNoticia], true);
    }

    /**
     * Salva o like ou dislike relacionado ao comentário da notícia
     *
     * @param int $idNoticia Código da notícia
     * @param int $idComentario Código do comentário
     * @param string $ipUsuario Ip do usuário
     * @param bool $like Indicador para informar se é like (True = Like, False = Dislike)
     *
     * @return array
     * @throws Exception
     */
    public function salvarLikeDislike($idNoticia, $idComentario, $ipUsuario, $like = true) {
        $quantidade = ['like' => 0, 'dislike' => 0];

        if ($this->verificarExisteNoticia($idNoticia) && $this->verificarExisteComentario($idComentario)) {
            try {
                $this->persistence->beginTransaction();

                $this->deletarLikeDislike($idNoticia, $idComentario, $ipUsuario);
                $likes = $this->persistence->selectTable('comentario_like_dislike', [
                    'id_noticia'    => $idNoticia,
                    'id_comentario' => $idComentario,
                    'ip_usuario'    => $ipUsuario
                ]);

                $this->persistence->insertTable('comentario_like_dislike', [
                    'id_noticia'    => $idNoticia,
                    'id_comentario' => $idComentario,
                    'ip_usuario'    => $ipUsuario,
                    'ind_like'      => (int) $like,
                    'data_criacao'  => Utils::date()
                ]);

                $quantidade['like'] = count(array_filter($likes, function ($like) {
                    return $like['ind_like'];
                }));

                $quantidade['dislike'] = count(array_filter($likes, function ($like) {
                    return !$like['ind_like'];
                }));

                $key = $like ? 'like' : 'dislike';
                $quantidade[$key]++;

                $this->persistence->commit();
            } catch (Exception $e) {
                $this->persistence->rollback();
                throw $e;
            }
        }

        return $quantidade;
    }

    /**
     * Busca a quantidade de like por notícia
     *
     * @param int $idNoticia Código da notícia
     *
     * @return array
     * @throws Exception
     */
    private function buscarQuantidadeLikePorNoticia($idNoticia) {
        return $this->persistence->query('Noticia/QryQuantidadeLikePorNoticia', ['id_noticia' => $idNoticia], true);
    }

    /**
     * Verifica se a noticia existe
     *
     * @param $idNoticia
     * @return bool TRUE = Existe Senão = Não Existe
     */
    private function verificarExisteNoticia($idNoticia) {
        return count($this->persistence->selectTable('noticia', ['id_noticia' => $idNoticia], 'id_noticia', true)) > 0;
    }

    /**
     * Verificar se o comentário existe
     *
     * @param $idComentario
     * @return bool TRUE = Existe Senão = Não Existe
     */
    private function verificarExisteComentario($idComentario) {
        return count($this->persistence->selectTable('comentario', ['id_comentario' => $idComentario], 'id_comentario', true)) > 0;
    }

    /**
     * Deletar o like ou dislike do usuário
     *
     * @param int    $idNoticia    Código da notícia
     * @param int    $idComentario Código do comentário
     * @param string $ipUsuario    Ip do usuário
     */
    public function deletarLikeDislike($idNoticia, $idComentario, $ipUsuario) {
        $this->persistence->deleteTable('comentario_like_dislike', [
            'id_noticia'    => $idNoticia,
            'id_comentario' => $idComentario,
            'ip_usuario'    => $ipUsuario,
        ]);
    }

    /**
     * Busca os dados de estatistica do dashboard
     *
     * @return array
     * @throws Exception
     */
    public function buscarEstatistica() {
        return $this->persistence->query('Dashboard/QryBuscarEstatistica', [], true);
    }
}
