<?php

class UsuarioBO extends Model {

    /**
     * Busca o usuário de acordo com os parâmetros
     *
     * @param $usuario
     *
     * @return array
     */
    public function buscarUsuarioPorEmailSenha($usuario) {
        $senha = base64_encode($usuario['email'].$usuario['senha']);

        return $this->persistence->selectTable('usuario', [
            'email' => $usuario['email'],
            'senha' => $senha
        ], '*', true);
    }

    /**
     * Busca o usuário pelo código
     *
     * @param  int   $idUsuario
     * @return array
     */
    public function buscarUsuarioPorId($idUsuario) {
        return $this->persistence->selectTable('usuario', ['id_usuario' => $idUsuario], '*', true);
    }

    /**
     * Persiste os dados do usuário
     *
     * @param $usuario
     *
     * @return string
     * @throws Exception
     */
    public function salvarUsuario($usuario) {
        $usuario['tipo_usuario']   = UsuarioConstant::TIPO_USUARIO_COMUM;
        $usuario['senha']          = base64_encode($usuario['email'].$usuario['senha']);
        $usuario['data_criacao']   = Utils::date();
        $usuario['data_alteracao'] = Utils::date();

        return $this->persistence->insertTable('usuario', $usuario);
    }

    /**
     * Cadastra os dados do usuário e cria uma conta
     *
     * @param $usuario
     * @throws BusinessException
     * @throws Exception
     */
    public function cadastrarUsuario($usuario) {
        $email = $this->persistence->selectTable('usuario', ['email' => $usuario['email']], 'id_usuario');

        if (!empty($email)) {
            throw new BusinessException('Já existe um usuário cadastrado com esse email');
        }

        try {
            $this->persistence->beginTransaction();

            $idUsuario = $this->salvarUsuario($usuario);
            (new ContaUsuarioBO())->salvarContaUsuario([
                'id_usuario'       => $idUsuario,
                'saldo'            => '0.00000000',
                'ficha'            => 0,
                'data_atualizacao' => Utils::date()
            ]);

            $this->persistence->commit();
        } catch (Exception $e) {
            $this->persistence->rollback();
            throw $e;
        }
    }

    /**
     * Verifica se o usuário logado é administrador
     *
     * @return bool
     */
    public function validarPermissaoAdmin() {
        $userSession = $this->buscarUsuarioLogado();
        $usuario     = $this->persistence->selectTable('usuario', ['id_usuario' => $userSession['id_usuario']], '*', true);

        return $usuario['tipo_usuario'] == UsuarioConstant::TIPO_USUARIO_ADMIN;
    }

    /**
     * Atualiza os dados do usuário
     *
     * @param int   $idUsuario
     * @param array $usuario
     */
    public function atualizarUsuario($idUsuario, $usuario) {
        $this->persistence->updateTable('usuario', $usuario, ['id_usuario' => $idUsuario]);
    }

    /**
     * Busca o endereço do usuário
     *
     * @param int $idUsuario Código do usuário
     * @param int $indAtivo  Indica se o endereço do usuário esta ativo
     *
     * @return array Dados do usuário
     */
    public function buscarEnderecoUsuario($idUsuario, $indAtivo = 1) {
        return $this->persistence->selectTable('usuario_endereco', ['id_usuario' => $idUsuario, 'ind_disponivel' => 0, 'ind_ativo' => $indAtivo], '*', true);
    }

    /**
     * Busca o endereço do usuário pelo id
     *
     * @param int $idUsuarioEndereco Código do usuário endereço
     *
     * @return array Dados do usuário
     */
    public function buscarEnderecoUsuarioPorId($idUsuarioEndereco) {
        return $this->persistence->selectTable('usuario_endereco', ['id_usuario_endereco' => $idUsuarioEndereco], '*', true);
    }

    /**
     * Busca o endereço do usuário ativo pelo endereço
     *
     * @param string $endereco
     * @param int    $indAtivo
     * @param bool   $unique
     *
     * @return array
     */
    public function buscarEnderecoUsuarioPorEndereco($endereco, $indAtivo = 1, $unique = false) {
        return $this->persistence->selectTable('usuario_endereco', ['endereco_carteira' => $endereco, 'ind_disponivel' => 0, 'ind_ativo' => $indAtivo], '*', $unique);
    }

    /**
     * Deleta um registro do endereço usuário pelo seu código
     *
     * @param $idUsuarioEndereco
     */
    public function deletarEnderecoUsuario($idUsuarioEndereco) {
        $this->persistence->deleteTable('usuario_endereco', ['id_usuario_endereco' => $idUsuarioEndereco]);
    }

    /**
     * Processa os enderecos que estão a um certo período inativos e disponibiliza para outros usuários
     */
    public function processarEnderecosInativos() {
        $enderecos   = $this->persistence->selectTable('usuario_endereco', ['ind_ativo' => 1, 'ind_disponivel' => 0]);
        $transacaoBO = new TransacaoBO();

        $limiteDiaEnderecoInativo = (new ConfiguracaoSistemaBO())->buscarConfiguracao('LIMITE_DIA_ENDERECO')['valor'];

        foreach ($enderecos as $endereco) {
            $transacao = $transacaoBO->buscarTransacaoPagamento(['id_usuario_endereco' => $endereco['id_usuario_endereco']], true);
            $intervalo = Utils::diffDate($endereco['data_atribuicao'], Utils::date(), 'd');

            if (empty($transacao) && $intervalo > $limiteDiaEnderecoInativo) {
                $this->persistence->updateTable('usuario_endereco', ['ind_disponivel' => 1, 'ind_ativo' => 0], ['id_usuario_endereco' => $endereco['id_usuario_endereco']]);
            }
        }
    }
}
