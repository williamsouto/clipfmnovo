<?php

class ContatoBO extends Model {

    /**
     * Busca os contatos que não foram vistos
     *
     * @param int $idContato      Id do contato
     * @param int $indVisualizado Indicador para informar os contatos vistos
     * @param int $limite         Limite de registros para retornar
     * @param int $indDashboard   Indicador para informar que contato esta sendo exibido no dashboard
     *
     * @return array
     * @throws Exception
     */
    public function buscarContatos($idContato = null, $indVisualizado = null, $limite = null, $indDashboard = null) {
        $unique = !empty($idContato);
        $filtro = $unique ? ['id_contato' => $idContato] : [];

        if (!empty($limite)) {
            $filtro['limite']    = $limite;
            $filtro['ind_visto'] = $indVisualizado;
        }

        if (isset($indDashboard)) {
            $filtro['ind_dashboard'] = $indDashboard;
        }

        return $this->persistence->query('Contato/QryBuscarContatos', $filtro, $unique);
    }

    /**
     * Flega o contato do usuário como visto
     *
     * @param array $contato Dados do contato
     * @throws Exception
     */
    public function visualizarContato($contato) {
        $contato['ind_visto'] = 1;
        $contato['data_visualizacao'] = Utils::date();

        $this->atualizarContato($contato['id_contato'], $contato);
    }

    /**
     * Salva o contato que o usuário submeteu no formulário do site
     *
     * @param array $contato Dados do contato
     *
     * @return string
     * @throws Exception
     */
    public function salvarContato($contato) {
        if (!empty($contato['id_contato'])) {
            $this->atualizarContato($contato['id_contato'], $contato);
            return $contato['id_contato'];
        }

        return $this->persistence->insertTable('contato', array_merge($contato, ['data_criacao' => Utils::date()]));
    }

    /**
     * Atualiza os dados do contato
     *
     * @param int|array $idContato ID do contato
     * @param array     $contato   Dados a serem atualizados
     *
     * @throws Exception
     */
    public function atualizarContato($idContato, $contato) {
        $this->persistence->query('Contato/QryUpdateContatos', array_merge(['id_contato' => $idContato], $contato), false, Persistence::TYPE_UPDATE);
    }

    /**
     * Deleta o contato através do ID
     *
     * @param int $idContato ID do contato
     */
    public function deletarContato($idContato) {
        $this->persistence->deleteTable('contato', ['id_contato' => $idContato]);
    }

    /**
     * Busca o próximo contato para exibir no dashboard
     *
     * @return array
     * @throws Exception
     */
    public function buscarProximoContatoDashboard() {
        $contato = $this->buscarContatos(null, 0, 1, 0);

        if (!empty($contato)) {
            $contato = array_shift($contato);
            $this->atualizarContato($contato['id_contato'], ['ind_dashboard' => 1]);
        }

        return $contato;
    }
}
