<?php

class PedidoBO extends Model {

    /**
     * Salva o pedido
     *
     * @param $pedido
     */
    public function salvarPedido($pedido) {
        $pedido['data_criacao'] = Utils::date();
        $this->persistence->insertTable('pedido', $pedido);
    }

    /**
     * Busca os pedidos que não foram vistos
     *
     * @param int $idPedido     Id do Pedido
     * @param int $limite       Limite de registros para retornar
     * @param int $indDashboard Indicador para informar que contato esta sendo exibido no dashboard
     *
     * @return array
     * @throws Exception
     */
    public function buscarPedidos($idPedido = null, $limite = null, $indDashboard = null) {
        $unique = !empty($idPedido);
        $filtro = $unique ? ['id_pedido' => $idPedido] : [];

        if (!empty($limite)) {
            $filtro['limite']    = $limite;
        }

        if (isset($indDashboard)) {
            $filtro['ind_dashboard'] = $indDashboard;
        }

        return $this->persistence->query('Pedido/QryBuscarPedidos', $filtro, $unique);
    }

    /**
     * Atualiza os dados do pedido
     *
     * @param int|array $idPedido ID do pedido
     * @param array     $pedido   Dados a serem atualizados
     *
     * @throws Exception
     */
    public function atualizarPedido($idPedido, $pedido) {
        $this->persistence->query('Pedido/QryUpdatePedidos', array_merge(['id_pedido' => $idPedido], $pedido), false, Persistence::TYPE_UPDATE);
    }

    /**
     * Deleta o pedido através do ID
     *
     * @param int $idPedido ID do pedido
     */
    public function deletarPedido($idPedido) {
        $this->persistence->deleteTable('pedido', ['id_pedido' => $idPedido]);
    }

    /**
     * Busca o próximo pedido para exibir no dashboard
     *
     * @return array
     * @throws Exception
     */
    public function buscarProximoPedidoDashboard() {
        $pedido = $this->buscarPedidos(null, 1, 0);

        if (!empty($pedido)) {
            $pedido = array_shift($pedido);
            $this->atualizarPedido($pedido['id_pedido'], ['ind_dashboard' => 1]);
        }

        return $pedido;
    }
}
