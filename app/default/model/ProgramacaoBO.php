<?php

class ProgramacaoBO extends Model {

    /**
     * Busca as programações cadastradas
     *
     * @param int $idProgramacao
     *
     * @return array
     */
    public function buscarProgramacao($idProgramacao) {
        $unique = !empty($idProgramacao);
        $filtro = $unique ? ['id_programacao' => $idProgramacao] : [];

        $programacoes = $this->persistence->selectTable('programacao', $filtro, '*', $unique);
        ArrayHelper::collectionDateFormat($programacoes, 'data_criacao', 'd/m/Y', $unique);
        ArrayHelper::collectionDateFormat($programacoes, 'horario', 'H:i', $unique);

        return $programacoes;
    }

    /**
     * Busca a programação pelo dia da semana
     *
     * @param $dia
     *
     * @return array
     */
    public function buscarProgramacaoPorDia($dia) {
        $programacao = $this->persistence->selectTable('programacao', ['dia' => $dia], '*');
        ArrayHelper::collectionDateFormat($programacao, 'horario', 'H:i');
        return $programacao;
    }

    /**
     * Salva a programação
     *
     * @param $programacao
     * @param $arquivo
     *
     * @return string
     * @throws BusinessException
     */
    public function salvarProgramacao($programacao, $arquivo) {
        $this->validarConflitoProgramacao($programacao['horario'], $programacao['dia'], $programacao['id_programacao']);

        $programacao['imagem'] = $arquivo;

        if (!empty($programacao['id_programacao'])) {
            $this->atualizarProgramacao($programacao['id_programacao'], $programacao);
            return $programacao['id_programacao'];
        }

        return $this->persistence->insertTable('programacao', array_merge($programacao, ['data_criacao' => Utils::date()]));
    }

    /**
     * Atualiza a programação
     *
     * @param int   $idProgramacao Código da programação
     * @param array $programacao   Dados a serem atualizados
     */
    public function atualizarProgramacao($idProgramacao, $programacao) {
        $this->persistence->updateTable('programacao', $programacao, ['id_programacao' => $idProgramacao]);
    }

    /**
     * Valida se a programação que irá ser cadastrada, esta com o mesmo horário e dia de outra existente
     *
     * @param $horario
     * @param $dia
     * @param $idProgramacao
     *
     * @return bool
     * @throws BusinessException
     */
    private function validarConflitoProgramacao($horario, $dia, $idProgramacao) {
        $programacao = $this->persistence->selectTable('programacao', ['horario' => $horario, 'dia' => $dia], 'id_programacao', true);

        if (count($programacao) > 0 && ($programacao['id_programacao'] != $idProgramacao)) {
            throw new BusinessException(sprintf('Horário %s para o dia %s já está cadastrado.', $horario, ProgramacaoConstant::getNomeDia($dia)));
        }

        return true;
    }

    /**
     * Busca a programação atual que está no ar
     *
     * @return array
     *
     * @throws \Exception
     */
    public function buscarProgramacaoNoAr() {
        return $this->persistence->query('Programacao/QryBuscarProgramacaoNoAr', ['horario' =>  Utils::hour(), 'dia' => Utils::weekDay()], true);
    }
}
