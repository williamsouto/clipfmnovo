var $http = (function() {
    return {
        init : function() {
            var statusOK    = 200;
            var statusError = 500;

            var CONFIGS = {
                dataType: 'json',
                processData: false,
                contentType: false
            };

            /**
             * Envia requisição verbo GET
             *
             * @param {string}  $url    Endereço do recurso
             * @param {object}  $params Parâmetros utilizdos
             *
             * @return {object} Retorna uma promisse
             */
            var get = function ($url, $params) {
                return resolve($.ajax({
                    url   : $url,
                    method: 'GET',
                    data  : $params
                }));
            };

            /**
             * Envia requisição verbo POST
             *
             * @param {string}  $url    Endereço do recurso
             * @param {object}  $params Parâmetros utilizdos
             * @param {boolean} options Caso seja verdadeiro, injetar configurações na requisição ajax
             *
             * @return {object} Retorna uma promisse
             */
            var post = function ($url, $params, options) {
                var optDefaults = {
                    url   : $url,
                    method: 'POST',
                    data  : $params,
                    cache : false
                };

                if (options) {
                    optDefaults = Object.assign(optDefaults, CONFIGS)
                }

                return resolve($.ajax(optDefaults));
            };

            /**
             * Resolve a função e retorna um objeto promisse
             *
             * @param func
             * @return {*}
             */
            var resolve = function (func) {
                displayLoading();
                return func.then(function (response) {
                    response['success'] = true;
                    return response;
                }).catch(function (error) {
                    return {
                        status : error.status,
                        message: error.responseText
                    };
                }).always(function () {
                    hideLoading();
                });
            };

            var displayLoading = function () {
                $('body').append('<div class="loading-ajax"></div>');
            };

            var hideLoading = function () {
                var $loading = $('.loading-ajax');
                $loading.remove();
            };

            return {
                get : get,
                post: post
            }
        }
    };
})();
