var $utils = (function () {
    return {
        init: function () {

            var redirect = function (url) {
                window.location.href = url;
            };

            var isEmpty = function (val) {
                return val === ''
                    || typeof val === 'undefined'
                    || val === null
                    || (typeof val === 'object' && Object.keys(val).length === 0);
            };

            var isTypeOf = function (value, type) {
                return typeof value === type
            };

            var getParams = function () {
                var url = window.location.href;
                url = url.split('/');

                return url[url.length - 1];
            };

            var getDay = function () {
                var dia = new Date();
                return dia.getDay();
            };

            var stripHTML = function (string) {
                return string.replace(/<.*?>/g, '');
            };

            var getUrl = function () {
                return window.location.href;
            };

            return {
                redirect : redirect,
                isEmpty  : isEmpty,
                isTypeOf : isTypeOf,
                getParams: getParams,
                getDay   : getDay,
                stripHTML: stripHTML,
                getUrl   : getUrl
            };
        }
    }
})();
