var $mask = (function() {
    return {
        init : function ($selector, type) {
            var MASK = {
                'time' : '00:00'
            };

            var TYPE = {
                'TIME' : 'time',
                'MONEY': 'money'
            };

            $($selector).keypress(function(e) {
                mask(this, type, e);
            });

            var mask = function (field, type, event){
                if (field === false) {
                    event.returnValue = false;
                }
                return formatField(field, type, event);
            };

            var formatField = function (field, type, event) {
                if (type === TYPE.MONEY) {
                    moneyValidation(field);
                } else {
                    defaultValidation(field, MASK[type], event);
                }
            };

            var moneyValidation = function (field) {
                var v = field.value;
                v = v.replace(/^(0+)(\d)/g, "$2");
                v = v.replace(/\D/g,"") // permite digitar apenas numero
                v = v.replace(/(\d{1})(\d{17})$/,"$1.$2") // coloca ponto antes dos ultimos digitos
                v = v.replace(/(\d{1})(\d{13})$/,"$1.$2") // coloca ponto antes dos ultimos 13 digitos
                v = v.replace(/(\d{1})(\d{10})$/,"$1.$2") // coloca ponto antes dos ultimos 10 digitos
                v = v.replace(/(\d{1})(\d{7})$/,"$1.$2") // coloca ponto antes dos ultimos 7 digitos
                v = v.replace(/(\d{1})(\d{1,1})$/,"$1,$2") // coloca virgula antes dos ultimos 4 digitos

                if (v.length === 0) {
                    v = '0,0'+v;
                }
                field.value = v;
            };

            var defaultValidation = function (field, mask, event) {
                var boleanMask;
                var typed = event.keyCode;
                var exp   = /\-|\.|\/|\(|\)| /g;

                var numbers       = field.value.toString().replace( exp, "" );
                var fieldPosition = 0;
                var newFieldValue = "";
                var maskSize      = numbers.length;

                if (typed !== 8) { // backspace
                    for(var i = 0; i <= maskSize; i++) {
                        boleanMask = ((mask.charAt(i) === "-") || (mask.charAt(i) === ".") || (mask.charAt(i) === "/") || (mask.charAt(i) === ":"));
                        boleanMask = boleanMask || ((mask.charAt(i) === "(") || (mask.charAt(i) === ")") || (mask.charAt(i) === " "));
                        if (boleanMask) {
                            newFieldValue += mask.charAt(i);
                            fieldPosition++;
                        } else {
                            newFieldValue += numbers.charAt(fieldPosition);
                            fieldPosition++;
                        }
                    }
                    field.value = newFieldValue;
                }
                return true;
            };
        }
    }
})();
