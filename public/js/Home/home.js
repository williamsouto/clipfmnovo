(function (utils) {
    $('#loading').fadeOut(300);

    var $play          = $('.play');
    var $pause         = $('.pause');
    var $volumeControl = $(".controle-volume");
    var $audioOff      = $('.audio .off');
    var $radio         = $('.radio');

    var Player = _player('http://ice1.crossradio.com.br:8530/live.mp3', $play, $pause, $volumeControl, $('.audio .on'), $audioOff);

    $audioOff.hide();
    $pause.hide();

    $volumeControl.slider({
        orientation: "vertical",
        range: {min: 0, max: 100},
        value: 60,
        change: function( event, ui ) {
            Player.alterVolume(ui.value);
        }
    });

    $radio.click(function () {
        utils.redirect('/site');
    });

    $radio.click(function () {
        utils.redirect('/site');
    });

    $play.click(function() {
        Player.play();
    });

    $pause.click(function() {
        Player.pause();
    });

    $('.audio').click(function() {
        Player.mute();
    });

    function _player(stream, playSelector, pauseSelector, volumeControlSelector, onSelector, offSelector) {
        console.log('testando');
        var Player = new Audio(stream);
        var $play  = playSelector;
        var $pause = pauseSelector;
        var $on    = onSelector;
        var $off   = offSelector;
        var $volumeControl = volumeControlSelector;

        var play = function () {
            var playPromise = Player.play();

            if ($utils.isEmpty(playPromise)) {
                playPromise.then(function() {
                    $play.hide();
                    $pause.css('display', 'inline-block');
                }).catch(function(error) {
                    console.log('failed to load stream', error);
                });
            }
        };

        var pause = function () {
            $play.show();
            $pause.hide();
            Player.pause();
        };

        var alterVolume = function (volume) {
            Player.volume = volume / 100;
            changeIcon(volume);
        };

        var mute = function () {
            var volume = Player.volume === 0 ? 80 : 0;
            $volumeControl.slider("option", "value", volume);
            changeIcon(volume);

            Player.volume = volume / 100;
        };

        var changeIcon  = function (volume) {
            if (volume === 0) {
                $on.hide();
                $off.show();
            } else {
                $on.show();
                $off.hide();
            }
        };

        return {
            play       : play,
            pause      : pause,
            alterVolume: alterVolume,
            mute       : mute
        };
    }
})($utils);
