var $validate = (function () {
    return {
        init : function (form) {
            var $form   = $(form);
            var childs  = $form.find('input, select, textarea, radio');
            var button  = $form.find('button.submit');
            var invalid = false;
            var elementAlert;
            var nameAlert;
            var textAlert;

            var TYPE = {
                'EMAIL'  : 'email',
                'INTEIRO': 'inteiro',
                '': '',
                'TIME'   : 'time',
                'FILE'    : 'file'
            };

            var TEXT = {
                'REQUIRED' : 'O campo %s é obrigatório',
                'EMAIL'    : 'Email inválido',
                'INTEIRO'  : 'Campo é numérico',
                'TIME'     : 'Hora inválida',
                'FILE'     : 'Formato de arquivo inválido'
            };

            var fieldsInvalid = {};
            var initializeValidate = function (childs, button) {
                button.prop("disabled", true);

                childs.each(function (index, element) {
                    var $element = $(element);
                    $element.change(function () {
                        invalid = isInvalid(this);
                        buttonValidate(invalid);
                        replace(this);

                        if ($element.data('mask') === TYPE.FILE) {
                            $element.parent().next().val($element.val());
                        }
                    });

                    $element.keypress(function () {
                        buttonValidate(invalid);
                    });

                    $element.blur(function () {
                        if (invalid) {
                            addAlert(elementAlert, nameAlert, textAlert);
                        }
                    });

                    if ($element.is('input') && ($element.data('mask') === TYPE.INTEIRO || $element.data('mask') === TYPE.TIME)) {
                        $element.keypress(function (e) {
                            var keyCode = (e.keyCode ? e.keyCode : e.which);
                            if (!(keyCode > 47 && keyCode < 58)) {
                                e.preventDefault();
                            }
                        });
                    }
                });
                
                $(document).keypress(function (e) {
                    if (e.which === 13 && !button.prop('disabled')) {
                        var idButton = button.attr('id');
                        $('#'+idButton).trigger('click');
                    }
                });
            };

            var isInvalid = function (element) {
                var $element = $(element);
                var required = $element.data('required');
                var value    = $element.val();
                var type     = $element.data('mask');
                var name     = $element.attr('name');

                var invalid  = false;

                $element.removeClass('required');
                if (required && value === "") {
                    setDataAlert(element, name, TEXT.REQUIRED);
                    invalid = true;
                } else if (required) {
                    if (TYPE.EMAIL === type && !maskEmail(value)) {
                        setDataAlert(element, name, TEXT.EMAIL);
                        invalid = true;
                    } else if (TYPE.TIME === type && !maskTime(value)) {
                        setDataAlert(element, name, TEXT.TIME);
                        invalid = true;
                    } else if (TYPE.FILE === type && !maskFile(value)) {
                        setDataAlert(element, name, TEXT.FILE);
                        invalid = true;
                    }
                }

                fieldsInvalid[name] = invalid;
                return invalid;
            };

            var setDataAlert = function (element, name, text) {
                elementAlert = element;
                nameAlert    = name;
                textAlert    = text;
            };

            var addAlert = function (element, name, text) {
                var $element = $(element);
                var validate = $('div.validate[data-name="'+name+'"]');
                var label    = $element.attr('aria-label');

                if (validate.length === 0) {
                    var parent = $element.parents('.form-group');
                    parent.append('<div class="validate" data-name="'+name+'"></div>');

                    validate.attr('style', 'display: block');
                }

                var oldText = (validate.text() === '') ? '' : validate.text();
                validate.text(text.replace('%s', label));

                $element.addClass('required');
                $(validate).show('slow');
                setTimeout(function () {
                    $(validate).hide('slow');

                    if (oldText !== '') {
                        validate.text(oldText);
                    }
                }, 5000);
            };

            var buttonValidate = function (invalid) {
                var data     = $form.serializeArray();
                var files    = [];
                var name     = '';
                var field    = '';
                var disabled = false;

                $form.find('input:file').each(function () {
                    name = $(this).attr('name');
                    var value = $(this).val();

                    if ($utils.isEmpty(value)) {
                        value = $(this).parent().next().val()
                    }

                    files.push({name: name, value: value});
                });

                data = $.merge(data, files);

                if (!invalid) {
                    for (var i in data) {
                        if (data.hasOwnProperty(i)) {
                            field = data[i].name;
                            var fieldRequired = $form.find('[name="'+field+'"]').data('required');

                            if (fieldsInvalid[field] || (fieldRequired && data[i].value === "")) {
                                disabled = true;
                            }
                        }
                    }
                }

                button.prop("disabled", invalid || disabled);
            };

            var replace = function (element) {
                var type  = $(element).attr('type');
                var value = $(element).val();
                var max   = '';

                if (type !== 'file') {
                    if (type === 'number') {
                        max   = $(element).attr('max');
                        max   = parseInt(max, 10);
                        value = parseInt(value, 10);

                        max = max < value ? max : value;
                    } else {
                        max = $(element).attr('maxlength');
                        max = max < value.length ? value.substr(0, max) : value;
                    }

                    $(element).val(max);
                }
            };

            var maskEmail = function ($email) {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
                return emailReg.test($email);
            };

            var maskTime = function (value) {
                var regex = /([01][0-9]|2[0-3]):[0-5][0-9]/;
                return regex.test(value)
            };

            var maskFile = function (value) {
                var extAllowed = ['jpg', 'jpeg', 'png', 'gif'];
                var extension  = value.substr(value.lastIndexOf('.') + 1);

                return extAllowed.indexOf(extension) >= 0;
            };

            var dataForm = function (formData) {
                var data  = $form.serializeArray();
                var obj   = {};
                var field = null;
                var files = [];
                var name  = '';

                $form.find('input:file').each(function () {
                    name      = $(this).attr('name');
                    var value = $(this)[0].files[0];

                    if (!$utils.isTypeOf(value, 'object')) {
                        value = $(this).parent().next().val();

                        var type = 'image/' + value.split('.')[1];
                        var file = new File([value], value, {type: type});

                        file['exists'] = true;

                        value = file;
                    }

                    files.push({name: name, value: value});
                });

                data = $.merge(data, files);

                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        field = data[i].name;
                        field = $form.find('[name="'+field+'"]');

                        if ($(field).data('mask') === 'inteiro') {
                            obj[data[i].name] = parseInt(data[i].value, 10);
                        } else if ($(field).data('mask') === 'decimal') {
                            var val = data[i].value;
                            val = val.replace(/^(0+)(\d)/g, "$2");
                            val = val.replace('.', '');
                            val = val.replace(',', '.');
                            obj[data[i].name] = val;
                        } else {
                            obj[data[i].name] = data[i].value;
                        }
                    }
                }

                if (formData) {
                    var newData = new FormData();

                    $.each(obj, function (key, value) {
                        newData.append(key, value);
                    });

                    obj = newData;
                }

                return obj;
            };

            var bindForm = function (dataBind) {
                var data  = $form.serializeArray();
                var map   = {};

                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        map[data[i].name] = dataBind[data[i].name];
                    }
                }

                var key = '';

                childs.each(function () {
                    var $this = $(this);
                    key = $this.attr('name');

                    if (map.hasOwnProperty(key)) {
                        $(this).val(map[key]);
                    }
                });

                $form.find('input:file').each(function () {
                    var valor = dataBind[$(this).attr('name')];

                    $(this).parent().next().val(valor);
                });

                buttonValidate(invalid);
            };

            var resetForm = function () {
                childs.each(function (index, element) {
                    $(element).val('');
                    $(element).removeClass('required');
                });

                fieldsInvalid = {};
                invalid = false;
                button.prop("disabled", true);
            };

            initializeValidate(childs, button);

            return {
                dataForm : dataForm,
                bindForm : bindForm,
                resetForm: resetForm
            }
        }
    }
})();
