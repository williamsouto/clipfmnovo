(function (validate, http, componente, utils) {
    var validation = validate.init('form[name="login"]');
    var dialog     = componente.dialog();

    validation.resetForm();

    $('#btnLogar').click(function () {
        var data = validation.dataForm();

        http.post('/login/autenticar', data).then(function (resposta) {
            if (resposta.success) {
                utils.redirect('/' + resposta.url);
                return true;
            }

            dialog.error(resposta.message);
        });
    });

})($validate, $http, $componente, $utils);
