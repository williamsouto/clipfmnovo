(function (componente, http, validate) {
    var ROTA_BUSCAR = '/administrador/categoria/buscar';
    var ROTA_SALVAR = '/administrador/categoria/salvar';

    var tabs         = componente.tabs('#categoria');
    var dataTable    = componente.dataTable('#tblCategoria');
    var validation   = validate.init('form[name="categoria"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    var editar = function() {
        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();
                validation.bindForm(resposta['categorias']);
            });
        });
    };

    var buscarCategorias = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['categorias'], editar);
        });
    };

    buscarCategorias();

    $('#btnCadastrar, #btnCancelar').click(function () {
        resetar()
    });

    $('#btnSalvarCategoria').click(function () {
        var data = validation.dataForm();

        http.post(ROTA_SALVAR, data).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarCategorias();

            resetar();
        });
    });

})($componente, $http, $validate);
