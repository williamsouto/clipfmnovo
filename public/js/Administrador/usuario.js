(function (componente, http, validate) {

    var ROTA_BUSCAR = '/administrador/usuario/buscar';
    var ROTA_SALVAR = '/administrador/usuario/salvar';

    var tabs         = componente.tabs('#usuario');
    var dataTable    = componente.dataTable('#tblUsuario');
    var validation   = validate.init('form[name="usuario"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();

                resposta['usuarios']['confirmar_senha'] = resposta['usuarios']['senha'];
                validation.bindForm(resposta['usuarios']);
            });
        });
    };

    var buscarUsuarios = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['usuarios'], editar);
        });
    };

    buscarUsuarios();

    $('#btnCancelar').click(function () {
        resetar();
    });

    $('#btnCadastrar').click(function () {
        resetar();
    });

    $('#btnSalvarUsuario').click(function () {
        var data = validation.dataForm();

        if (data['senha'] !== data['confirmar_senha']) {
            notification.error('Confirmação de senha não confere com a senha inserida');
            return false;
        }

        data = validation.dataForm(true);

        http.post(ROTA_SALVAR, data, true).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarUsuarios();
            resetar();
        });
    });

})($componente, $http, $validate);
