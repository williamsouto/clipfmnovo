(function (utils) {
    var $menuAdministrador = $('#menuAdministrador');
    var $wrapperAdmin      = $('.wrapper-admin');
    var $btnExibirMenu     = $('.admin-head .btn-exibir-menu');

    $menuAdministrador.menu();

    $('.item').click(function () {
        var pagina = $(this).data('pagina');

        var url = pagina !== '' ? '/administrador/' + pagina : '/administrador';
        utils.redirect(url);
    });

    var pagina = utils.getParams();
    var $menu  = pagina === 'administrador' ? $('li[data-pagina=""]') : $('li[data-pagina="'+pagina+'"]');

    $menu.addClass('active');

    var validarResize = function () {

        if ($(window).width() <= 800) {
            $wrapperAdmin.hide();
            $btnExibirMenu.show();
            resizeContentAdmin();

        } else if ($(window).width() <= 900) {
            $('.arrow-left').hide();
            $btnExibirMenu.hide();
            $wrapperAdmin.show();
            fecharMenu();

        } else {
            $wrapperAdmin.show();
            $btnExibirMenu.hide();
            resizeContentAdmin();
        }
    };

    var resizeContentAdmin = function () {
        var $contentAdmin = $('.app .content-admin');

        if ($wrapperAdmin.is(':visible')) {

            if ($menuAdministrador.hasClass("mini-menu")) {
                $contentAdmin.css('padding', '0 0 0 60px');
            } else {
                $contentAdmin.css('padding', '0 0 0 191px');
            }

        } else {
            $contentAdmin.css('padding', '0');
        }
    };

    var fecharMenu = function () {
        $('.arrow-right').show('fast');

        $menuAdministrador.addClass('mini-menu');
        $menuAdministrador.find('li span').hide();

        resizeContentAdmin();

        $wrapperAdmin.effect("size", {
            to: { width: 50}
        }, 500);
    };

    var abrirMenu = function () {
        $('.arrow-left').show();

        $menuAdministrador.removeClass('mini-menu');
        resizeContentAdmin();

        $wrapperAdmin.effect("size", {
            to: { width: 191}
        }, 500, function () {
            $menuAdministrador.find('li span').show();
        });
    };

    $('.arrow-left').click(function () {
        $(this).hide();
        fecharMenu();
    });

    $('.arrow-right').click(function () {
        $(this).hide();
        abrirMenu();
    });

    $(window).resize(function() {
        validarResize();
    });

    $btnExibirMenu.click(function () {
        if (!$wrapperAdmin.is(':visible')) {
            $wrapperAdmin.show();
            abrirMenu();
            $('.arrow-left').hide();
            $('.arrow-right').hide();

        } else {
            $wrapperAdmin.hide();
            fecharMenu();
        }
    });

    validarResize();

})($utils, $componente);
