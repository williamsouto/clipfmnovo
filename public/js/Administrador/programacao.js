(function (componente, http, validate, mask) {

    var ROTA_BUSCAR = '/administrador/programacao/buscar';
    var ROTA_SALVAR = '/administrador/programacao/salvar';

    var tabs         = componente.tabs('#programacao');
    var dataTable    = componente.dataTable('#tblProgramacao');
    var validation   = validate.init('form[name="programacao"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();
    mask.init('#horario', 'time');

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();
                validation.bindForm(resposta['programacoes']);
            });
        });
    };

    var buscarProgramacoes = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['programacoes'], editar);
        });
    };

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    buscarProgramacoes();

    $('#btnCancelar').click(function () {
        resetar();
    });

    $('#btnCadastrar').click(function () {
        resetar();
    });

    $('#btnSalvarProgramacao').click(function () {
        var data = validation.dataForm(true);

        http.post(ROTA_SALVAR, data, true).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarProgramacoes();
            resetar();
        });
    });

})($componente, $http, $validate, $mask);
