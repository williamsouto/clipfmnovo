(function (utils, componente, http) {
    $('#loading').fadeOut(300);

    var notification = componente.notification('#mensagem');
    var linkLeiaMais = "<a class=\"leia-mais\">[Ler mais...]</a><br>";

    var buscarEstatistica = function () {
        http.get('/administrador/estatistica').then(function (resposta) {
            componente.bind('#estatistica', resposta['estatistica']);
        });
    };

    var buscarPedidos = function () {
        http.get('/administrador/pedido/buscar/5').then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            componente.forItem('#pedidos', resposta['pedidos']);
            criarEventoBotoesPedido('#pedidos');
        });
    };

    var buscarContatos = function () {
        http.get('/administrador/contato/dashboard/5').then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            if (resposta['contatos'].length > 0) {
                for (var contato in resposta['contatos']) {
                    if (resposta['contatos'].hasOwnProperty(contato)) {
                        resposta['contatos'][contato]['mensagem'] = processarMensagem(resposta['contatos'][contato]['mensagem']);
                    }
                }

                componente.forItem('#contatos', resposta['contatos']);
                criarEventoManipularTexto();
                criarEventoBotoesContato('#contatos');
            } else {
                $('.bloco-contatos').hide();
            }
        });
    };

    var criarEventoBotoesContato = function ($seletor) {
        var $seletorVisualizar = utils.isTypeOf($seletor, 'string') ? $($seletor + ' .visualizar') : $seletor.find('.visualizar');
        var $seletorDeletar = utils.isTypeOf($seletor, 'string') ? $($seletor + ' .deletar') : $seletor.find('.deletar');

        $seletorVisualizar.click(function () {
            callbackClickBtnContato(this);
        });

        $seletorDeletar.click(function () {
            callbackClickBtnContato(this);
        });
    };

    var criarEventoBotoesPedido = function ($seletor) {
        var $seletorDeletar = utils.isTypeOf($seletor, 'string') ? $($seletor + ' .deletar') : $seletor.find('.deletar');

        $seletorDeletar.click(function () {
            callbackClickBtnPedido(this);
        });
    };

    var callbackClickBtnContato = function (seletor) {
        var $this = $(seletor);
        var id    = $this.data('id');
        var rota  = $this.attr('class') === 'visualizar' ? 'visualizar-dashboard' : 'deletar';

        http.post('/administrador/contato/'+rota, {id_contato: id}).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            notification.success(resposta.message);
            criarAnimacaoAcoesBotoesBox($this, '#contatos', resposta['contato']['id_contato'], 'div[data-item="contato"]', 'contato', resposta['contato']);
        });
    };

    var callbackClickBtnPedido = function (seletor) {
        var $this = $(seletor);
        var id    = $this.data('id');

        http.post('/administrador/pedido/deletar', {id_pedido: id}).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            notification.success(resposta.message);
            criarAnimacaoAcoesBotoesBox($this, '#pedidos', resposta['pedido']['id_pedido'], 'div[data-item="pedido"]', 'pedido', resposta['pedido']);
        });
    };

    var criarAnimacaoAcoesBotoesBox = function ($this, container, id, seletor, assinatura, data) {
        var $box = $this.parent().parent();

        if (assinatura === 'pedido') {
            $box = $box.parent();
        }

        if (!utils.isTypeOf(id, 'undefined')) {
            var html = componente.getBind(seletor, assinatura, data);

            var $clone = $box.clone();
            $clone.html(html);

            var inserirNovoBox = function() {
                $(container).append($clone);
                var $novoBox = $(container + ' .widget-content:last-child');

                $novoBox.hide().show('drop', {direction: 'right'}, 1000);

                if (assinatura === 'contato') {
                    criarEventoBotoesContato($novoBox);
                } else {
                    criarEventoBotoesPedido($novoBox);
                }
            };
        }

        $box.hide('drop', {direction: 'left'}, 1000, function () {
            if (utils.isTypeOf(inserirNovoBox, 'function')) {
                setTimeout(inserirNovoBox(), 1500);
            }
        });
    };

    var criarEventoManipularTexto = function () {
        $('.leia-mais').click(function () {
            var $this = $(this);
            var $mensagem = $this.parent();
            $this.remove();

            var height = $mensagem.css('max-height');
            $mensagem.css('max-height', '100%');
            $mensagem.html($mensagem.html().replace('<br>', ''));
            $mensagem.append('<br><a class="diminuir">[Diminuir...]</a>');

            $('.diminuir').click(function () {
                $(this).remove();
                $mensagem.html($mensagem.html().replace('<br>', ''));
                $mensagem.css('max-height', height);

                $mensagem.html(processarMensagem($mensagem.html()));
                criarEventoManipularTexto();
            });
        });
    };

    var processarMensagem = function (mensagem) {
        var str = mensagem;

        if (mensagem.length > 280) {
            str = str.substr(0, 280);
            str += linkLeiaMais;
            str += mensagem.substr(280, mensagem.length - 1);
        }

        return str;
    };

    http.get('/administrador/programa-no-ar').then(function (resposta) {
        componente.bind('#agora_no_ar', resposta['programa']);
    });

    buscarEstatistica();
    buscarPedidos();
    buscarContatos();

})($utils, $componente, $http);
