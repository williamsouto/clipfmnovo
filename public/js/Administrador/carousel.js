(function (validate, http, componente, utils) {
    var DESTAQUE     = 'Destaques';
    var tabs         = componente.tabs('#carousel');
    var validation   = validate.init('form[name="carousel"]');
    var dataTable    = componente.dataTable('#tblCarousel');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();
    var ehDestaque   = false;

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get('/administrador/carousel/buscar/' + id).then(function (resposta) {
                resetar();
                buscarTiposCarousels(function () {
                    validation.bindForm(resposta['carousels']);
                });
            });
        });
    };

    var buscarCarousels = function () {
        http.get('/administrador/carousel/buscar').then(function (resposta) {
            dataTable.setRows(resposta['carousels'], editar);
        });
    };

    var buscarTiposCarousels = function (callback) {
        http.get('/administrador/carousel-tipo/buscar').then(function (resposta) {
            componente.forItem('#id_carousel_tipo', resposta['tipos_carousel']);

            if (utils.isTypeOf(callback, 'function')) {
                callback();
            }
        });
    };

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    $("#noticia").autocomplete({
        source: function(request, response) {
            $("#id_noticia").val('');

            http.get('/administrador/noticia/buscar/termo', {termo: request.term}).then(function (noticias) {
                var data = [];
                $.each(noticias['noticias'], function (index, noticia) {
                    data.push({label: noticia['titulo'], value: noticia['id_noticia'] + ' - ' +noticia['titulo'], id_noticia: noticia['id_noticia']});
                });

                response(data);
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $("#id_noticia").val(ui.item.id_noticia);
        }
    });

    $('#btnSalvarCarousel').click(function () {
        var data = validation.dataForm(true);
        var desc = $('#descricao').val();

        if (utils.isEmpty(desc) && !ehDestaque) {
            notification.info('Descrição deve ser preenchido.');
            return false;
        }

        data.append('descricao', desc);

        http.post('/administrador/carousel/salvar', data, true).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarCarousels();
            resetar();
        });
    });

    $('#btnCadastrar, #btnCancelar').click(function () {
        validation.resetForm();
        tabs.alterTab();
        buscarTiposCarousels();
    });

    $('#id_carousel_tipo'). change(function () {
        var id = $(this).val();

        http.get('/administrador/carousel-tipo/buscar/'+id).then(function (response) {
            if (DESTAQUE === response['tipos_carousel']['nome']) {
                $('#boxDescricao').hide();
                ehDestaque = true;
            } else {
                $('#boxDescricao').show();
                ehDestaque = false;
            }
        });
    });

    buscarCarousels();

})($validate, $http, $componente, $utils);
