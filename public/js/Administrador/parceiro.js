(function (componente, http, validate) {

    var ROTA_BUSCAR = '/administrador/parceiro/buscar';
    var ROTA_SALVAR = '/administrador/parceiro/salvar';

    var tabs         = componente.tabs('#parceiro');
    var dataTable    = componente.dataTable('#tblParceiro');
    var validation   = validate.init('form[name="parceiro"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();
                validation.bindForm(resposta['parceiros']);
            });
        });
    };

    var buscarParceiros = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['parceiros'], editar);
        });
    };

    buscarParceiros();

    $('#btnCancelar').click(function () {
        resetar();
    });

    $('#btnCadastrar').click(function () {
        resetar();
    });

    $('#btnSalvarParceiro').click(function () {
        var data = validation.dataForm(true);

        http.post(ROTA_SALVAR, data, true).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarParceiros();
            resetar();
        });
    });

})($componente, $http, $validate);
