(function (componente, http, validate, utils) {

    var ROTA_BUSCAR = '/administrador/noticia/buscar';
    var ROTA_SALVAR = '/administrador/noticia/salvar';

    CKEDITOR.replace( 'texto', {
        language: 'pt-br',
        filebrowserUploadUrl: '/administrador/noticia/imagem'
    });

    var tabs         = componente.tabs('#noticia');
    var dataTable    = componente.dataTable('#tblNoticia');
    var validation   = validate.init('form[name="noticia"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();
                buscarCategorias(function () {
                    validation.bindForm(resposta['noticias']);
                });

                CKEDITOR.instances['texto'].setData(resposta['noticias']['texto']);
            });
        });
    };

    var buscarNoticias = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['noticias'], editar);
        });
    };

    var buscarCategorias = function (callback) {
        http.get('/administrador/categoria/buscar').then(function (resposta) {
            componente.forItem('#categoria', resposta['categorias']);

            if (utils.isTypeOf(callback, 'function')) {
                callback();
            }
        });
    };

    var resetar = function () {
        validation.resetForm();
        CKEDITOR.instances['texto'].setData('');
        tabs.alterTab();
    };

    buscarNoticias();

    $('#btnCancelar').click(function () {
        resetar();
    });

    $('#btnCadastrar').click(function () {
        resetar();
        buscarCategorias();
    });

    $('#btnSalvarNoticia').click(function () {
        var texto = CKEDITOR.instances['texto'].getData();

        if (utils.isEmpty(texto)) {
            notification.info('Texto da notícia não pode ser vazio.');
            return false;
        }

        var data = validation.dataForm(true);
        data.append('texto', texto);

        http.post(ROTA_SALVAR, data, true).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarNoticias();
            resetar();
        });
    });

})($componente, $http, $validate, $utils);
