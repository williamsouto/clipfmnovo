(function (componente, http, validate) {

    var ROTA_BUSCAR     = '/administrador/contato/buscar';
    var ROTA_VISUALIZAR = '/administrador/contato/visualizar';

    var tabs         = componente.tabs('#contato');
    var dataTable    = componente.dataTable('#tblContato');
    var validation   = validate.init('form[name="contato"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();
                validation.bindForm(resposta['contatos']);

                if (parseInt(resposta['contatos']['ind_visto'], 10)) {
                    $('#btnVisualizarContato').attr('disabled', true);
                }
            });
        });
    };

    var buscarContatos = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['contatos'], editar);
        });
    };

    buscarContatos();

    $('#btnCancelar').click(function () {
        resetar();
    });

    $('#btnVisualizarContato').click(function () {
        var id = $('#id_contato').val();

        http.post(ROTA_VISUALIZAR, {id_contato: id}).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarContatos();
            resetar();
        });
    });

})($componente, $http, $validate);
