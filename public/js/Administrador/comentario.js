(function (componente, http, validate) {

    var ROTA_BUSCAR  = '/administrador/comentario/buscar';
    var ROTA_APROVAR = '/administrador/comentario/aprovar';

    var tabs         = componente.tabs('#comentario');
    var dataTable    = componente.dataTable('#tblComentario');
    var validation   = validate.init('form[name="comentario"]');
    var notification = componente.notification('#mensagem');
    var dialog       = componente.dialog();

    var resetar = function () {
        validation.resetForm();
        tabs.alterTab();
    };

    var editar = function() {

        $('.btnEditar').click(function () {
            var id = $(this).data('id');

            http.get(ROTA_BUSCAR + '/' + id).then(function (resposta) {
                resetar();
                validation.bindForm(resposta['comentarios']);

                if (parseInt(resposta['comentarios']['ind_exibir'], 10)) {
                    $('#btnAprovarComentario').attr('disabled', true);
                }
            });
        });
    };

    var buscarComentarios = function () {
        http.get(ROTA_BUSCAR).then(function (resposta) {
            dataTable.setRows(resposta['comentarios'], editar);
        });
    };

    buscarComentarios();

    $('#btnCancelar').click(function () {
        resetar();
    });

    $('#btnAprovarComentario').click(function () {
        var data = {
            id_comentario: $('#id_comentario').val(),
            id_noticia   : $('#id_noticia').val()
        };

        http.post(ROTA_APROVAR, data).then(function (resposta) {
            if (!resposta.success) {
                notification.error(resposta.message);
                return false;
            }

            dialog.success(resposta.message);
            buscarComentarios();
            resetar();
        });
    });

})($componente, $http, $validate);
