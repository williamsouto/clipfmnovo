var $animate = (function () {
    return {
        init: function () {
            var $slideRight = function (selector, pixels, speed) {
                var $selector   = $(selector);
                var initial     = $selector.css('right').replace('px', '')*1;
                var newPosition = pixels - initial;
                $selector.animate({'right':newPosition}, speed);
            };

            var $slideLeft = function (selector, pixels, speed) {
                var $selector   = $(selector);
                var initial     = $selector.css('right').replace('px', '')*1;
                var newPosition = pixels - initial;
                $selector.animate({'right':newPosition}, speed);
            };

            return {
                slideRight: $slideRight,
                slideLeft : $slideLeft
            }
        }
    }
})();
