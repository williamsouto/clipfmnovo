var $componente = (function() {
    return {
        init : function() {
            var $tab = function ($content, $url) {
                var tabActive = $('.tabs').data('active');
                $('.tabs .tab-title[data-title="'+tabActive+'"]').addClass('active-tab');

                $('.tabs .tab-title').click(function() {
                    var $this = $(this);
                    var body  = $this.next('.tab-body');
                    animation(body);
                    active(this);

                    var content = $this.data('content');
                    if ($content && typeof content !== 'undefined') {
                        $('.tab-content#'+content).show();
                    }

                    var url = $this.data('href');
                    if ($url && typeof url !== 'undefined') {
                        window.location.href = url;
                    }
                });

                var active = function(element) {
                    var $element = $(element);
                    var parent = $element.parent().attr('class');
                    if (parent !== 'tab-body') {
                        $('.tabs .tab-title').removeClass('active-tab');
                        $element.toggleClass('active-tab');
                    }
                };

                var animation = function (body) {
                    if (body.is(":visible")) {
                        body.slideUp('fast');
                    } else {
                        body.slideDown('fast');
                    }
                }
            };

            var $modal = function ($title) {
                var redirectUrl;
                var div = document.createElement('div');
                div.setAttribute('class', 'modal-app');
                $('body').append(div);

                $('.modal-app').load('/page/modal', {titulo: $title}, function () {
                    $('.modal-app #btnFecharModal').click(function () {
                        hide();
                    });
                });

                var show = function (content, url) {
                    var $appModal = $('#appModal');
                    redirectUrl = url;

                    $appModal.find('.content').html(content);
                    $appModal.modal('show');
                };

                var hide = function () {
                    $('#appModal').modal('hide');

                    if (typeof redirectUrl !== 'undefined') {
                        window.location.href = redirectUrl;
                    }
                };

                return {
                    show: show,
                    hide: hide
                }
            };

            var $dialog = function () {

                var div = document.createElement('div');
                div.setAttribute('class', 'dialog-app');
                $('body').append(div);

                $('.dialog-app').load('/page/dialog');

                var success = function (message) {
                    $('#appDialog .info, #appDialog .success, #appDialog .warning, #appDialog .error').hide();
                    show(message, 'success', 'green');
                };

                var info = function (message) {
                    $('#appDialog .info, #appDialog .success, #appDialog .warning, #appDialog .error').hide();
                    show(message, 'info', 'blue');
                };

                var warning = function (message) {
                    $('#appDialog .info, #appDialog .success, #appDialog .warning, #appDialog .error').hide();
                    show(message, 'warning', 'orange');
                };

                var error = function (message) {
                    $('#appDialog .info, #appDialog .success, #appDialog .warning, #appDialog .error').hide();
                    show(message, 'error', 'red');
                };

                var show = function (message, type, color) {
                    var $dialog = $('#appDialog');

                    $dialog.find('.content .message').html(message);
                    $dialog.find('.'+type+' .icon').addClass('color-'+color);
                    $dialog.find('.'+type).show();
                    $dialog.modal('show');
                };

                var hide = function () {
                    $('#appDialog').modal('hide');
                };

                return {
                    success: success,
                    info   : info,
                    warning: warning,
                    error  : error,
                    hide   : hide
                }
            };

            var $notification = function (parent) {
                var SUCCESS = 'success';
                var ERROR   = 'error';
                var WARNING = 'warning';
                var INFO    = 'info';

                var CONFIG = {
                    success: {icon: 'fa fa-check', label: '&nbsp;&nbsp;Sucesso! '},
                    error  : {icon: 'fa fa-times', label: '&nbsp;&nbsp;Erro! '},
                    warning: {icon: 'fa fa-warning', label: '&nbsp;&nbsp;Atenção! '},
                    info   : {icon: 'fa fa-info', label: '&nbsp;&nbsp;Informação! '}
                };

                var $parent = $(parent);
                $parent.css('display', 'none');

                var notification = document.createElement('div');
                var message      = $(notification).clone().get(0);

                notification.className = 'notification';
                message.className      = 'message';

                var span = document.createElement('span');
                var i    = document.createElement('i');

                span.className = 'btn-close';
                i.className    = 'fa fa-window-close';

                span.appendChild(i);
                notification.appendChild(message);
                notification.appendChild(span);
                $parent.append(notification);

                var success = function (message) {
                    viewMessage(message, SUCCESS);
                };

                var error = function (message) {
                    viewMessage(message, ERROR);
                };

                var warning = function (message) {
                    viewMessage(message, WARNING);
                };

                var info = function (message) {
                    viewMessage(message, INFO);
                };

                var viewMessage = function (message, type) {
                    var $message = $('.message');
                    var strong   = document.createElement('strong');
                    var i        = document.createElement('i');

                    $(strong).append(CONFIG[type]['label']);
                    i.className = CONFIG[type]['icon'];

                    $message.append(i);
                    $message.append(strong);
                    $message.append(message);
                    $parent.find('.notification').addClass(type);

                    $parent.fadeIn(800).delay(5000).fadeOut(800, function () {
                        clear(type);
                    });

                    $('.btn-close').click(function () {
                        $parent.hide();
                        clear(type);
                    });
                };

                var clear =function (type) {
                    $('.message').html('');
                    $parent.find('.notification').removeClass(type);
                };

                return {
                    success: success,
                    error  : error,
                    warning: warning,
                    info   : info
                }
            };

            var $player = function (stream, playSelector, pauseSelector, volumeControlSelector, onSelector, offSelector) {
                var Player = new Audio(stream);
                var $play  = playSelector;
                var $pause = pauseSelector;
                var $on    = onSelector;
                var $off   = offSelector;
                var $volumeControl = volumeControlSelector;

                var play = function () {
                    var playPromise = Player.play();

                    if ($utils.isEmpty(playPromise)) {
                        playPromise.then(function() {
                            $play.hide();
                            $pause.css('display', 'inline-block');
                        }).catch(function(error) {
                            console.log('failed to load stream', error);
                        });
                    }
                };

                var pause = function () {
                    $play.show();
                    $pause.hide();
                    Player.pause();
                };

                var alterVolume = function (volume) {
                    Player.volume = volume / 100;
                    changeIcon(volume);
                };

                var mute = function () {
                    var volume = Player.volume === 0 ? 80 : 0;
                    $volumeControl.slider("option", "value", volume);
                    changeIcon(volume);

                    Player.volume = volume / 100;
                };

                var changeIcon  = function (volume) {
                    if (volume === 0) {
                        $on.hide();
                        $off.show();
                    } else {
                        $on.show();
                        $off.hide();
                    }
                };

                return {
                    play       : play,
                    pause      : pause,
                    alterVolume: alterVolume,
                    mute       : mute
                };
            };

            var $tabs = function ($selector) {
                var selector = $selector;
                var alterTab = function () {

                    var $tabActive = $(selector+'.app-tabs .app-tab.tab-active');
                    var $tab       = $(selector+'.app-tabs .app-tab:not(.tab-active)');

                    var $lastTab  = $(selector+' .app-tab:last');
                    var $firstTab = $(selector+' .app-tab:first');

                    var directionHide, directionShow = '';

                    if ($lastTab.is(":visible")) {
                        directionShow = 'left';
                        directionHide = 'right';

                    } else {
                        directionShow = 'right';
                        directionHide = 'left';
                    }

                    $($tabActive).hide('slide', {direction: directionHide}, function () {
                        $($tab).show('slide', {direction: directionShow});

                        $firstTab.toggleClass('tab-active');
                        $lastTab.toggleClass('tab-active');
                    });
                };

                return {
                    alterTab: alterTab
                }
            };

            var $dataTable = function (grid) {
                var $grid          = grid;
                var $headerColumns = $(grid).find('thead tr th');

                var ACOES    = 'acoes';
                var rows     = [];
                var total    = 0;
                var begin    = 0;
                var end      = 0;
                var limit    = 10;
                var active   = 1;
                var offset   = 0;
                var callback = null;

                var setRows = function (collection, callbackFunction) {
                    rows  = collection;
                    total = $utils.isEmpty(collection) ? 0 : collection.length;

                    createTable(collection);
                    active = 1;
                    configPaginate();

                    if (typeof callbackFunction === 'function') {
                        callback = callbackFunction;
                        callbackFunction();
                    }
                };

                var createTable = function (collection) {
                    createQuantityView();

                    $($grid).find('tbody').remove();
                    var tbody = document.createElement('tbody');
                    var tr, td;

                    if (!$utils.isTypeOf(collection, "undefined") && collection.length > 0) {
                        var key, type, acoes;

                        for (var i = 0; i < limit; i++) {

                            if (i < total && i < collection.length) {
                                tr = document.createElement('tr');
                                $headerColumns.each(function () {
                                    var $this = $(this);

                                    type = $this.data('type');
                                    key  = $this.data('key');

                                    td = document.createElement('td');

                                    if ($this.data('expand')) {
                                        $(td).addClass('expand');
                                        var spanPlus  = document.createElement('span');
                                        var spanMinus = document.createElement('span');

                                        $(spanPlus).addClass('add-row');
                                        $(spanMinus).addClass('remove-row');
                                        $(spanMinus).css('display', 'none');

                                        var iconPlus = document.createElement('i');
                                        $(iconPlus).addClass('fa fa-plus-square');

                                        var iconMinus = document.createElement('i');
                                        $(iconMinus).addClass('fa fa-minus-square');

                                        $(spanPlus).append(iconPlus);
                                        $(spanMinus).append(iconMinus);
                                        $(spanPlus).attr('data-index', i);
                                        $(spanMinus).attr('data-index', i);

                                        td.append(spanPlus);
                                        td.append(spanMinus);

                                        $(spanPlus).click(function () {
                                            var $this = $(this);
                                            var currentRow = $this.parent().parent();
                                            var newRow     = document.createElement('tr');
                                            var count      = $(currentRow).find('td').length;
                                            var tdData     = document.createElement('td');
                                            var indexRow   = $this.data('index');
                                            var validClass = '.hide-medium-viewport, .hide-small-viewport, .hide-min-viewport';

                                            $(tdData).attr('colspan', count);
                                            var info  = null;

                                            for (var data in rows[indexRow]) {
                                                var thKey       = $('table thead tr th[data-key="'+data+'"]');
                                                var label       = thKey.text();
                                                var indexColumn = thKey.parent().children().index(thKey);
                                                var hasClass    = $(currentRow).find('td:eq('+indexColumn+')').is(validClass);
                                                var visible     = !$(currentRow).find('td:eq('+indexColumn+')').is(':visible');

                                                if (rows[indexRow].hasOwnProperty(data) && label !== "" && hasClass && visible) {
                                                    var p = document.createElement('p');
                                                    info = label+': '+rows[indexRow][data];

                                                    $(p).append(info);
                                                    $(tdData).append(p);
                                                }
                                            }

                                            $(newRow).append(tdData);

                                            $(newRow).addClass('data-row');
                                            $(currentRow).after(newRow);

                                            $this.hide();
                                            $this.parent().find('span.remove-row').show();
                                        });

                                        $(spanMinus).click(function () {
                                            var rowRemove = $(this).parent().parent().next();
                                            rowRemove.remove();

                                            $(this).hide();
                                            $(this).parent().find('span.add-row').show();
                                        });
                                    }

                                    if (type === ACOES) {
                                        acoes = $this.find('.actions > *').clone();

                                        var id = '';

                                        acoes.each(function () {
                                            id = $(this).data('key');
                                            $(this).attr("data-id", collection[i][id]);
                                        });

                                        acoes.appendTo(td);

                                    } else if (collection[i].hasOwnProperty(key)) {
                                        td.append(collection[i][key]);
                                    }

                                    var classAditional = $this.data('class');

                                    if (classAditional !== undefined) {
                                        $(td).addClass(classAditional);
                                        $this.addClass(classAditional);
                                    }

                                    var align = $this.data('align');
                                    align = align ? align : 'left';

                                    $(td).addClass(align);

                                    tr.append(td);
                                });

                                $(window).resize(function () {
                                    resize();
                                });
                            }

                            tbody.append(tr);
                        }

                    } else {
                        tr = document.createElement('tr');
                        td = document.createElement('td');

                        tr.appendChild(td);
                        td.className = 'center';
                        td.setAttribute('colspan', $headerColumns.length);
                        $(td).append('Nenhum Registro');

                        tbody.append(tr);
                    }

                    $($grid).append(tbody);
                    resize();
                };

                var resize = function () {
                    var spanAdd    = null;
                    var spanRemove = null;

                    $('td.hide-medium-viewport, td.hide-small-viewport, td.hide-min-viewport').each(function () {
                        var $this  = $(this);
                        spanAdd    = $this.parent().find('td.expand span.add-row');
                        spanRemove = $this.parent().find('td.expand span.remove-row');

                        if ($this.css('display') === 'none') {
                            spanAdd.show();
                        } else {
                            spanAdd.hide();
                        }

                        $this.parent().parent().find('tr.data-row').remove();
                        spanRemove.hide();
                    });
                };

                var configPaginate = function () {
                    $('.table-rodape').remove();

                    var div   = document.createElement('div');
                    var left  = document.createElement('div');
                    var right = document.createElement('div');

                    left.className = 'pull-left';
                    right.className = 'pull-right';

                    div.className = 'table-rodape';

                    var pagLeft = document.createElement('div');
                    pagLeft.className = 'pagination';

                    var pagRight = $(pagLeft).clone().get(0);

                    var child = '<p>Exibindo <span class="inicio"></span> até <span class="fim"></span> de <span class="total"></span></p>';
                    $(pagLeft).append(child);

                    $('.page.active').removeClass('active');

                    var amount;

                    if ((total / limit) >= 5) {
                        amount = 5;
                    }

                    if ((total / limit) < 5 && (total % limit) > 0) {
                        amount = parseInt((total / limit), 10) + 1;
                    }

                    var index = offset;

                    for (var i = 0; i < amount; i++) {
                        var divPage = document.createElement('div');
                        divPage.className = active === (index + 1) ? 'page active' : 'page';
                        divPage.setAttribute('data-index', index);

                        $(divPage).append(index+1);
                        pagRight.append(divPage);

                        index++;
                    }

                    right.append(pagRight);
                    left.append(pagLeft);

                    div.appendChild(left);
                    div.appendChild(right);

                    $($grid).parent().append(div);

                    calcValues(active - 1);

                    $('.table-rodape .pagination .inicio').append(begin);
                    $('.table-rodape .pagination .fim').append(end);
                    $('.table-rodape .pagination .total').append(total);

                    $('.page').click(function () {
                        changePage($(this).data('index'));
                    });
                };

                var changePage = function (index) {
                    calcValues(index);
                    var array = rows.slice(begin - 1, end);
                    createTable(array);

                    if (typeof callback === "function") {
                        callback();
                    }

                    configPaginate();
                };

                var calcValues = function (index) {
                    begin  = (index * limit) + 1;
                    end    = limit * (index + 1);
                    active = index + 1;

                    var totalPage = (total % limit) > 0 ? parseInt(total / limit, 10) + 1 : (total / limit);

                    offset = (index <= 2) || totalPage <= 5 ? 0 : index - 2;

                    if (totalPage > 5 && (active + 2) > totalPage) {
                        offset = index - (((active + 2) - 5) + 1);
                    }
                };

                var createQuantityView = function () {
                    var options = "" +
                        "<option value='5'>5</option>" +
                        "<option value='10'>10</option>" +
                        "<option value='15'>15</option>" +
                        "<option value='20'>20</option>";

                    var select = document.createElement('select');
                    select.title = 'Alterar Quantidade de Exibição';
                    select.id = 'amountRows';

                    $(select).append(options);
                    $(select).val(limit);

                    var $tableController = $($grid).prev();
                    $tableController.html('Exibir ');
                    $tableController.append(select);

                    $tableController.find('select#amountRows').change(function () {
                        limit = $(this).val();
                        changePage(0);
                    });
                };

                return {
                    setRows : setRows
                }
            };

            var $jcarousel = function (selector, number) {
                var jcarousel = $(selector);
                var amount = number !== undefined ? number : 3;

                jcarousel
                    .on('jcarousel:reload jcarousel:create', function () {
                        var carousel = $(this),
                            width = carousel.innerWidth();

                        if (width >= 600) {
                            width = width / amount;
                        } else if (width >= 350) {
                            width = width / 2;
                        }

                        carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                    })
                    .jcarousel({
                        wrap: 'circular'
                    });

                $(selector + ' + .jcarousel-control-prev').jcarouselControl({
                    target: '-=1'
                });

                $(selector + ' + a + .jcarousel-control-next').jcarouselControl({
                    target: '+=1'
                });

                $('.jcarousel-pagination')
                    .on('jcarouselpagination:active', 'a', function() {
                        $(this).addClass('active');
                    })
                    .on('jcarouselpagination:inactive', 'a', function() {
                        $(this).removeClass('active');
                    })
                    .on('click', function(e) {
                        e.preventDefault();
                    }).jcarouselPagination({
                    perPage: 1,
                    item: function(page) {
                        return '<a href="#' + page + '">' + page + '</a>';
                    }
                });
            };

            var $forItem = function (selector, collection, remove) {
                if ($utils.isEmpty(collection)) {
                    $(selector).hide();
                    return false;
                }

                var $container = $(selector);
                var clause     = $container.data('for');

                $container.find('.item-for').each(function () {
                    $(this).remove();
                });

                var element = clause.split(' ')[0];
                var $item   = $container.find("*[data-item='"+ element +"']");

                if (!remove) {
                    $item.css('display', 'block');
                }

                var copy = $item.clone();

                if (!remove) {
                    $item.css('display', 'none');
                }

                if (remove) {
                    $item.remove();
                }

                copy.removeAttr('data-item');
                copy.addClass('item-for');

                var div = document.createElement('div');
                $(div).append(copy);

                var occurrences = div.innerHTML.match(/\{\{\s*([^\s]+)\s*\}\}/g);
                var conditions = div.innerHTML.match(/\{\{\s*([^\\s]+)\s\s*\}\}/g);

                if (occurrences !== null) {
                    $.each(collection, function (index, item) {
                        var html = div.innerHTML;

                        occurrences.forEach(function (occur) {
                            var expression = occur.replace(/{{\s*|\s*}}/g, "");
                            var key    = expression.split('.')[1];

                            html = html.replace(occur, item[key]);
                        });

                        if (!$utils.isEmpty(conditions)) {
                            conditions.forEach(function (occur) {

                                var expression  = occur.replace(/{{\s*|\s*}}/g, "");
                                var condicional = expression.split('?')[0];
                                var action      = expression.split('?')[1];
                                var result      = false;

                                var op = condicional.trim().split(' ');
                                var value = op[0] === 'index' ? index.toString() : op[0].split('.')[1];

                                switch (op[1]) {
                                    case '==':
                                        result = value === op[2];
                                        break;
                                    case '!=':
                                        result = value !== op[2];
                                        break;
                                    case '>':
                                        result = value > op[2];
                                        break;
                                    case '>=':
                                        result = value >= op[2];
                                        break;
                                    case '<':
                                        result = value < op[2];
                                        break;
                                    default:
                                        result = value <= op[2];
                                }

                                if (!result) {
                                    action = '';
                                }

                                html = html.replace(occur, action.replace(' ', ''));
                            });
                        }

                        $container.append(html);
                    });
                }
            };

            var $bind = function (selector, data, returnHtml) {
                if ($utils.isEmpty(data)) {
                    $(selector).hide();
                    return false;
                }

                var $content = $(selector);

                var occurrences = $content.html().match(/\{\{\s*([^\s]+)\s*\}\}/g);
                var html = $content.html();

                occurrences.forEach(function (occur) {
                    var expression = occur.replace(/{{\s*|\s*}}/g, "");
                    var params = expression.split('.');
                    var key    = params[1];
                    var assign = selector.replace('#', '');

                    if (assign === params[0]) {
                        html = html.replace(occur, data[key]);
                    }
                });

                if (returnHtml === true) {
                    return html;
                }

                $content.html(html);
            };

            var $getBind = function (selector, idTemp, data) {
                $(selector).attr('id', idTemp);
                var html = $bind('#'+idTemp, data, true);
                $(selector).removeAttr('id');

                return html;
            };

            var $share = function (selector, socialNetwork, obj) {
                var social= {
                    facebook: 'https://www.facebook.com/sharer/sharer.php?u={{ location }}&ampt;t={{ title }}',
                    twitter: 'https://twitter.com/intent/tweet?url={{ location }}&text={{ title }}',
                    google: 'https://plus.google.com/share?url={{ location }}&text={{ title }}',
                    linkedin: 'https://www.linkedin.com/cws/share?url={{ location }}?name={{ title }}'
                };

                var getWindowOptions = function() {
                    var width = 500;
                    var height = 350;
                    var left = (window.innerWidth / 2) - (width / 2);
                    var top = (window.innerHeight / 2) - (height / 2);

                    return [
                        'resizable,scrollbars,status',
                        'height=' + height,
                        'width=' + width,
                        'left=' + left,
                        'top=' + top
                    ].join();
                };

                var text = social[socialNetwork];
                var params = text.match(/\{\{\s*([^\s]+)\s*\}\}/g);
                var socialShareBtn = $(selector);
                var url = social[socialNetwork];
                var property;

                if (params !== undefined && obj !== undefined) {
                    for (var i = 0; i < params.length; i++) {
                        property = params[i].replace(/{{\s*|\s*}}/g, "");

                        if (obj.hasOwnProperty(property)) {
                            url = url.replace(params[i], obj[property]);
                        }
                    }
                }

                socialShareBtn.attr('href', url);

                socialShareBtn.click(function(e) {
                    e.preventDefault();
                    var win = window.open(url, socialShareBtn, getWindowOptions());
                    win.opener = null;
                });
            };

            return {
                tab         : $tab,
                modal       : $modal,
                dialog      : $dialog,
                player      : $player,
                tabs        : $tabs,
                dataTable   : $dataTable,
                notification: $notification,
                forItem     : $forItem,
                jcarousel   : $jcarousel,
                bind        : $bind,
                getBind     : $getBind,
                share       : $share
            };
        }
    };
})();
